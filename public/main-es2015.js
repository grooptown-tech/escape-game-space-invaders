(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _trophies_trophy_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./trophies/trophy.component */ "./src/app/trophies/trophy.component.ts");
/* harmony import */ var _items_items_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./items/items.component */ "./src/app/items/items.component.ts");







class AppComponent {
    constructor(gameService, router) {
        this.gameService = gameService;
        this.router = router;
        this.title = 'escape-game';
        this.router.events.subscribe(event => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                function replaceAll(str, find, replace) {
                    return str.replace(new RegExp(find, 'g'), replace);
                }
                let page_title = replaceAll(event.urlAfterRedirects, "-", "");
                page_title = replaceAll(page_title, "/", "");
                console.log("Sending ", page_title);
                // @ts-ignore
                gtag('event', 'page_view', {
                    page_title: page_title,
                    page_location: page_title,
                    page_path: page_title,
                    send_to: 'G-1RR79R36XT'
                });
            }
        });
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 3, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-trophy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-items");
    } }, directives: [_trophies_trophy_component__WEBPACK_IMPORTED_MODULE_3__["TrophyComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"], _items_items_component__WEBPACK_IMPORTED_MODULE_4__["ItemsComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/app.constants.ts":
/*!**********************************!*\
  !*** ./src/app/app.constants.ts ***!
  \**********************************/
/*! exports provided: AppConstants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConstants", function() { return AppConstants; });
class AppConstants {
}
/* Items */
AppConstants.KEY_DRAWER1 = "key_drawer1";
AppConstants.PHONE_ITEM = "phone-flash-invaders";
AppConstants.PHONE_ITEM_INVADERS = "invaders";
AppConstants.SCREWDRIVER = "screwdriver";
AppConstants.REMOTE = "remote";
AppConstants.GRIMOIRE = "grimoire";
AppConstants.LASER = "space-invader-laser";
AppConstants.OLD_GOLD_KEY = "old-golden-key";
AppConstants.SUSHIS = "sushis";
/* States */
AppConstants.INTRO_DONE = "intro-done";
AppConstants.LOCKER_GOLD_OPENED = "lockerGoldOpened";
AppConstants.COMPUTER_OPENED = "computerOpened";
AppConstants.SPACE_INVADERS_WON = "space-invaders-won";
AppConstants.IS_SAFE_OPENED = "is-safe-opened";
AppConstants.PAINTING_DONE = "painting-done";
AppConstants.LIVING_ROOM1_COUNT_VISIT = "living-room1-count-visit";
/* Trophies */
AppConstants.CARROTS = "carrots";
AppConstants.HEART = "heart";
AppConstants.NURSE_INVADER = "nurse-invader";
AppConstants.INFIRMARY_INVADER = "infirmary-invader";
AppConstants.BANDAID_INVADER = "bandaid-invader";
AppConstants.SYRINGE_INVADER = "syringe-invader";
AppConstants.RABBIT = "rabbit";


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _livingroom1_livingroom1_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./livingroom1/livingroom1.component */ "./src/app/livingroom1/livingroom1.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _livingroom2_livingroom2_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./livingroom2/livingroom2.component */ "./src/app/livingroom2/livingroom2.component.ts");
/* harmony import */ var _livingroom3_livingroom3_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./livingroom3/livingroom3.component */ "./src/app/livingroom3/livingroom3.component.ts");
/* harmony import */ var _livingroom4_livingroom4_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./livingroom4/livingroom4.component */ "./src/app/livingroom4/livingroom4.component.ts");
/* harmony import */ var _emptydrawer_emptydrawer_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./emptydrawer/emptydrawer.component */ "./src/app/emptydrawer/emptydrawer.component.ts");
/* harmony import */ var _livingroom3drawer1_livingroom3drawer1_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./livingroom3drawer1/livingroom3drawer1.component */ "./src/app/livingroom3drawer1/livingroom3drawer1.component.ts");
/* harmony import */ var _items_items_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./items/items.component */ "./src/app/items/items.component.ts");
/* harmony import */ var _livingroom1drawer_purple_livingroom1drawer_purple_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./livingroom1drawer-purple/livingroom1drawer-purple.component */ "./src/app/livingroom1drawer-purple/livingroom1drawer-purple.component.ts");
/* harmony import */ var _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./landingpage/landingpage.component */ "./src/app/landingpage/landingpage.component.ts");
/* harmony import */ var _livingroom_window_livingroom_window_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./livingroom-window/livingroom-window.component */ "./src/app/livingroom-window/livingroom-window.component.ts");
/* harmony import */ var _phone_invaders_phone_invaders_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./phone-invaders/phone-invaders.component */ "./src/app/phone-invaders/phone-invaders.component.ts");
/* harmony import */ var _lockergold1_lockergold1_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./lockergold1/lockergold1.component */ "./src/app/lockergold1/lockergold1.component.ts");
/* harmony import */ var _insidechestgold_insidechestgold_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./insidechestgold/insidechestgold.component */ "./src/app/insidechestgold/insidechestgold.component.ts");
/* harmony import */ var _trophies_trophy_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./trophies/trophy.component */ "./src/app/trophies/trophy.component.ts");
/* harmony import */ var _insidecupboardleftinner_insidecupboardleftinner_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./insidecupboardleftinner/insidecupboardleftinner.component */ "./src/app/insidecupboardleftinner/insidecupboardleftinner.component.ts");
/* harmony import */ var _watchingtv_watchingtv_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./watchingtv/watchingtv.component */ "./src/app/watchingtv/watchingtv.component.ts");
/* harmony import */ var _coffee_coffee_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./coffee/coffee.component */ "./src/app/coffee/coffee.component.ts");
/* harmony import */ var _clock_digital_clock_digital_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./clock-digital/clock-digital.component */ "./src/app/clock-digital/clock-digital.component.ts");
/* harmony import */ var _read_grimoire_read_grimoire_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./read-grimoire/read-grimoire.component */ "./src/app/read-grimoire/read-grimoire.component.ts");
/* harmony import */ var _inside_computer_secured_inside_computer_secured_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./inside-computer-secured/inside-computer-secured.component */ "./src/app/inside-computer-secured/inside-computer-secured.component.ts");
/* harmony import */ var _inside_computer_login_inside_computer_login_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./inside-computer-login/inside-computer-login.component */ "./src/app/inside-computer-login/inside-computer-login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _under_sofa_right_under_sofa_right_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./under-sofa-right/under-sofa-right.component */ "./src/app/under-sofa-right/under-sofa-right.component.ts");
/* harmony import */ var _inside_wood_cupboard_left_inside_wood_cupboard_left_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./inside-wood-cupboard-left/inside-wood-cupboard-left.component */ "./src/app/inside-wood-cupboard-left/inside-wood-cupboard-left.component.ts");
/* harmony import */ var _jukebox_jukebox_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./jukebox/jukebox.component */ "./src/app/jukebox/jukebox.component.ts");
/* harmony import */ var _door_opened_game_won_door_opened_game_won_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./door-opened-game-won/door-opened-game-won.component */ "./src/app/door-opened-game-won/door-opened-game-won.component.ts");
/* harmony import */ var _inside_cupboard_japan_inside_cupboard_japan_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./inside-cupboard-japan/inside-cupboard-japan.component */ "./src/app/inside-cupboard-japan/inside-cupboard-japan.component.ts");








































class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
            _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
            _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_6__["MatTooltipModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"].forRoot([
                { path: '', component: _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_19__["LandingpageComponent"] },
                { path: 'livingroom1', component: _livingroom1_livingroom1_component__WEBPACK_IMPORTED_MODULE_9__["Livingroom1Component"] },
                { path: 'livingroom2', component: _livingroom2_livingroom2_component__WEBPACK_IMPORTED_MODULE_12__["Livingroom2Component"] },
                { path: 'livingroom3', component: _livingroom3_livingroom3_component__WEBPACK_IMPORTED_MODULE_13__["Livingroom3Component"] },
                { path: 'livingroom4', component: _livingroom4_livingroom4_component__WEBPACK_IMPORTED_MODULE_14__["Livingroom4Component"] },
                { path: 'emptydrawer', component: _emptydrawer_emptydrawer_component__WEBPACK_IMPORTED_MODULE_15__["EmptydrawerComponent"] },
                { path: 'livingroom3drawer1', component: _livingroom3drawer1_livingroom3drawer1_component__WEBPACK_IMPORTED_MODULE_16__["Livingroom3drawer1Component"] },
                { path: 'livingroom1drawerPurple', component: _livingroom1drawer_purple_livingroom1drawer_purple_component__WEBPACK_IMPORTED_MODULE_18__["Livingroom1drawerPurpleComponent"] },
                { path: 'livingroom-window/:windowId', component: _livingroom_window_livingroom_window_component__WEBPACK_IMPORTED_MODULE_20__["LivingroomWindowComponent"] },
                { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"] },
                { path: 'phone-invaders', component: _phone_invaders_phone_invaders_component__WEBPACK_IMPORTED_MODULE_21__["PhoneInvadersComponent"] },
                { path: 'lockergold1', component: _lockergold1_lockergold1_component__WEBPACK_IMPORTED_MODULE_22__["Lockergold1Component"] },
                { path: 'inside-chest-gold', component: _insidechestgold_insidechestgold_component__WEBPACK_IMPORTED_MODULE_23__["InsidechestgoldComponent"] },
                { path: 'inside-cupboard-left-inner', component: _insidecupboardleftinner_insidecupboardleftinner_component__WEBPACK_IMPORTED_MODULE_25__["InsidecupboardleftinnerComponent"] },
                { path: 'watching-tv', component: _watchingtv_watchingtv_component__WEBPACK_IMPORTED_MODULE_26__["WatchingtvComponent"] },
                { path: 'coffee', component: _coffee_coffee_component__WEBPACK_IMPORTED_MODULE_27__["CoffeeComponent"] },
                { path: 'clock-digital', component: _clock_digital_clock_digital_component__WEBPACK_IMPORTED_MODULE_28__["ClockDigitalComponent"] },
                { path: 'read-grimoire', component: _read_grimoire_read_grimoire_component__WEBPACK_IMPORTED_MODULE_29__["ReadGrimoireComponent"] },
                { path: 'inside-computer-login', component: _inside_computer_login_inside_computer_login_component__WEBPACK_IMPORTED_MODULE_31__["InsideComputerLoginComponent"] },
                { path: 'inside-computer-secured', component: _inside_computer_secured_inside_computer_secured_component__WEBPACK_IMPORTED_MODULE_30__["InsideComputerSecuredComponent"] },
                { path: 'under-sofa-right', component: _under_sofa_right_under_sofa_right_component__WEBPACK_IMPORTED_MODULE_33__["UnderSofaRightComponent"] },
                { path: 'inside-wood-cupboard-left', component: _inside_wood_cupboard_left_inside_wood_cupboard_left_component__WEBPACK_IMPORTED_MODULE_34__["InsideWoodCupboardLeftComponent"] },
                { path: 'jukebox', component: _jukebox_jukebox_component__WEBPACK_IMPORTED_MODULE_35__["JukeboxComponent"] },
                { path: 'doorOpenedGameWon', component: _door_opened_game_won_door_opened_game_won_component__WEBPACK_IMPORTED_MODULE_36__["DoorOpenedGameWonComponent"] },
                { path: 'japan-expo', component: _inside_cupboard_japan_inside_cupboard_japan_component__WEBPACK_IMPORTED_MODULE_37__["InsideCupboardJapanComponent"] },
                { path: '**', component: _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_19__["LandingpageComponent"] }
            ], { useHash: true }),
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_32__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_32__["ReactiveFormsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
        _livingroom1_livingroom1_component__WEBPACK_IMPORTED_MODULE_9__["Livingroom1Component"],
        _livingroom2_livingroom2_component__WEBPACK_IMPORTED_MODULE_12__["Livingroom2Component"],
        _livingroom3_livingroom3_component__WEBPACK_IMPORTED_MODULE_13__["Livingroom3Component"],
        _livingroom4_livingroom4_component__WEBPACK_IMPORTED_MODULE_14__["Livingroom4Component"],
        _emptydrawer_emptydrawer_component__WEBPACK_IMPORTED_MODULE_15__["EmptydrawerComponent"],
        _livingroom3drawer1_livingroom3drawer1_component__WEBPACK_IMPORTED_MODULE_16__["Livingroom3drawer1Component"],
        _items_items_component__WEBPACK_IMPORTED_MODULE_17__["ItemsComponent"],
        _livingroom1drawer_purple_livingroom1drawer_purple_component__WEBPACK_IMPORTED_MODULE_18__["Livingroom1drawerPurpleComponent"],
        _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_19__["LandingpageComponent"],
        _livingroom_window_livingroom_window_component__WEBPACK_IMPORTED_MODULE_20__["LivingroomWindowComponent"],
        _phone_invaders_phone_invaders_component__WEBPACK_IMPORTED_MODULE_21__["PhoneInvadersComponent"],
        _lockergold1_lockergold1_component__WEBPACK_IMPORTED_MODULE_22__["Lockergold1Component"],
        _insidechestgold_insidechestgold_component__WEBPACK_IMPORTED_MODULE_23__["InsidechestgoldComponent"],
        _trophies_trophy_component__WEBPACK_IMPORTED_MODULE_24__["TrophyComponent"],
        _insidecupboardleftinner_insidecupboardleftinner_component__WEBPACK_IMPORTED_MODULE_25__["InsidecupboardleftinnerComponent"],
        _watchingtv_watchingtv_component__WEBPACK_IMPORTED_MODULE_26__["WatchingtvComponent"],
        _coffee_coffee_component__WEBPACK_IMPORTED_MODULE_27__["CoffeeComponent"],
        _clock_digital_clock_digital_component__WEBPACK_IMPORTED_MODULE_28__["ClockDigitalComponent"],
        _read_grimoire_read_grimoire_component__WEBPACK_IMPORTED_MODULE_29__["ReadGrimoireComponent"],
        _inside_computer_secured_inside_computer_secured_component__WEBPACK_IMPORTED_MODULE_30__["InsideComputerSecuredComponent"],
        _inside_computer_login_inside_computer_login_component__WEBPACK_IMPORTED_MODULE_31__["InsideComputerLoginComponent"],
        _under_sofa_right_under_sofa_right_component__WEBPACK_IMPORTED_MODULE_33__["UnderSofaRightComponent"],
        _inside_wood_cupboard_left_inside_wood_cupboard_left_component__WEBPACK_IMPORTED_MODULE_34__["InsideWoodCupboardLeftComponent"],
        _jukebox_jukebox_component__WEBPACK_IMPORTED_MODULE_35__["JukeboxComponent"],
        _door_opened_game_won_door_opened_game_won_component__WEBPACK_IMPORTED_MODULE_36__["DoorOpenedGameWonComponent"],
        _inside_cupboard_japan_inside_cupboard_japan_component__WEBPACK_IMPORTED_MODULE_37__["InsideCupboardJapanComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
        _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_6__["MatTooltipModule"], _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_32__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_32__["ReactiveFormsModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
                    _livingroom1_livingroom1_component__WEBPACK_IMPORTED_MODULE_9__["Livingroom1Component"],
                    _livingroom2_livingroom2_component__WEBPACK_IMPORTED_MODULE_12__["Livingroom2Component"],
                    _livingroom3_livingroom3_component__WEBPACK_IMPORTED_MODULE_13__["Livingroom3Component"],
                    _livingroom4_livingroom4_component__WEBPACK_IMPORTED_MODULE_14__["Livingroom4Component"],
                    _emptydrawer_emptydrawer_component__WEBPACK_IMPORTED_MODULE_15__["EmptydrawerComponent"],
                    _livingroom3drawer1_livingroom3drawer1_component__WEBPACK_IMPORTED_MODULE_16__["Livingroom3drawer1Component"],
                    _items_items_component__WEBPACK_IMPORTED_MODULE_17__["ItemsComponent"],
                    _livingroom1drawer_purple_livingroom1drawer_purple_component__WEBPACK_IMPORTED_MODULE_18__["Livingroom1drawerPurpleComponent"],
                    _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_19__["LandingpageComponent"],
                    _livingroom_window_livingroom_window_component__WEBPACK_IMPORTED_MODULE_20__["LivingroomWindowComponent"],
                    _phone_invaders_phone_invaders_component__WEBPACK_IMPORTED_MODULE_21__["PhoneInvadersComponent"],
                    _lockergold1_lockergold1_component__WEBPACK_IMPORTED_MODULE_22__["Lockergold1Component"],
                    _insidechestgold_insidechestgold_component__WEBPACK_IMPORTED_MODULE_23__["InsidechestgoldComponent"],
                    _trophies_trophy_component__WEBPACK_IMPORTED_MODULE_24__["TrophyComponent"],
                    _insidecupboardleftinner_insidecupboardleftinner_component__WEBPACK_IMPORTED_MODULE_25__["InsidecupboardleftinnerComponent"],
                    _watchingtv_watchingtv_component__WEBPACK_IMPORTED_MODULE_26__["WatchingtvComponent"],
                    _coffee_coffee_component__WEBPACK_IMPORTED_MODULE_27__["CoffeeComponent"],
                    _clock_digital_clock_digital_component__WEBPACK_IMPORTED_MODULE_28__["ClockDigitalComponent"],
                    _read_grimoire_read_grimoire_component__WEBPACK_IMPORTED_MODULE_29__["ReadGrimoireComponent"],
                    _inside_computer_secured_inside_computer_secured_component__WEBPACK_IMPORTED_MODULE_30__["InsideComputerSecuredComponent"],
                    _inside_computer_login_inside_computer_login_component__WEBPACK_IMPORTED_MODULE_31__["InsideComputerLoginComponent"],
                    _under_sofa_right_under_sofa_right_component__WEBPACK_IMPORTED_MODULE_33__["UnderSofaRightComponent"],
                    _inside_wood_cupboard_left_inside_wood_cupboard_left_component__WEBPACK_IMPORTED_MODULE_34__["InsideWoodCupboardLeftComponent"],
                    _jukebox_jukebox_component__WEBPACK_IMPORTED_MODULE_35__["JukeboxComponent"],
                    _door_opened_game_won_door_opened_game_won_component__WEBPACK_IMPORTED_MODULE_36__["DoorOpenedGameWonComponent"],
                    _inside_cupboard_japan_inside_cupboard_japan_component__WEBPACK_IMPORTED_MODULE_37__["InsideCupboardJapanComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                    _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                    _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_6__["MatTooltipModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"].forRoot([
                        { path: '', component: _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_19__["LandingpageComponent"] },
                        { path: 'livingroom1', component: _livingroom1_livingroom1_component__WEBPACK_IMPORTED_MODULE_9__["Livingroom1Component"] },
                        { path: 'livingroom2', component: _livingroom2_livingroom2_component__WEBPACK_IMPORTED_MODULE_12__["Livingroom2Component"] },
                        { path: 'livingroom3', component: _livingroom3_livingroom3_component__WEBPACK_IMPORTED_MODULE_13__["Livingroom3Component"] },
                        { path: 'livingroom4', component: _livingroom4_livingroom4_component__WEBPACK_IMPORTED_MODULE_14__["Livingroom4Component"] },
                        { path: 'emptydrawer', component: _emptydrawer_emptydrawer_component__WEBPACK_IMPORTED_MODULE_15__["EmptydrawerComponent"] },
                        { path: 'livingroom3drawer1', component: _livingroom3drawer1_livingroom3drawer1_component__WEBPACK_IMPORTED_MODULE_16__["Livingroom3drawer1Component"] },
                        { path: 'livingroom1drawerPurple', component: _livingroom1drawer_purple_livingroom1drawer_purple_component__WEBPACK_IMPORTED_MODULE_18__["Livingroom1drawerPurpleComponent"] },
                        { path: 'livingroom-window/:windowId', component: _livingroom_window_livingroom_window_component__WEBPACK_IMPORTED_MODULE_20__["LivingroomWindowComponent"] },
                        { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"] },
                        { path: 'phone-invaders', component: _phone_invaders_phone_invaders_component__WEBPACK_IMPORTED_MODULE_21__["PhoneInvadersComponent"] },
                        { path: 'lockergold1', component: _lockergold1_lockergold1_component__WEBPACK_IMPORTED_MODULE_22__["Lockergold1Component"] },
                        { path: 'inside-chest-gold', component: _insidechestgold_insidechestgold_component__WEBPACK_IMPORTED_MODULE_23__["InsidechestgoldComponent"] },
                        { path: 'inside-cupboard-left-inner', component: _insidecupboardleftinner_insidecupboardleftinner_component__WEBPACK_IMPORTED_MODULE_25__["InsidecupboardleftinnerComponent"] },
                        { path: 'watching-tv', component: _watchingtv_watchingtv_component__WEBPACK_IMPORTED_MODULE_26__["WatchingtvComponent"] },
                        { path: 'coffee', component: _coffee_coffee_component__WEBPACK_IMPORTED_MODULE_27__["CoffeeComponent"] },
                        { path: 'clock-digital', component: _clock_digital_clock_digital_component__WEBPACK_IMPORTED_MODULE_28__["ClockDigitalComponent"] },
                        { path: 'read-grimoire', component: _read_grimoire_read_grimoire_component__WEBPACK_IMPORTED_MODULE_29__["ReadGrimoireComponent"] },
                        { path: 'inside-computer-login', component: _inside_computer_login_inside_computer_login_component__WEBPACK_IMPORTED_MODULE_31__["InsideComputerLoginComponent"] },
                        { path: 'inside-computer-secured', component: _inside_computer_secured_inside_computer_secured_component__WEBPACK_IMPORTED_MODULE_30__["InsideComputerSecuredComponent"] },
                        { path: 'under-sofa-right', component: _under_sofa_right_under_sofa_right_component__WEBPACK_IMPORTED_MODULE_33__["UnderSofaRightComponent"] },
                        { path: 'inside-wood-cupboard-left', component: _inside_wood_cupboard_left_inside_wood_cupboard_left_component__WEBPACK_IMPORTED_MODULE_34__["InsideWoodCupboardLeftComponent"] },
                        { path: 'jukebox', component: _jukebox_jukebox_component__WEBPACK_IMPORTED_MODULE_35__["JukeboxComponent"] },
                        { path: 'doorOpenedGameWon', component: _door_opened_game_won_door_opened_game_won_component__WEBPACK_IMPORTED_MODULE_36__["DoorOpenedGameWonComponent"] },
                        { path: 'japan-expo', component: _inside_cupboard_japan_inside_cupboard_japan_component__WEBPACK_IMPORTED_MODULE_37__["InsideCupboardJapanComponent"] },
                        { path: '**', component: _landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_19__["LandingpageComponent"] }
                    ], { useHash: true }),
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_32__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_32__["ReactiveFormsModule"]
                ],
                providers: [],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/clock-digital/clock-digital.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/clock-digital/clock-digital.component.ts ***!
  \**********************************************************/
/*! exports provided: ClockDigitalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClockDigitalComponent", function() { return ClockDigitalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");





function ClockDigitalComponent_span_3_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/invaders/spaceInvaders-", el_r1.toLowerCase(), ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function ClockDigitalComponent_span_3_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](el_r1);
} }
function ClockDigitalComponent_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ClockDigitalComponent_span_3_span_1_Template, 2, 1, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ClockDigitalComponent_span_3_span_2_Template, 2, 1, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r1 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.inInvaderLetter(el_r1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.inInvaderLetter(el_r1));
} }
const _c0 = function () { return ["/livingroom2"]; };
class ClockDigitalComponent {
    constructor(gameService, router) {
        this.gameService = gameService;
        this.router = router;
        this.timer = "";
    }
    ngOnInit() {
        this.changeTime();
    }
    inInvaderLetter(el) {
        return ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F'].indexOf(el.toUpperCase()) != -1;
    }
    changeTime() {
        let newDate = (new Date).toTimeString().substring(0, 8);
        if (newDate != this.timer) {
            this.timer = newDate;
            this.gameService.playSound("Tick.mp3");
        }
        if (this.router.url != "/clock-digital") {
            return;
        }
        setTimeout(() => {
            this.changeTime();
        }, 50);
    }
}
ClockDigitalComponent.ɵfac = function ClockDigitalComponent_Factory(t) { return new (t || ClockDigitalComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
ClockDigitalComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ClockDigitalComponent, selectors: [["app-clock-digital"]], decls: 6, vars: 3, consts: [[1, "clock-digital-bg"], [1, "clock-digital"], [1, "clock-digital-inner"], [4, "ngFor", "ngForOf"], [3, "routerLink"], [1, "goToBottom"], [4, "ngIf"], ["class", "letter", 4, "ngIf"], [3, "src"], [1, "letter"]], template: function ClockDigitalComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ClockDigitalComponent_span_3_Template, 3, 2, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.timer.split(""));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"]], styles: [".clock-digital-bg[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/clock-digital-bg.png');\n  background-size: 100% 100%;\n}\n.clock-digital[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/clock-digital.png');\n  background-size: 100% 100%;\n  height: 300px;\n  width: 750px;\n  left: 50px;\n  top: 320px;\n  position: absolute;\n  letter-spacing: 18px;\n}\n.clock-digital-inner[_ngcontent-%COMP%] {\n  position: absolute;\n  left: 110px;\n  top: 120px;\n}\n.clock-digital-inner[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  height: 62px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2xvY2stZGlnaXRhbC9jbG9jay1kaWdpdGFsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCx5RUFBb0Q7RUFDcEQsMEJBQTBCO0FBQzVCO0FBQ0E7RUFDRSxzRUFBc0U7RUFDdEUsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixZQUFZO0VBQ1osVUFBVTtFQUNWLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFVBQVU7QUFDWjtBQUNBO0VBQ0UsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvY2xvY2stZGlnaXRhbC9jbG9jay1kaWdpdGFsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2xvY2stZGlnaXRhbC1iZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2Nsb2NrLWRpZ2l0YWwtYmcucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbi5jbG9jay1kaWdpdGFsIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ltYWdlcy92YXJpb3VzL2Nsb2NrLWRpZ2l0YWwucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgd2lkdGg6IDc1MHB4O1xuICBsZWZ0OiA1MHB4O1xuICB0b3A6IDMyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxldHRlci1zcGFjaW5nOiAxOHB4O1xufVxuLmNsb2NrLWRpZ2l0YWwtaW5uZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDExMHB4O1xuICB0b3A6IDEyMHB4O1xufVxuLmNsb2NrLWRpZ2l0YWwtaW5uZXIgaW1nIHtcbiAgaGVpZ2h0OiA2MnB4O1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ClockDigitalComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-clock-digital',
                templateUrl: './clock-digital.component.html',
                styleUrls: ['./clock-digital.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/coffee/coffee.component.ts":
/*!********************************************!*\
  !*** ./src/app/coffee/coffee.component.ts ***!
  \********************************************/
/*! exports provided: CoffeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoffeeComponent", function() { return CoffeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



const _c0 = function () { return ["/livingroom4"]; };
class CoffeeComponent {
    constructor() { }
    ngOnInit() {
    }
}
CoffeeComponent.ɵfac = function CoffeeComponent_Factory(t) { return new (t || CoffeeComponent)(); };
CoffeeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CoffeeComponent, selectors: [["app-coffee"]], decls: 10, vars: 2, consts: [[1, "coffee-bg"], [1, "coffee-box"], [1, "decaf-invaders-container"], ["src", "assets/images/invaders/spaceInvaders-d.png"], ["src", "assets/images/invaders/spaceInvaders-e.png"], ["src", "assets/images/invaders/spaceInvaders-c.png"], ["src", "assets/images/invaders/spaceInvaders-a.png"], ["src", "assets/images/invaders/spaceInvaders-f.png"], [3, "routerLink"], [1, "goToBottom"]], template: function CoffeeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: [".coffee-bg[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/coffee-bear.jpg');\n  background-size: 100% 100%;\n}\n.coffee-box[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 520px;\n  width: 400px;\n  background-image: url('/escape-game-space-invaders/coffe-box.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 530px;\n  top: 134px;\n}\n.decaf-text[_ngcontent-%COMP%] {\n  color: white;\n  font-size: 50px;\n  font-family: cursive;\n  top: 155px;\n  position: absolute;\n  left: 50px;\n  letter-spacing: 19px;\n}\n.decaf-invaders-container[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 155px;\n  left: 41px;\n}\n.decaf-invaders-container[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 51px;\n  margin-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29mZmVlL2NvZmZlZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsb0VBQStDO0VBQy9DLDBCQUEwQjtBQUM1QjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0VBQ1osa0VBQTZDO0VBQzdDLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7QUFDWjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1Ysb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFVBQVU7QUFDWjtBQUNBO0VBQ0UsV0FBVztFQUNYLGlCQUFpQjtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL2NvZmZlZS9jb2ZmZWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2ZmZWUtYmcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9jb2ZmZWUtYmVhci5qcGdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuLmNvZmZlZS1ib3gge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNTIwcHg7XG4gIHdpZHRoOiA0MDBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2NvZmZlLWJveC5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMDA7XG4gIGxlZnQ6IDUzMHB4O1xuICB0b3A6IDEzNHB4O1xufVxuLmRlY2FmLXRleHQge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgZm9udC1mYW1pbHk6IGN1cnNpdmU7XG4gIHRvcDogMTU1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNTBweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDE5cHg7XG59XG4uZGVjYWYtaW52YWRlcnMtY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDE1NXB4O1xuICBsZWZ0OiA0MXB4O1xufVxuLmRlY2FmLWludmFkZXJzLWNvbnRhaW5lciBpbWcge1xuICB3aWR0aDogNTFweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CoffeeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-coffee',
                templateUrl: './coffee.component.html',
                styleUrls: ['./coffee.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/door-opened-game-won/door-opened-game-won.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/door-opened-game-won/door-opened-game-won.component.ts ***!
  \************************************************************************/
/*! exports provided: DoorOpenedGameWonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DoorOpenedGameWonComponent", function() { return DoorOpenedGameWonComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






const _c0 = function (a0, a1, a2, a3, a4) { return { "top.px": a0, "left.px": a1, "width.px": a2, "height.px": a3, "background-image": a4 }; };
function DoorOpenedGameWonComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 5);
} if (rf & 2) {
    const trophyPaint_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction5"](1, _c0, trophyPaint_r1.y, trophyPaint_r1.x, trophyPaint_r1.width, trophyPaint_r1.height, "url(assets/images/trophies/" + trophyPaint_r1.name + ".png)"));
} }
const _c1 = function () { return ["/livingroom1"]; };
class DoorOpenedGameWonComponent {
    constructor(gameService) {
        this.gameService = gameService;
        this.trophies = [
            { x: 550, y: 456, height: 164, width: 64, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].NURSE_INVADER },
            { x: 350, y: 480, height: 70, width: 70, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].INFIRMARY_INVADER },
            { x: 0, y: 480, height: 46, width: 32, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].BANDAID_INVADER },
            { x: 0, y: 480, height: 50, width: 50, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SYRINGE_INVADER },
            { x: 462, y: 147, height: 40, width: 30, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].HEART },
            { x: 70, y: 70, height: 80, width: 80, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].CARROTS },
            { x: 500, y: 430, height: 30, width: 25, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].RABBIT },
        ];
    }
    ngOnInit() {
        this.gameService.playSound("mario-kart-tour-win.mp3");
    }
}
DoorOpenedGameWonComponent.ɵfac = function DoorOpenedGameWonComponent_Factory(t) { return new (t || DoorOpenedGameWonComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
DoorOpenedGameWonComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DoorOpenedGameWonComponent, selectors: [["app-door-opened-game-won"]], decls: 6, vars: 3, consts: [["id", "gameWonLandscape", 1, "fullbg"], [1, "won-message"], ["class", "trophy", 3, "ngStyle", 4, "ngFor", "ngForOf"], [3, "routerLink"], [1, "goToBottom"], [1, "trophy", 3, "ngStyle"]], template: function DoorOpenedGameWonComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Happy Birthday !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, DoorOpenedGameWonComponent_div_3_Template, 1, 7, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.trophies);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c1));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgStyle"]], styles: ["#gameWonLandscape[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/gameWonLandscape.jpg');\n}\n\n.won-message[_ngcontent-%COMP%] {\n  font-family: OvertheRainbow Verdana, fantasy;\n  font-size: 80px;\n  position: absolute;\n  top: 200px;\n  left: 150px;\n  color: white;\n  font-weight: bold;\n}\n\n.trophy[_ngcontent-%COMP%] {\n  position: absolute;\n  background-size: 100% 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZG9vci1vcGVuZWQtZ2FtZS13b24vZG9vci1vcGVuZWQtZ2FtZS13b24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlFQUFvRDtBQUN0RDs7QUFFQTtFQUNFLDRDQUE0QztFQUM1QyxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixXQUFXO0VBQ1gsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQiwwQkFBMEI7QUFDNUIiLCJmaWxlIjoic3JjL2FwcC9kb29yLW9wZW5lZC1nYW1lLXdvbi9kb29yLW9wZW5lZC1nYW1lLXdvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2dhbWVXb25MYW5kc2NhcGUge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvZ2FtZVdvbkxhbmRzY2FwZS5qcGdcIik7XG59XG5cbi53b24tbWVzc2FnZSB7XG4gIGZvbnQtZmFtaWx5OiBPdmVydGhlUmFpbmJvdyBWZXJkYW5hLCBmYW50YXN5O1xuICBmb250LXNpemU6IDgwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAyMDBweDtcbiAgbGVmdDogMTUwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4udHJvcGh5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DoorOpenedGameWonComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-door-opened-game-won',
                templateUrl: './door-opened-game-won.component.html',
                styleUrls: ['./door-opened-game-won.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/emptydrawer/emptydrawer.component.ts":
/*!******************************************************!*\
  !*** ./src/app/emptydrawer/emptydrawer.component.ts ***!
  \******************************************************/
/*! exports provided: EmptydrawerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmptydrawerComponent", function() { return EmptydrawerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



const _c0 = function () { return ["/livingroom2"]; };
class EmptydrawerComponent {
    constructor() { }
    ngOnInit() {
    }
}
EmptydrawerComponent.ɵfac = function EmptydrawerComponent_Factory(t) { return new (t || EmptydrawerComponent)(); };
EmptydrawerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: EmptydrawerComponent, selectors: [["app-emptydrawer"]], decls: 3, vars: 2, consts: [["id", "emptydrawer-root"], [3, "routerLink"], [1, "goToBottom"]], template: function EmptydrawerComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: ["#emptydrawer-root[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/emptyDrawer.jpg');\n  background-size: 100% 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZW1wdHlkcmF3ZXIvZW1wdHlkcmF3ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLG9FQUE0RDtFQUM1RCwwQkFBMEI7QUFDNUIiLCJmaWxlIjoic3JjL2FwcC9lbXB0eWRyYXdlci9lbXB0eWRyYXdlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2VtcHR5ZHJhd2VyLXJvb3Qge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pbWFnZXMvZW1wdHlEcmF3ZXIuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EmptydrawerComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-emptydrawer',
                templateUrl: './emptydrawer.component.html',
                styleUrls: ['./emptydrawer.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var t_writer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! t-writer.js */ "./node_modules/t-writer.js/dist/t-writer.js");
/* harmony import */ var t_writer_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(t_writer_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");






const _c0 = ["startText"];
const _c1 = function () { return ["/livingroom1"]; };
class HomeComponent {
    constructor(gameService) {
        this.gameService = gameService;
        this.gameService.resetGame(true);
    }
    ngOnInit() {
        const target = document.querySelector('#startText');
        const writer = new t_writer_js__WEBPACK_IMPORTED_MODULE_1___default.a(target, {
            loop: false,
            typeColor: '#01FF40',
            typeSpeed: 60
        });
        setTimeout(function () {
            writer.type('The Ministry Of Defense Has Examined More Than 15 000 Alleged Sightings of Flying Saucers And Another Unidentified Flying Objects Over Paris Since 1959 And Is Still Keeping An Open Mind On The Existence Of Aliens. In 2005, More Than 350 000 People Were In The Street Of Paris For The Technoparade. Suddenly, Coming From The Sky, A Flying Object Approached And Hovered Overhead.<br/><br/><br/>Now we know the truth ........  SPACE INVADERS ARE BACK<br/><br/>')
                .start();
        }, 1500);
        this.gameService.playMusic("space-invaders-are-back.mp3");
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], viewQuery: function HomeComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.startTextEl = _t.first);
    } }, decls: 10, vars: 2, consts: [[1, "home-root"], ["alt", "invader-image", "src", "assets/images/trophies/rabbit.png", 1, "space-invader"], ["id", "startText"], ["startText", ""], [1, "launch-escape-game"], [3, "routerLink"], ["mat-raised-button", "", "color", "primary"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2, 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Enter Room !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c1));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButton"]], styles: [".home-root[_ngcontent-%COMP%] {\n  background-size: 950px 650px;\n  height: 100%;\n  background-color: rgb(10,10,10);\n  color: white;\n}\n\n.space-invader[_ngcontent-%COMP%] {\n  display: block;\n  height: 300px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n#startText[_ngcontent-%COMP%] {\n  margin-left : 100px;\n  margin-right : 100px;\n}\n\n.launch-escape-game[_ngcontent-%COMP%] {\n  text-align:center;\n  margin-left: auto;\n  margin-right: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw0QkFBNEI7RUFDNUIsWUFBWTtFQUNaLCtCQUErQjtFQUMvQixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ob21lLXJvb3Qge1xuICBiYWNrZ3JvdW5kLXNpemU6IDk1MHB4IDY1MHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigxMCwxMCwxMCk7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnNwYWNlLWludmFkZXIge1xuICBkaXNwbGF5OiBibG9jaztcbiAgaGVpZ2h0OiAzMDBweDtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn1cblxuI3N0YXJ0VGV4dCB7XG4gIG1hcmdpbi1sZWZ0IDogMTAwcHg7XG4gIG1hcmdpbi1yaWdodCA6IDEwMHB4O1xufVxuXG4ubGF1bmNoLWVzY2FwZS1nYW1lIHtcbiAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG5cblxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, { startTextEl: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['startText']
        }] }); })();


/***/ }),

/***/ "./src/app/inside-computer-login/inside-computer-login.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/inside-computer-login/inside-computer-login.component.ts ***!
  \**************************************************************************/
/*! exports provided: InsideComputerLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsideComputerLoginComponent", function() { return InsideComputerLoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");









function InsideComputerLoginComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Wrong login/password ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom4"]; };
class InsideComputerLoginComponent {
    constructor(gameService, router) {
        this.gameService = gameService;
        this.router = router;
        this.wrongAnswer = false;
    }
    ngOnInit() {
    }
    doLogin() {
        this.gameService.playSound("sound-effect-computer-data.mp3");
        if (this.gameService.getStc().toUpperCase().replace("I AM ", "") == this.login.toUpperCase() &&
            this.password.toUpperCase() == "DECAF") {
            this.gameService.usedItems[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].COMPUTER_OPENED] = true;
            this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].REMOTE);
            this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].GRIMOIRE);
            this.router.navigate(['inside-computer-secured']);
        }
        else {
            this.wrongAnswer = true;
        }
    }
}
InsideComputerLoginComponent.ɵfac = function InsideComputerLoginComponent_Factory(t) { return new (t || InsideComputerLoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"])); };
InsideComputerLoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: InsideComputerLoginComponent, selectors: [["app-inside-computer-login"]], decls: 18, vars: 5, consts: [["id", "macbook-login-bg"], ["id", "macbook-login"], ["id", "login-interface"], [1, "login-form", 3, "submit"], [1, "input-login"], ["matInput", "", "placeholder", "XXXXXXX", "value", "", "name", "login", "autocomplete", "off", 3, "ngModel", "ngModelChange"], [1, "interline"], [1, "input-password"], ["matInput", "", "placeholder", "XXXXX", "name", "password", "autocomplete", "off", 3, "ngModel", "ngModelChange"], ["type", "submit", 1, "login-button", "pointer"], ["class", "wrongAnswer", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], [1, "wrongAnswer"]], template: function InsideComputerLoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("submit", function InsideComputerLoginComponent_Template_form_submit_3_listener() { return ctx.doLogin(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-form-field", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "I am XXXXXXX");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "input", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function InsideComputerLoginComponent_Template_input_ngModelChange_7_listener($event) { return ctx.login = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "How is my Coffee ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function InsideComputerLoginComponent_Template_input_ngModelChange_13_listener($event) { return ctx.password = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, InsideComputerLoginComponent_div_15_Template, 2, 0, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.login);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.password);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.wrongAnswer);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_6__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]], styles: ["#macbook-login-bg[_ngcontent-%COMP%] {\n  background-color: black;\n  height: 100%;\n  width: 100%;\n}\n#macbook-login[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 500px;\n  top: 0;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/macbookpro-login.png');\n  background-size: 100% 100%;\n}\n#login-interface[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 457px;\n  top: 43px;\n  left: 28px;\n  width: 842px;\n  background-image: url('/escape-game-space-invaders/login-interface.png');\n  background-size: 100% 100%;\n}\n.login-form[_ngcontent-%COMP%] {\n  position: absolute;\n  left: 375px;\n  top: 208px;\n  font-size: 12px;\n}\n.mat-form-field-appearance-legacy[_ngcontent-%COMP%]   .mat-form-field-infix[_ngcontent-%COMP%] {\n  padding: .2375em 0 !important;\n}\n.interline[_ngcontent-%COMP%] {\n  height: 11px;\n}\n.input-password[_ngcontent-%COMP%] {\n  top: -23px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 90px !important;\n}\n.login-button[_ngcontent-%COMP%] {\n  height: 20px;\n  width: 20px;\n  position: absolute;\n  left: 94px;\n  top: 49px;\n  background: none;\n  border: none;\n}\n.wrongAnswer[_ngcontent-%COMP%] {\n  top: 64px;\n  position: absolute;\n  width: 400px;\n  color: red;\n}\n.goToBottom[_ngcontent-%COMP%] {\n  height: 250px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5zaWRlLWNvbXB1dGVyLWxvZ2luL2luc2lkZS1jb21wdXRlci1sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUJBQXVCO0VBQ3ZCLFlBQVk7RUFDWixXQUFXO0FBQ2I7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsTUFBTTtFQUNOLFdBQVc7RUFDWCx5RUFBb0Q7RUFDcEQsMEJBQTBCO0FBQzVCO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFNBQVM7RUFDVCxVQUFVO0VBQ1YsWUFBWTtFQUNaLHdFQUFtRDtFQUNuRCwwQkFBMEI7QUFDNUI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsVUFBVTtFQUNWLGVBQWU7QUFDakI7QUFFQTtFQUNFLDZCQUE2QjtBQUMvQjtBQUNBO0VBQ0UsWUFBWTtBQUNkO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFNBQVM7RUFDVCxnQkFBZ0I7RUFDaEIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixVQUFVO0FBQ1o7QUFDQTtFQUNFLGFBQWE7QUFDZiIsImZpbGUiOiJzcmMvYXBwL2luc2lkZS1jb21wdXRlci1sb2dpbi9pbnNpZGUtY29tcHV0ZXItbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNtYWNib29rLWxvZ2luLWJnIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4jbWFjYm9vay1sb2dpbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiA1MDBweDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL21hY2Jvb2twcm8tbG9naW4ucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cblxuI2xvZ2luLWludGVyZmFjZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiA0NTdweDtcbiAgdG9wOiA0M3B4O1xuICBsZWZ0OiAyOHB4O1xuICB3aWR0aDogODQycHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9sb2dpbi1pbnRlcmZhY2UucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cblxuLmxvZ2luLWZvcm0ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDM3NXB4O1xuICB0b3A6IDIwOHB4O1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5tYXQtZm9ybS1maWVsZC1hcHBlYXJhbmNlLWxlZ2FjeSAubWF0LWZvcm0tZmllbGQtaW5maXgge1xuICBwYWRkaW5nOiAuMjM3NWVtIDAgIWltcG9ydGFudDtcbn1cbi5pbnRlcmxpbmUge1xuICBoZWlnaHQ6IDExcHg7XG59XG4uaW5wdXQtcGFzc3dvcmQge1xuICB0b3A6IC0yM3B4O1xufVxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDkwcHggIWltcG9ydGFudDtcbn1cbi5sb2dpbi1idXR0b24ge1xuICBoZWlnaHQ6IDIwcHg7XG4gIHdpZHRoOiAyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDk0cHg7XG4gIHRvcDogNDlweDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgYm9yZGVyOiBub25lO1xufVxuLndyb25nQW5zd2VyIHtcbiAgdG9wOiA2NHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA0MDBweDtcbiAgY29sb3I6IHJlZDtcbn1cbi5nb1RvQm90dG9tIHtcbiAgaGVpZ2h0OiAyNTBweDtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InsideComputerLoginComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-inside-computer-login',
                templateUrl: './inside-computer-login.component.html',
                styleUrls: ['./inside-computer-login.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/inside-computer-secured/inside-computer-secured.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/inside-computer-secured/inside-computer-secured.component.ts ***!
  \******************************************************************************/
/*! exports provided: InsideComputerSecuredComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsideComputerSecuredComponent", function() { return InsideComputerSecuredComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






function InsideComputerSecuredComponent_img_2_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideComputerSecuredComponent_img_2_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.getBandAid(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function InsideComputerSecuredComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r1.getTopSecretCode(), " ");
} }
function InsideComputerSecuredComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 11);
} }
const _c0 = function () { return ["/livingroom4"]; };
class InsideComputerSecuredComponent {
    constructor(gameService) {
        this.gameService = gameService;
        this.SAVE_KEY = "dataSavedDuringInvaderGame";
        this.showPostIt = false;
        this.loadDataIfExist();
        if (localStorage.getItem("hasWonSpaceInvader") == "true") {
            localStorage.removeItem("hasWonSpaceInvader");
            this.gameService.addToUsedItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SPACE_INVADERS_WON, true);
        }
    }
    ngOnInit() {
    }
    goToSpaceInvaderGame() {
        this.saveCurrentDataBeforeInvaderGame();
        location.href = location.pathname + "vintage-space-invaders";
    }
    getTopSecretCode() {
        return this.gameService.getTopSecretCode();
    }
    getBandAid() {
        this.gameService.addTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].BANDAID_INVADER);
    }
    isBandAidTaken() {
        return this.gameService.hasTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].BANDAID_INVADER);
    }
    isTopSecretOpen() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SPACE_INVADERS_WON);
    }
    openToSecret() {
        if (!this.isTopSecretOpen()) {
            return;
        }
        this.showPostIt = true;
    }
    loadDataIfExist() {
        let item = localStorage.getItem(this.SAVE_KEY);
        if (item) {
            this.gameService.loadData(item);
            localStorage.removeItem(this.SAVE_KEY);
        }
    }
    saveCurrentDataBeforeInvaderGame() {
        localStorage.setItem(this.SAVE_KEY, JSON.stringify(this.gameService.getData()));
    }
}
InsideComputerSecuredComponent.ɵfac = function InsideComputerSecuredComponent_Factory(t) { return new (t || InsideComputerSecuredComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
InsideComputerSecuredComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: InsideComputerSecuredComponent, selectors: [["app-inside-computer-secured"]], decls: 8, vars: 5, consts: [[1, "fullbg", "macbook-bg"], [1, "space-invader-game-icon", "pointer", 3, "click"], ["src", "assets/images/trophies/bandaid-invader.png", "class", "bandaid-invader pointer", 3, "click", 4, "ngIf"], [1, "top-secret", "pointer", 3, "click"], ["class", "top-secret-post-it", 4, "ngIf"], ["class", "lock", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], ["src", "assets/images/trophies/bandaid-invader.png", 1, "bandaid-invader", "pointer", 3, "click"], [1, "top-secret-post-it"], [1, "inner-post-it"], [1, "lock"]], template: function InsideComputerSecuredComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideComputerSecuredComponent_Template_div_click_1_listener() { return ctx.goToSpaceInvaderGame(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, InsideComputerSecuredComponent_img_2_Template, 1, 0, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideComputerSecuredComponent_Template_div_click_3_listener() { return ctx.openToSecret(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, InsideComputerSecuredComponent_div_4_Template, 3, 1, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, InsideComputerSecuredComponent_div_5_Template, 1, 0, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isBandAidTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showPostIt);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isTopSecretOpen());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]], styles: [".macbook-bg[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/macbook-bg.jpg');\n}\n\n.bandaid-invader[_ngcontent-%COMP%] {\n  height: 200px;\n  left: 360px;\n  top: 209px;\n  position: absolute;\n  transform: rotate(101deg);\n  z-index: 2000;\n}\n\n.space-invader-game-icon[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/space-invader-icon.png');\n  height: 75px;\n  width: 75px;\n  background-size: 100% 100%;\n  left: 395px;\n  top: 269px;\n  position: absolute;\n  z-index: 1000;\n}\n\n.top-secret[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/top-secret.png');\n  height: 160px;\n  width: 200px;\n  background-size: 100% 100%;\n  top: 70px;\n  left: 50px;\n  position: absolute;\n}\n\n.lock[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/lock.png');\n  height: 160px;\n  width: 160px;\n  background-size: 100% 100%;\n  top: 50px;\n  left: 160px;\n  position: absolute;\n  z-index:  5000;\n}\n\n.top-secret-post-it[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/post-it.png');\n  height: 160px;\n  width: 160px;\n  background-size: 100% 100%;\n  top: 100px;\n  left: 80px;\n  position: absolute;\n}\n\n.inner-post-it[_ngcontent-%COMP%] {\n  left: 50px;\n  position: absolute;\n  top: 66px;\n  transform: rotate(-32deg);\n  font-family: fantasy;\n  letter-spacing: 3px;\n  font-size: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5zaWRlLWNvbXB1dGVyLXNlY3VyZWQvaW5zaWRlLWNvbXB1dGVyLXNlY3VyZWQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1FQUE4QztBQUNoRDs7QUFFQTtFQUNFLGFBQWE7RUFDYixXQUFXO0VBQ1gsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQix5QkFBeUI7RUFDekIsYUFBYTtBQUNmOztBQUNBO0VBQ0UsMkVBQXNEO0VBQ3RELFlBQVk7RUFDWixXQUFXO0VBQ1gsMEJBQTBCO0VBQzFCLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLGFBQWE7QUFDZjs7QUFDQTtFQUNFLG1FQUE4QztFQUM5QyxhQUFhO0VBQ2IsWUFBWTtFQUNaLDBCQUEwQjtFQUMxQixTQUFTO0VBQ1QsVUFBVTtFQUNWLGtCQUFrQjtBQUNwQjs7QUFDQTtFQUNFLDZEQUF3QztFQUN4QyxhQUFhO0VBQ2IsWUFBWTtFQUNaLDBCQUEwQjtFQUMxQixTQUFTO0VBQ1QsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixjQUFjO0FBQ2hCOztBQUNBO0VBQ0UsZ0VBQTJDO0VBQzNDLGFBQWE7RUFDYixZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLFVBQVU7RUFDVixVQUFVO0VBQ1Ysa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QseUJBQXlCO0VBQ3pCLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsZUFBZTtBQUNqQiIsImZpbGUiOiJzcmMvYXBwL2luc2lkZS1jb21wdXRlci1zZWN1cmVkL2luc2lkZS1jb21wdXRlci1zZWN1cmVkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFjYm9vay1iZyB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9tYWNib29rLWJnLmpwZ1wiKTtcbn1cblxuLmJhbmRhaWQtaW52YWRlciB7XG4gIGhlaWdodDogMjAwcHg7XG4gIGxlZnQ6IDM2MHB4O1xuICB0b3A6IDIwOXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zZm9ybTogcm90YXRlKDEwMWRlZyk7XG4gIHotaW5kZXg6IDIwMDA7XG59XG4uc3BhY2UtaW52YWRlci1nYW1lLWljb24ge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvc3BhY2UtaW52YWRlci1pY29uLnBuZ1wiKTtcbiAgaGVpZ2h0OiA3NXB4O1xuICB3aWR0aDogNzVweDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGxlZnQ6IDM5NXB4O1xuICB0b3A6IDI2OXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDEwMDA7XG59XG4udG9wLXNlY3JldCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy90b3Atc2VjcmV0LnBuZ1wiKTtcbiAgaGVpZ2h0OiAxNjBweDtcbiAgd2lkdGg6IDIwMHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgdG9wOiA3MHB4O1xuICBsZWZ0OiA1MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4ubG9jayB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9sb2NrLnBuZ1wiKTtcbiAgaGVpZ2h0OiAxNjBweDtcbiAgd2lkdGg6IDE2MHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgdG9wOiA1MHB4O1xuICBsZWZ0OiAxNjBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAgNTAwMDtcbn1cbi50b3Atc2VjcmV0LXBvc3QtaXQge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvcG9zdC1pdC5wbmdcIik7XG4gIGhlaWdodDogMTYwcHg7XG4gIHdpZHRoOiAxNjBweDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHRvcDogMTAwcHg7XG4gIGxlZnQ6IDgwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5pbm5lci1wb3N0LWl0IHtcbiAgbGVmdDogNTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDY2cHg7XG4gIHRyYW5zZm9ybTogcm90YXRlKC0zMmRlZyk7XG4gIGZvbnQtZmFtaWx5OiBmYW50YXN5O1xuICBsZXR0ZXItc3BhY2luZzogM3B4O1xuICBmb250LXNpemU6IDI0cHg7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InsideComputerSecuredComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-inside-computer-secured',
                templateUrl: './inside-computer-secured.component.html',
                styleUrls: ['./inside-computer-secured.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/inside-cupboard-japan/inside-cupboard-japan.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/inside-cupboard-japan/inside-cupboard-japan.component.ts ***!
  \**************************************************************************/
/*! exports provided: InsideCupboardJapanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsideCupboardJapanComponent", function() { return InsideCupboardJapanComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






function InsideCupboardJapanComponent_img_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 17);
} }
function InsideCupboardJapanComponent_img_14_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideCupboardJapanComponent_img_14_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.takeRabbit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom3"]; };
class InsideCupboardJapanComponent {
    constructor(gameService) {
        this.gameService = gameService;
    }
    ngOnInit() {
    }
    shouldShowRabbit() {
        return !this.gameService.hasTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].RABBIT)
            && this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SUSHIS);
    }
    takeRabbit() {
        this.gameService.addTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].RABBIT);
    }
    clickOnCatJapanStatue() {
        if (this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SUSHIS)) {
            this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SUSHIS);
        }
    }
    shouldShowSushis() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SUSHIS);
    }
}
InsideCupboardJapanComponent.ɵfac = function InsideCupboardJapanComponent_Factory(t) { return new (t || InsideCupboardJapanComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
InsideCupboardJapanComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: InsideCupboardJapanComponent, selectors: [["app-inside-cupboard-japan"]], decls: 17, vars: 4, consts: [[1, "inside-wood-cupboard-right", "fullbg"], [1, "itembg", "itembg"], [1, "itembg", "fan"], [1, "itembg", "house"], [1, "itembg", "monster"], [1, "itembg", "rice"], [1, "itembg", "tree"], [1, "itembg", "sun"], [1, "itembg", "sign"], [1, "itembg", "mountain"], [1, "itembg", "lamp"], [1, "itembg", "cat-japan-statue"], [1, "itembg", "cat-japan-statue-click-zone", "pointer", 3, "click"], ["alt", "sushis", "src", "assets/images/items/sushis.png", "class", "sushis", 4, "ngIf"], ["alt", "rabbit", "src", "assets/images/trophies/rabbit.png", "class", "rabbit pointer", 3, "click", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], ["alt", "sushis", "src", "assets/images/items/sushis.png", 1, "sushis"], ["alt", "rabbit", "src", "assets/images/trophies/rabbit.png", 1, "rabbit", "pointer", 3, "click"]], template: function InsideCupboardJapanComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideCupboardJapanComponent_Template_div_click_12_listener() { return ctx.clickOnCatJapanStatue(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, InsideCupboardJapanComponent_img_13_Template, 1, 0, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, InsideCupboardJapanComponent_img_14_Template, 1, 0, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.shouldShowSushis());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.shouldShowRabbit());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]], styles: [".inside-wood-cupboard-right[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/inside-wood-cupboard-right.jpg');\n}\n\n.itembg[_ngcontent-%COMP%] {\n  background-size: 100% 100%;\n  height: 100px;\n  width: 100px;\n  position: absolute;\n}\n\n.fan[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/fan.png');\n  top: 500px;\n  left: 550px;\n}\n\n.house[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/house.png');\n  top: 340px;\n  left: 403px;\n}\n\n.monster[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/monster.png');\n  top: 220px;\n  left: 400px;\n}\n\n.rice[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/rice.png');\n  top: 500px;\n  left: 250px;\n}\n\n.lamp[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/lamp.png');\n  left: 400px;\n  top: 50px;\n}\n\n.sign[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/sign.png');\n  top: 223px;\n  left: 200px;\n}\n\n.sun[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/sun.png');\n  top: 230px;\n  left: 550px;\n}\n\n.tree[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/tree.png');\n  top: 340px;\n  left: 563px;\n}\n\n.branch[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/branch.png');\n}\n\n.mountain[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/mountain.png');\n  top: 340px;\n  left: 200px;\n}\n\n.cat-japan-statue[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/cat-japan-statue.png');\n  left: 350px;\n  top: 380px;\n  height: 200px;\n  width: 200px;\n}\n\n.cat-japan-statue-click-zone[_ngcontent-%COMP%] {\n  left: 350px;\n  top: 380px;\n  height: 200px;\n  width: 200px;\n  z-index: 6000;\n}\n\n.sushis[_ngcontent-%COMP%] {\n  top: 540px;\n  position: absolute;\n  left: 370px;\n}\n\n.rabbit[_ngcontent-%COMP%] {\n  top: 350px;\n  left: 50px;\n  height: 200px;\n  z-index: 5000;\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5zaWRlLWN1cGJvYXJkLWphcGFuL2luc2lkZS1jdXBib2FyZC1qYXBhbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUZBQThEO0FBQ2hFOztBQUVBO0VBQ0UsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixZQUFZO0VBQ1osa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UsNERBQXVDO0VBQ3ZDLFVBQVU7RUFDVixXQUFXO0FBQ2I7O0FBQ0E7RUFDRSw4REFBeUM7RUFDekMsVUFBVTtFQUNWLFdBQVc7QUFDYjs7QUFDQTtFQUNFLGdFQUEyQztFQUMzQyxVQUFVO0VBQ1YsV0FBVztBQUNiOztBQUNBO0VBQ0UsNkRBQXdDO0VBQ3hDLFVBQVU7RUFDVixXQUFXO0FBQ2I7O0FBQ0E7RUFDRSw2REFBd0M7RUFDeEMsV0FBVztFQUNYLFNBQVM7QUFDWDs7QUFDQTtFQUNFLDZEQUF3QztFQUN4QyxVQUFVO0VBQ1YsV0FBVztBQUNiOztBQUNBO0VBQ0UsNERBQXVDO0VBQ3ZDLFVBQVU7RUFDVixXQUFXO0FBQ2I7O0FBQ0E7RUFDRSw2REFBd0M7RUFDeEMsVUFBVTtFQUNWLFdBQVc7QUFDYjs7QUFDQTtFQUNFLCtEQUEwQztBQUM1Qzs7QUFDQTtFQUNFLGlFQUE0QztFQUM1QyxVQUFVO0VBQ1YsV0FBVztBQUNiOztBQUNBO0VBQ0UseUVBQW9EO0VBQ3BELFdBQVc7RUFDWCxVQUFVO0VBQ1YsYUFBYTtFQUNiLFlBQVk7QUFDZDs7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1YsYUFBYTtFQUNiLFlBQVk7RUFDWixhQUFhO0FBQ2Y7O0FBQ0E7RUFDRSxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLFdBQVc7QUFDYjs7QUFDQTtFQUNFLFVBQVU7RUFDVixVQUFVO0VBQ1YsYUFBYTtFQUNiLGFBQWE7RUFDYixrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9pbnNpZGUtY3VwYm9hcmQtamFwYW4vaW5zaWRlLWN1cGJvYXJkLWphcGFuLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5zaWRlLXdvb2QtY3VwYm9hcmQtcmlnaHQge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvaW5zaWRlLXdvb2QtY3VwYm9hcmQtcmlnaHQuanBnXCIpO1xufVxuXG4uaXRlbWJnIHtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLmZhbiB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9mYW4ucG5nXCIpO1xuICB0b3A6IDUwMHB4O1xuICBsZWZ0OiA1NTBweDtcbn1cbi5ob3VzZSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9ob3VzZS5wbmdcIik7XG4gIHRvcDogMzQwcHg7XG4gIGxlZnQ6IDQwM3B4O1xufVxuLm1vbnN0ZXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvbW9uc3Rlci5wbmdcIik7XG4gIHRvcDogMjIwcHg7XG4gIGxlZnQ6IDQwMHB4O1xufVxuLnJpY2Uge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvcmljZS5wbmdcIik7XG4gIHRvcDogNTAwcHg7XG4gIGxlZnQ6IDI1MHB4O1xufVxuLmxhbXAge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvbGFtcC5wbmdcIik7XG4gIGxlZnQ6IDQwMHB4O1xuICB0b3A6IDUwcHg7XG59XG4uc2lnbiB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9zaWduLnBuZ1wiKTtcbiAgdG9wOiAyMjNweDtcbiAgbGVmdDogMjAwcHg7XG59XG4uc3VuIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL3N1bi5wbmdcIik7XG4gIHRvcDogMjMwcHg7XG4gIGxlZnQ6IDU1MHB4O1xufVxuLnRyZWUge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvdHJlZS5wbmdcIik7XG4gIHRvcDogMzQwcHg7XG4gIGxlZnQ6IDU2M3B4O1xufVxuLmJyYW5jaCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9icmFuY2gucG5nXCIpO1xufVxuLm1vdW50YWluIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL21vdW50YWluLnBuZ1wiKTtcbiAgdG9wOiAzNDBweDtcbiAgbGVmdDogMjAwcHg7XG59XG4uY2F0LWphcGFuLXN0YXR1ZSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9jYXQtamFwYW4tc3RhdHVlLnBuZ1wiKTtcbiAgbGVmdDogMzUwcHg7XG4gIHRvcDogMzgwcHg7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAyMDBweDtcbn1cbi5jYXQtamFwYW4tc3RhdHVlLWNsaWNrLXpvbmUge1xuICBsZWZ0OiAzNTBweDtcbiAgdG9wOiAzODBweDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgd2lkdGg6IDIwMHB4O1xuICB6LWluZGV4OiA2MDAwO1xufVxuLnN1c2hpcyB7XG4gIHRvcDogNTQwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMzcwcHg7XG59XG4ucmFiYml0IHtcbiAgdG9wOiAzNTBweDtcbiAgbGVmdDogNTBweDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgei1pbmRleDogNTAwMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InsideCupboardJapanComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-inside-cupboard-japan',
                templateUrl: './inside-cupboard-japan.component.html',
                styleUrls: ['./inside-cupboard-japan.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/inside-wood-cupboard-left/inside-wood-cupboard-left.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/inside-wood-cupboard-left/inside-wood-cupboard-left.component.ts ***!
  \**********************************************************************************/
/*! exports provided: InsideWoodCupboardLeftComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsideWoodCupboardLeftComponent", function() { return InsideWoodCupboardLeftComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






function InsideWoodCupboardLeftComponent_div_1_tr_4_td_1_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideWoodCupboardLeftComponent_div_1_tr_4_td_1_Template_td_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const col_r6 = ctx.$implicit; const line_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit; const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r7.clickCell(line_r4, col_r6); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return [1, 2, 3]; };
function InsideWoodCupboardLeftComponent_div_1_tr_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, InsideWoodCupboardLeftComponent_div_1_tr_4_td_1_Template, 1, 0, "td", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
} }
function InsideWoodCupboardLeftComponent_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.currentPass, " ");
} }
const _c1 = function () { return [1, 2, 3, 4]; };
function InsideWoodCupboardLeftComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideWoodCupboardLeftComponent_div_1_Template_div_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r10.openSafe(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "table");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, InsideWoodCupboardLeftComponent_div_1_tr_4_Template, 2, 2, "tr", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, InsideWoodCupboardLeftComponent_div_1_div_5_Template, 2, 1, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.currentPass != "");
} }
function InsideWoodCupboardLeftComponent_div_2_img_1_Template(rf, ctx) { if (rf & 1) {
    const _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideWoodCupboardLeftComponent_div_2_img_1_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r14.takeSyringue(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function InsideWoodCupboardLeftComponent_div_2_img_2_Template(rf, ctx) { if (rf & 1) {
    const _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsideWoodCupboardLeftComponent_div_2_img_2_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r17); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r16.takeLaser(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function InsideWoodCupboardLeftComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, InsideWoodCupboardLeftComponent_div_2_img_1_Template, 1, 0, "img", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, InsideWoodCupboardLeftComponent_div_2_img_2_Template, 1, 0, "img", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r1.isSyringueTaken());
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r1.isLaserTaken());
} }
const _c2 = function () { return ["/livingroom3"]; };
class InsideWoodCupboardLeftComponent {
    constructor(gameService) {
        this.gameService = gameService;
        this.currentPass = "";
    }
    ngOnInit() {
    }
    clickCell(line, col) {
        let cellId = "" + (col + (line - 1) * 3);
        if (cellId == "10")
            cellId = 'A';
        if (cellId == "11")
            cellId = '0';
        if (cellId == "12")
            cellId = 'B';
        this.currentPass += cellId;
        if (this.currentPass == this.gameService.getTopSecretCode()) {
            this.gameService.addToUsedItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].IS_SAFE_OPENED, true);
        }
    }
    openSafe() {
        if (this.currentPass == this.gameService.getTopSecretCode()) {
            this.gameService.addToUsedItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].IS_SAFE_OPENED, true);
        }
        this.currentPass = "";
    }
    isSafeOpened() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].IS_SAFE_OPENED);
    }
    takeSyringue() {
        this.gameService.addTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SYRINGE_INVADER);
    }
    isSyringueTaken() {
        return this.gameService.hasTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SYRINGE_INVADER);
    }
    takeLaser() {
        this.gameService.addItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LASER);
    }
    isLaserTaken() {
        return this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LASER);
    }
}
InsideWoodCupboardLeftComponent.ɵfac = function InsideWoodCupboardLeftComponent_Factory(t) { return new (t || InsideWoodCupboardLeftComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
InsideWoodCupboardLeftComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: InsideWoodCupboardLeftComponent, selectors: [["app-inside-wood-cupboard-left"]], decls: 5, vars: 4, consts: [[1, "fullbg", "inside-wood-cupboard-left"], ["class", "safe-padnum-lock", 4, "ngIf"], ["class", "safe-opened", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], [1, "safe-padnum-lock"], [1, "handle-safe", "pointer", 3, "click"], [1, "padnum-container"], [4, "ngFor", "ngForOf"], ["class", "current-code", 4, "ngIf"], ["class", "padnum-cell pointer", 3, "click", 4, "ngFor", "ngForOf"], [1, "padnum-cell", "pointer", 3, "click"], [1, "current-code"], [1, "safe-opened"], ["src", "assets/images/trophies/syringe-invader.png", "class", "pointer", 3, "click", 4, "ngIf"], ["src", "assets/images/items/space-invader-laser.png", "class", "pointer", 3, "click", 4, "ngIf"], ["src", "assets/images/trophies/syringe-invader.png", 1, "pointer", 3, "click"], ["src", "assets/images/items/space-invader-laser.png", 1, "pointer", 3, "click"]], template: function InsideWoodCupboardLeftComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, InsideWoodCupboardLeftComponent_div_1_Template, 6, 3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, InsideWoodCupboardLeftComponent_div_2_Template, 3, 2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isSafeOpened());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isSafeOpened());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c2));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"]], styles: [".inside-wood-cupboard-left[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/inside-wood-cupboard-left.jpg');\n}\n\n.safe-padnum-lock[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/safe-padnum-lock.png');\n  height: 400px;\n  width: 500px;\n  left: 150px;\n  top: 140px;\n  background-size: 100% 100%;\n  position: absolute;\n}\n\n.handle-safe[_ngcontent-%COMP%] {\n  height: 150px;\n  position: absolute;\n  width: 200px;\n  top: 150px;\n  left: 50px;\n}\n\n.padnum-container[_ngcontent-%COMP%] {\n  left: 296px;\n  top: 159px;\n  position: absolute;\n}\n\n.padnum-cell[_ngcontent-%COMP%] {\n  height: 23px;\n  width: 21px;\n}\n\n.padnum-container[_ngcontent-%COMP%]   table[_ngcontent-%COMP%] {\n  border-collapse: collapse;\n}\n\n.safe-opened[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/safeOpened.png');\n  height: 400px;\n  width: 650px;\n  left: 150px;\n  top: 140px;\n  background-size: 100% 100%;\n  position: absolute;\n}\n\n.safe-opened[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  height: 70px;\n  top: 200px;\n  left: 200px;\n  position: absolute;\n}\n\n.current-code[_ngcontent-%COMP%] {\n    background-color: rgba(200,200,200,0.4);\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    padding: 3px;\n    margin-top: 294px;\n    margin-left: 149px;\n    transform: rotate(-3deg);\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5zaWRlLXdvb2QtY3VwYm9hcmQtbGVmdC9pbnNpZGUtd29vZC1jdXBib2FyZC1sZWZ0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkVBQUU7RUFDQSxrRkFBNkQ7QUFDL0Q7O0FBRUE7RUFDRSx5RUFBb0Q7RUFDcEQsYUFBYTtFQUNiLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLDBCQUEwQjtFQUMxQixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixVQUFVO0VBQ1YsVUFBVTtBQUNaOztBQUNBO0VBQ0UsV0FBVztFQUNYLFVBQVU7RUFDVixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osV0FBVztBQUNiOztBQUNBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsbUVBQThDO0VBQzlDLGFBQWE7RUFDYixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDViwwQkFBMEI7RUFDMUIsa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDVixXQUFXO0VBQ1gsa0JBQWtCO0FBQ3BCOztBQUNFO0lBQ0UsdUNBQXVDO0lBQ3ZDLDBCQUFrQjtJQUFsQix1QkFBa0I7SUFBbEIsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHdCQUF3QjtFQUMxQiIsImZpbGUiOiJzcmMvYXBwL2luc2lkZS13b29kLWN1cGJvYXJkLWxlZnQvaW5zaWRlLXdvb2QtY3VwYm9hcmQtbGVmdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAuaW5zaWRlLXdvb2QtY3VwYm9hcmQtbGVmdCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9pbnNpZGUtd29vZC1jdXBib2FyZC1sZWZ0LmpwZ1wiKTtcbn1cblxuLnNhZmUtcGFkbnVtLWxvY2sge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvc2FmZS1wYWRudW0tbG9jay5wbmdcIik7XG4gIGhlaWdodDogNDAwcHg7XG4gIHdpZHRoOiA1MDBweDtcbiAgbGVmdDogMTUwcHg7XG4gIHRvcDogMTQwcHg7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4uaGFuZGxlLXNhZmUge1xuICBoZWlnaHQ6IDE1MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAyMDBweDtcbiAgdG9wOiAxNTBweDtcbiAgbGVmdDogNTBweDtcbn1cbi5wYWRudW0tY29udGFpbmVyIHtcbiAgbGVmdDogMjk2cHg7XG4gIHRvcDogMTU5cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5wYWRudW0tY2VsbCB7XG4gIGhlaWdodDogMjNweDtcbiAgd2lkdGg6IDIxcHg7XG59XG4ucGFkbnVtLWNvbnRhaW5lciB0YWJsZSB7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG59XG5cbi5zYWZlLW9wZW5lZCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9zYWZlT3BlbmVkLnBuZ1wiKTtcbiAgaGVpZ2h0OiA0MDBweDtcbiAgd2lkdGg6IDY1MHB4O1xuICBsZWZ0OiAxNTBweDtcbiAgdG9wOiAxNDBweDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5zYWZlLW9wZW5lZCBpbWcge1xuICBoZWlnaHQ6IDcwcHg7XG4gIHRvcDogMjAwcHg7XG4gIGxlZnQ6IDIwMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4gIC5jdXJyZW50LWNvZGUge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjAwLDIwMCwyMDAsMC40KTtcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XG4gICAgcGFkZGluZzogM3B4O1xuICAgIG1hcmdpbi10b3A6IDI5NHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxNDlweDtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgtM2RlZyk7XG4gIH1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InsideWoodCupboardLeftComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-inside-wood-cupboard-left',
                templateUrl: './inside-wood-cupboard-left.component.html',
                styleUrls: ['./inside-wood-cupboard-left.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/insidechestgold/insidechestgold.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/insidechestgold/insidechestgold.component.ts ***!
  \**************************************************************/
/*! exports provided: InsidechestgoldComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsidechestgoldComponent", function() { return InsidechestgoldComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






function InsidechestgoldComponent_img_1_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsidechestgoldComponent_img_1_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.getScrewDriver(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function InsidechestgoldComponent_img_2_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsidechestgoldComponent_img_2_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.addHeart(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom4"]; };
class InsidechestgoldComponent {
    constructor(gameService) {
        this.gameService = gameService;
    }
    ngOnInit() {
    }
    getScrewDriver() {
        this.gameService.addItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SCREWDRIVER);
    }
    hasScrewDriverBeenTaken() {
        return this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SCREWDRIVER);
    }
    hasHeartBeenTaken() {
        return this.gameService.hasTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].HEART);
    }
    addHeart() {
        this.gameService.addTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].HEART);
    }
}
InsidechestgoldComponent.ɵfac = function InsidechestgoldComponent_Factory(t) { return new (t || InsidechestgoldComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
InsidechestgoldComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: InsidechestgoldComponent, selectors: [["app-insidechestgold"]], decls: 5, vars: 4, consts: [[1, "inside-chest"], ["class", "screwdriver", "src", "assets/images/items/screwdriver.png", 3, "click", 4, "ngIf"], ["class", "heart", "src", "assets/images/trophies/heart.png", 3, "click", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], ["src", "assets/images/items/screwdriver.png", 1, "screwdriver", 3, "click"], ["src", "assets/images/trophies/heart.png", 1, "heart", 3, "click"]], template: function InsidechestgoldComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, InsidechestgoldComponent_img_1_Template, 1, 0, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, InsidechestgoldComponent_img_2_Template, 1, 0, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasScrewDriverBeenTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasHeartBeenTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]], styles: [".inside-chest[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/inside-chest.jpg');\n  background-size: 100% 100%;\n}\n.screwdriver[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100px;\n  left: 400px;\n  top: 400px;\n}\n.heart[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100px;\n  left: 630px;\n  top: 500px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5zaWRlY2hlc3Rnb2xkL2luc2lkZWNoZXN0Z29sZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gscUVBQWdEO0VBQ2hELDBCQUEwQjtBQUM1QjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixXQUFXO0VBQ1gsVUFBVTtBQUNaO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFdBQVc7RUFDWCxVQUFVO0FBQ1oiLCJmaWxlIjoic3JjL2FwcC9pbnNpZGVjaGVzdGdvbGQvaW5zaWRlY2hlc3Rnb2xkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5zaWRlLWNoZXN0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvaW5zaWRlLWNoZXN0LmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG59XG4uc2NyZXdkcml2ZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwcHg7XG4gIGxlZnQ6IDQwMHB4O1xuICB0b3A6IDQwMHB4O1xufVxuLmhlYXJ0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBsZWZ0OiA2MzBweDtcbiAgdG9wOiA1MDBweDtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InsidechestgoldComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-insidechestgold',
                templateUrl: './insidechestgold.component.html',
                styleUrls: ['./insidechestgold.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/insidecupboardleftinner/insidecupboardleftinner.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/insidecupboardleftinner/insidecupboardleftinner.component.ts ***!
  \******************************************************************************/
/*! exports provided: InsidecupboardleftinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsidecupboardleftinnerComponent", function() { return InsidecupboardleftinnerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






function InsidecupboardleftinnerComponent_img_1_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsidecupboardleftinnerComponent_img_1_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.takeRemote(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function InsidecupboardleftinnerComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InsidecupboardleftinnerComponent_div_2_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.takeGrimoire(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom2"]; };
class InsidecupboardleftinnerComponent {
    constructor(gameService) {
        this.gameService = gameService;
    }
    ngOnInit() {
    }
    takeRemote() {
        this.gameService.addItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].REMOTE);
    }
    hasRemoteBeenTaken() {
        return this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].REMOTE);
    }
    hasGrimoireBeenTaken() {
        return this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].GRIMOIRE);
    }
    takeGrimoire() {
        this.gameService.currentItems[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].GRIMOIRE] = {
            zoomUrl: "/read-grimoire"
        };
    }
}
InsidecupboardleftinnerComponent.ɵfac = function InsidecupboardleftinnerComponent_Factory(t) { return new (t || InsidecupboardleftinnerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
InsidecupboardleftinnerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: InsidecupboardleftinnerComponent, selectors: [["app-insidecupboardleftinner"]], decls: 5, vars: 4, consts: [["id", "inside-cupboard-left-inner"], ["src", "assets/images/items/remote.png", "class", "remote", 3, "click", 4, "ngIf"], ["class", "grimoire pointer", 3, "click", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], ["src", "assets/images/items/remote.png", 1, "remote", 3, "click"], [1, "grimoire", "pointer", 3, "click"]], template: function InsidecupboardleftinnerComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, InsidecupboardleftinnerComponent_img_1_Template, 1, 0, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, InsidecupboardleftinnerComponent_div_2_Template, 1, 0, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasRemoteBeenTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasGrimoireBeenTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]], styles: ["#inside-cupboard-left-inner[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/emptycupboard.jpg');\n  background-size: 100% 100%;\n}\n.remote[_ngcontent-%COMP%] {\n  height: 150px;\n  position: absolute;\n  left: 250px;\n  top: 334px;\n  transform: rotate(-38deg);\n  z-index: 1000;\n}\n.grimoire[_ngcontent-%COMP%] {\n  height: 90px;\n  width: 120px;\n  background-image: url('/escape-game-space-invaders/grimoire.png');\n  background-size: 100% 100%;\n  z-index: 120;\n  left: 378px;\n  top: 547px;\n  position: absolute;\n  transform: rotate(12deg);\n  z-index: 9500;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5zaWRlY3VwYm9hcmRsZWZ0aW5uZXIvaW5zaWRlY3VwYm9hcmRsZWZ0aW5uZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLHNFQUFpRDtFQUNqRCwwQkFBMEI7QUFDNUI7QUFDQTtFQUNFLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFVBQVU7RUFDVix5QkFBeUI7RUFDekIsYUFBYTtBQUNmO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlFQUErRDtFQUMvRCwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixhQUFhO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9pbnNpZGVjdXBib2FyZGxlZnRpbm5lci9pbnNpZGVjdXBib2FyZGxlZnRpbm5lci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2luc2lkZS1jdXBib2FyZC1sZWZ0LWlubmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvZW1wdHljdXBib2FyZC5qcGdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuLnJlbW90ZSB7XG4gIGhlaWdodDogMTUwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMjUwcHg7XG4gIHRvcDogMzM0cHg7XG4gIHRyYW5zZm9ybTogcm90YXRlKC0zOGRlZyk7XG4gIHotaW5kZXg6IDEwMDA7XG59XG4uZ3JpbW9pcmUge1xuICBoZWlnaHQ6IDkwcHg7XG4gIHdpZHRoOiAxMjBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ltYWdlcy9pdGVtcy9ncmltb2lyZS5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMjA7XG4gIGxlZnQ6IDM3OHB4O1xuICB0b3A6IDU0N3B4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zZm9ybTogcm90YXRlKDEyZGVnKTtcbiAgei1pbmRleDogOTUwMDtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InsidecupboardleftinnerComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-insidecupboardleftinner',
                templateUrl: './insidecupboardleftinner.component.html',
                styleUrls: ['./insidecupboardleftinner.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/items/items.component.ts":
/*!******************************************!*\
  !*** ./src/app/items/items.component.ts ***!
  \******************************************/
/*! exports provided: ItemToHold, ItemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemToHold", function() { return ItemToHold; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsComponent", function() { return ItemsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");










function ItemsComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ItemsComponent_div_0_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.closePage(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " X\n");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function (a0, a1, a2) { return { "top.px": a0, "left.px": a1, "background-image": a2 }; };
function ItemsComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 8);
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](1, _c0, ctx_r1.currentItemToHold.y, ctx_r1.currentItemToHold.x, "url(assets/images/items/" + ctx_r1.currentItemToHold.name + ".png)"));
} }
function ItemsComponent_mat_icon_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "music_off");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ItemsComponent_mat_icon_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "audiotrack");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ItemsComponent_div_10_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ItemsComponent_div_10_div_2_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11); const item_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit; const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.clickMagnifyingGlass(item_r7.key); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ItemsComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ItemsComponent_div_10_Template_div_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const item_r7 = ctx.$implicit; const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r12.selectItem(item_r7.key); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ItemsComponent_div_10_div_2_Template, 1, 0, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r7 = ctx.$implicit;
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("selectedItem", ctx_r4.isItemSelected(item_r7.key));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r4.isZoomabledItem(item_r7.key));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/items/", item_r7.key, ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
class ItemToHold {
}
class ItemsComponent {
    constructor(gameService, router, route, _location) {
        this.gameService = gameService;
        this.router = router;
        this.route = route;
        this._location = _location;
        this.pageCloseable = [
            '/emptydrawer', '/livingroom3drawer1', '/livingroom1drawerPurple',
            '/phone-invaders', '/inside-cupboard-left-inner',
            '/watching-tv', "/read-grimoire", '/inside-wood-cupboard-left', '/japan-expo'
        ];
        this.itemsToHold = [
            {
                name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].KEY_DRAWER1,
                location: "/livingroom1",
                rootEl: "#livingroom1-root",
                x: 0,
                y: 0,
            },
            {
                name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].OLD_GOLD_KEY,
                location: "/livingroom1",
                rootEl: "#livingroom1-root",
                x: 0,
                y: 0,
            },
            {
                name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].REMOTE,
                location: "/livingroom3",
                rootEl: "#livingroom3-root",
                x: 0,
                y: 0,
            },
            {
                name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SCREWDRIVER,
                location: "/livingroom2",
                rootEl: "#livingroom2-root",
                x: 0,
                y: 0,
            },
            {
                name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SUSHIS,
                location: "/japan-expo",
                rootEl: ".inside-wood-cupboard-right",
                x: 0,
                y: 0,
            }
        ];
        this.hasItemToHold = false;
    }
    ngOnInit() {
    }
    getItems() {
        return this.gameService.getCurrentItems();
    }
    selectItem(name) {
        this.gameService.selectItem(name);
    }
    isItemSelected(name) {
        return this.gameService.isItemSelected(name);
    }
    isZoomabledItem(key) {
        return this.getItems()[key].zoomUrl;
    }
    clickMagnifyingGlass(key) {
        if (this.router.url === this.getItems()[key].zoomUrl) {
            this._location.back();
        }
        else {
            this.router.navigate([this.getItems()[key].zoomUrl]);
        }
    }
    onMouseMove(e) {
        let itemToHold = false;
        for (let i = 0; i < this.itemsToHold.length; i++) {
            let currentItem = this.itemsToHold[i];
            if (this.isItemSelected(currentItem.name) &&
                this.router.url == currentItem.location) {
                itemToHold = true;
                this.holdItem(currentItem, e);
            }
        }
        this.hasItemToHold = itemToHold;
    }
    holdItem(itemsToHold, e) {
        let rootEl = jquery__WEBPACK_IMPORTED_MODULE_2__(itemsToHold.rootEl);
        this.currentItemToHold = itemsToHold;
        this.currentItemToHold.x = e.pageX - rootEl.offset().left - rootEl.width() - 25;
        this.currentItemToHold.y = e.pageY - 80;
    }
    resetGame() {
        this.gameService.resetGame();
    }
    closePage() {
        this._location.back();
    }
    showClosePage() {
        return this.pageCloseable.indexOf(this.router.url) > -1;
    }
    playOrPauseCurrentMusic() {
        this.gameService.playOrPauseCurrentMusic();
    }
    isMusicPlaying() {
        return this.gameService.isCurrentMusicPlaying();
    }
}
ItemsComponent.ɵfac = function ItemsComponent_Factory(t) { return new (t || ItemsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"])); };
ItemsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ItemsComponent, selectors: [["app-items"]], hostBindings: function ItemsComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mousemove", function ItemsComponent_mousemove_HostBindingHandler($event) { return ctx.onMouseMove($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveDocument"]);
    } }, decls: 12, vars: 7, consts: [["class", "close-page pointer", 3, "click", 4, "ngIf"], ["class", "currentItemToHold", 3, "ngStyle", 4, "ngIf"], [1, "button", 3, "click"], ["mat-fab", "", "color", "primary", "aria-label", "Reset all Game", "matTooltip", "Reset All Game", "matTooltipPosition", "before"], ["mat-fab", "", "color", "primary", "aria-label", "Play Or Pause Current Music"], [4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "close-page", "pointer", 3, "click"], [1, "currentItemToHold", 3, "ngStyle"], [1, "item-container", "cursor", 3, "click"], ["class", "magnifying-glass cursor", 3, "click", 4, "ngIf"], [1, "image-item", "cursor", 3, "src"], [1, "magnifying-glass", "cursor", 3, "click"]], template: function ItemsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ItemsComponent_div_0_Template, 2, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ItemsComponent_div_1_Template, 1, 5, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ItemsComponent_Template_div_click_2_listener() { return ctx.resetGame(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "restore_page");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ItemsComponent_Template_div_click_6_listener() { return ctx.playOrPauseCurrentMusic(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, ItemsComponent_mat_icon_8_Template, 2, 0, "mat-icon", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, ItemsComponent_mat_icon_9_Template, 2, 0, "mat-icon", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, ItemsComponent_div_10_Template, 4, 4, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "keyvalue");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showClosePage());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.hasItemToHold);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isMusicPlaying());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isMusicPlaying());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 5, ctx.getItems()));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_7__["MatTooltip"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIcon"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgStyle"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["KeyValuePipe"]], styles: [".item-container[_ngcontent-%COMP%] {\n  position: relative;\n  margin-left: 5px;\n  margin-right: 5px;\n  height: 80px;\n  width: 80px;\n  border: 1px solid black;\n  background-color: rgb(40,40,40);\n  border-radius: 5px;\n  z-index: 5000;\n}\n\n.image-item[_ngcontent-%COMP%] {\n  display:  block;\n  max-width: 80%;\n  max-height: 80%;\n  margin: auto auto;\n  padding: 5px;\n}\n\n.selectedItem[_ngcontent-%COMP%] {\n  border: 3px white solid;\n}\n\n.currentItemToHold[_ngcontent-%COMP%] {\n  position: absolute;\n  z-index: 5000;\n  height: 50px;\n  width: 50px;\n  background-size: 100% 100%;\n  cursor: pointer\n}\n\n.button[_ngcontent-%COMP%] {\n  position: relative;\n  display: inline-block;\n  z-index: 9500;\n}\n\n.magnifying-glass[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/magnifying-glass.png');\n  background-size: 100% 100%;\n  height: 40px;\n  position: absolute;\n  right: 0;\n  width: 40px;\n  z-index: 100;\n}\n\n.my-tooltip[_ngcontent-%COMP%] {\n  position: relative;\n  color: red !important;\n  z-index: 9000 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaXRlbXMvaXRlbXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixXQUFXO0VBQ1gsdUJBQXVCO0VBQ3ZCLCtCQUErQjtFQUMvQixrQkFBa0I7RUFDbEIsYUFBYTtBQUNmOztBQUVBO0VBQ0UsZUFBZTtFQUNmLGNBQWM7RUFDZCxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLFlBQVk7QUFDZDs7QUFFQTtFQUNFLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLFdBQVc7RUFDWCwwQkFBMEI7RUFDMUI7QUFDRjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsYUFBYTtBQUNmOztBQUVBO0VBQ0UseUVBQW9EO0VBQ3BELDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQix3QkFBd0I7QUFDMUIiLCJmaWxlIjoic3JjL2FwcC9pdGVtcy9pdGVtcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLml0ZW0tY29udGFpbmVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tbGVmdDogNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgaGVpZ2h0OiA4MHB4O1xuICB3aWR0aDogODBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYig0MCw0MCw0MCk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgei1pbmRleDogNTAwMDtcbn1cblxuLmltYWdlLWl0ZW0ge1xuICBkaXNwbGF5OiAgYmxvY2s7XG4gIG1heC13aWR0aDogODAlO1xuICBtYXgtaGVpZ2h0OiA4MCU7XG4gIG1hcmdpbjogYXV0byBhdXRvO1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbi5zZWxlY3RlZEl0ZW0ge1xuICBib3JkZXI6IDNweCB3aGl0ZSBzb2xpZDtcbn1cblxuLmN1cnJlbnRJdGVtVG9Ib2xkIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA1MDAwO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgY3Vyc29yOiBwb2ludGVyXG59XG5cbi5idXR0b24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgei1pbmRleDogOTUwMDtcbn1cblxuLm1hZ25pZnlpbmctZ2xhc3Mge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvbWFnbmlmeWluZy1nbGFzcy5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IDQwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDA7XG4gIHdpZHRoOiA0MHB4O1xuICB6LWluZGV4OiAxMDA7XG59XG4ubXktdG9vbHRpcCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgY29sb3I6IHJlZCAhaW1wb3J0YW50O1xuICB6LWluZGV4OiA5MDAwICFpbXBvcnRhbnQ7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ItemsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-items',
                templateUrl: './items.component.html',
                styleUrls: ['./items.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }, { type: _angular_common__WEBPACK_IMPORTED_MODULE_5__["Location"] }]; }, { onMouseMove: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['document:mousemove', ['$event']]
        }] }); })();


/***/ }),

/***/ "./src/app/jukebox/jukebox.component.ts":
/*!**********************************************!*\
  !*** ./src/app/jukebox/jukebox.component.ts ***!
  \**********************************************/
/*! exports provided: JukeboxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JukeboxComponent", function() { return JukeboxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");









function JukeboxComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " I only know 1 Song ! ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function JukeboxComponent_img_11_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function JukeboxComponent_img_11_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.takeInfirmaryTrophy(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom3"]; };
class JukeboxComponent {
    constructor(gameService) {
        this.gameService = gameService;
        this.wrongAnswer = false;
        this.showInfirmaryTrophy = false;
    }
    ngOnInit() {
    }
    launchSong() {
        if (this.song && this.gameService.getTheJukeBoxMusic().toLowerCase() === this.song.toLowerCase()) {
            this.wrongAnswer = false;
            this.gameService.playMusic("its-a-small-world-after.mp3");
            this.showInfirmaryTrophy = true;
        }
        else {
            this.wrongAnswer = true;
        }
    }
    takeInfirmaryTrophy() {
        this.gameService.addTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].INFIRMARY_INVADER);
    }
    shouldShowInfirmaryTrophy() {
        return this.showInfirmaryTrophy && !this.gameService.hasTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].INFIRMARY_INVADER);
    }
}
JukeboxComponent.ɵfac = function JukeboxComponent_Factory(t) { return new (t || JukeboxComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
JukeboxComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: JukeboxComponent, selectors: [["app-jukebox"]], decls: 12, vars: 5, consts: [["id", "jukebox-root"], [1, "wallColor", "wallBlock"], [1, "carpetColor", "carpetBlock"], [3, "routerLink"], [1, "goToBottom"], [1, "jukebox"], [1, "jukebox-form", 3, "submit"], [1, "song-login"], ["matInput", "", "placeholder", "XX'X X XXXXX XXXXX", "value", "", "name", "song", "autocomplete", "off", 3, "ngModel", "ngModelChange"], ["class", "wrongAnswer", 4, "ngIf"], ["type", "submit", 1, "playSongButton", "pointer"], ["src", "assets/images/trophies/infirmary-invader.png", "class", "pointer infirmary-trophy", 3, "click", 4, "ngIf"], [1, "wrongAnswer"], ["src", "assets/images/trophies/infirmary-invader.png", 1, "pointer", "infirmary-trophy", 3, "click"]], template: function JukeboxComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "form", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("submit", function JukeboxComponent_Template_form_submit_6_listener() { return ctx.launchSong(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function JukeboxComponent_Template_input_ngModelChange_8_listener($event) { return ctx.song = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, JukeboxComponent_div_9_Template, 2, 0, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, JukeboxComponent_img_11_Template, 1, 0, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.song);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.wrongAnswer);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.shouldShowInfirmaryTrophy());
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormField"], _angular_material_input__WEBPACK_IMPORTED_MODULE_6__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"]], styles: [".jukebox[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 600px;\n  width: 400px;\n  background-image: url('/escape-game-space-invaders/jukebox.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 250px;\n  bottom: 40px;\n}\n\n.jukebox-form[_ngcontent-%COMP%] {\n  background-color: #F0DB12;\n  box-shadow: -1px 2px 10px 3px rgba(0, 0, 0, 0.3) inset;\n  padding: 5px;\n  top: 241px;\n  left: 98px;\n  position: absolute;\n}\n\n.wrongAnswer[_ngcontent-%COMP%] {\n  color: darkviolet;\n}\n\n.playSongButton[_ngcontent-%COMP%] {\n  position: absolute;\n  left: 0px;\n  top: 100px;\n  width: 200px;\n  height: 150px;\n  background-color: transparent;\n  border: none;\n}\n\n.infirmary-trophy[_ngcontent-%COMP%] {\n  height: 100px;\n  z-index: 5000;\n  position: absolute;\n  bottom: 30px;\n  left: 400px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvanVrZWJveC9qdWtlYm94LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixnRUFBZ0U7RUFDaEUsMEJBQTBCO0VBQzFCLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLHNEQUFzRDtFQUN0RCxZQUFZO0VBQ1osVUFBVTtFQUNWLFVBQVU7RUFDVixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFVBQVU7RUFDVixZQUFZO0VBQ1osYUFBYTtFQUNiLDZCQUE2QjtFQUM3QixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvanVrZWJveC9qdWtlYm94LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuanVrZWJveCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiA2MDBweDtcbiAgd2lkdGg6IDQwMHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaW1hZ2VzL3ZhcmlvdXMvanVrZWJveC5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMDA7XG4gIGxlZnQ6IDI1MHB4O1xuICBib3R0b206IDQwcHg7XG59XG5cbi5qdWtlYm94LWZvcm0ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjBEQjEyO1xuICBib3gtc2hhZG93OiAtMXB4IDJweCAxMHB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMykgaW5zZXQ7XG4gIHBhZGRpbmc6IDVweDtcbiAgdG9wOiAyNDFweDtcbiAgbGVmdDogOThweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLndyb25nQW5zd2VyIHtcbiAgY29sb3I6IGRhcmt2aW9sZXQ7XG59XG4ucGxheVNvbmdCdXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDBweDtcbiAgdG9wOiAxMDBweDtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDE1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uaW5maXJtYXJ5LXRyb3BoeSB7XG4gIGhlaWdodDogMTAwcHg7XG4gIHotaW5kZXg6IDUwMDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAzMHB4O1xuICBsZWZ0OiA0MDBweDtcbn1cblxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](JukeboxComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-jukebox',
                templateUrl: './jukebox.component.html',
                styleUrls: ['./jukebox.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/landingpage/landingpage.component.ts":
/*!******************************************************!*\
  !*** ./src/app/landingpage/landingpage.component.ts ***!
  \******************************************************/
/*! exports provided: LandingpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingpageComponent", function() { return LandingpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");




const _c0 = function () { return ["/home"]; };
class LandingpageComponent {
    constructor() { }
    ngOnInit() {
    }
}
LandingpageComponent.ɵfac = function LandingpageComponent_Factory(t) { return new (t || LandingpageComponent)(); };
LandingpageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LandingpageComponent, selectors: [["app-landingpage"]], decls: 5, vars: 2, consts: [[1, "home-root"], [1, "launch-escape-game"], [3, "routerLink"], ["mat-raised-button", "", "color", "primary"]], template: function LandingpageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Launch Escape Game !");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButton"]], styles: [".home-root[_ngcontent-%COMP%] {\n  background-size: 950px 650px;\n  height: 100%;\n  background-color: rgb(10,10,10);\n  color: white;\n}\n\n.launch-escape-game[_ngcontent-%COMP%] {\n  text-align:center;\n  margin-left: auto;\n  margin-right: auto;\n  padding-top: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGFuZGluZ3BhZ2UvbGFuZGluZ3BhZ2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDRCQUE0QjtFQUM1QixZQUFZO0VBQ1osK0JBQStCO0VBQy9CLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL2xhbmRpbmdwYWdlL2xhbmRpbmdwYWdlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaG9tZS1yb290IHtcbiAgYmFja2dyb3VuZC1zaXplOiA5NTBweCA2NTBweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTAsMTAsMTApO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5sYXVuY2gtZXNjYXBlLWdhbWUge1xuICB0ZXh0LWFsaWduOmNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgcGFkZGluZy10b3A6IDMwMHB4O1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LandingpageComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-landingpage',
                templateUrl: './landingpage.component.html',
                styleUrls: ['./landingpage.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/livingroom-window/livingroom-window.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/livingroom-window/livingroom-window.component.ts ***!
  \******************************************************************/
/*! exports provided: LivingroomWindowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivingroomWindowComponent", function() { return LivingroomWindowComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _service_phone_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/phone.service */ "./src/app/service/phone.service.ts");








function LivingroomWindowComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LivingroomWindowComponent_div_5_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.takePicture(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function LivingroomWindowComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 14);
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("left", ctx_r1.invadersToSnap[ctx_r1.windowId].backpacker.x, "px")("top", ctx_r1.invadersToSnap[ctx_r1.windowId].backpacker.y, "px")("width", ctx_r1.invadersToSnap[ctx_r1.windowId].backpacker.width, "px")("height", ctx_r1.invadersToSnap[ctx_r1.windowId].backpacker.height, "px");
} }
function LivingroomWindowComponent_span_12_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/invaders/spaceInvaders-", el_r5.toLowerCase(), ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function LivingroomWindowComponent_span_12_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](el_r5);
} }
function LivingroomWindowComponent_span_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, LivingroomWindowComponent_span_12_span_1_Template, 2, 1, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, LivingroomWindowComponent_span_12_span_2_Template, 2, 1, "span", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r5 = ctx.$implicit;
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.inInvaderLetter(el_r5));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r2.inInvaderLetter(el_r5));
} }
class LivingroomWindowComponent {
    constructor(route, _location, gameService, phoneService) {
        this.route = route;
        this._location = _location;
        this.gameService = gameService;
        this.phoneService = phoneService;
        this.invadersToSnap = this.phoneService.getInvadersToSnap();
    }
    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.windowId = params.get('windowId');
        });
        jquery__WEBPACK_IMPORTED_MODULE_1__(".livingroom-window-root").on('mousemove', function (e) {
            jquery__WEBPACK_IMPORTED_MODULE_1__('.phone-for-picture').css({
                left: e.pageX - jquery__WEBPACK_IMPORTED_MODULE_1__(".livingroom-window-root").offset().left - 35,
                top: e.pageY - 100
            });
        });
        this.gameService.playMusic(this.getCurrentInvader().song);
    }
    backClicked() {
        this._location.back();
    }
    isPhoneSelected() {
        return this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].PHONE_ITEM);
    }
    snapInvader() {
        if (!this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].PHONE_ITEM)) {
            return;
        }
        this.takePicture();
        if (!this.phoneService.hadAlreadyBeenSnapped(this.windowId)) {
            this.phoneService.snapInvader(this.windowId, this.getCurrentInvader().score);
            this.showScore();
        }
    }
    getCurrentInvader() {
        return this.invadersToSnap[this.windowId];
    }
    showScore() {
        let pictureFlash = jquery__WEBPACK_IMPORTED_MODULE_1__("#scoreToAdd");
        pictureFlash.show();
        pictureFlash.fadeOut(5000, function () {
            jquery__WEBPACK_IMPORTED_MODULE_1__("#picture-flash").hide();
        });
    }
    takePicture() {
        let pictureFlash = jquery__WEBPACK_IMPORTED_MODULE_1__("#picture-flash");
        pictureFlash.show();
        pictureFlash.fadeOut("slow", function () {
            jquery__WEBPACK_IMPORTED_MODULE_1__("#picture-flash").hide();
        });
        this.gameService.playSound("appareil-photo.mp3");
    }
    inInvaderLetter(el) {
        return ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F'].indexOf(el) != -1;
    }
    playSong() {
        this.gameService.playOrPauseCurrentMusic();
    }
}
LivingroomWindowComponent.ɵfac = function LivingroomWindowComponent_Factory(t) { return new (t || LivingroomWindowComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_5__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_phone_service__WEBPACK_IMPORTED_MODULE_6__["PhoneService"])); };
LivingroomWindowComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LivingroomWindowComponent, selectors: [["app-livingroom-window"]], decls: 14, vars: 13, consts: [[1, "livingroom-window-root", "wallColor"], [1, "insideWindowImage", 3, "src"], [1, "openedWindow"], [3, "click"], [1, "pointer", "goToBottom"], ["class", "phone-for-picture", 3, "click", 4, "ngIf"], [1, "invader-to-pick-block", 3, "click"], ["class", "backpacker", 3, "left", "top", "width", "height", 4, "ngIf"], ["id", "scoreToAdd"], ["id", "picture-flash"], [1, "invader-text"], [4, "ngFor", "ngForOf"], [1, "play-music-button", "pointer", 3, "click"], [1, "phone-for-picture", 3, "click"], [1, "backpacker"], [4, "ngIf"], ["class", "letter", 4, "ngIf"], [1, "", 3, "src"], [1, "letter"]], template: function LivingroomWindowComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LivingroomWindowComponent_Template_a_click_3_listener() { return ctx.backClicked(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, LivingroomWindowComponent_div_5_Template, 1, 0, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LivingroomWindowComponent_Template_div_click_6_listener() { return ctx.snapInvader(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, LivingroomWindowComponent_div_7_Template, 1, 8, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, LivingroomWindowComponent_span_12_Template, 3, 2, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LivingroomWindowComponent_Template_div_click_13_listener() { return ctx.playSong(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/livingroom-windows/city-find-invader", ctx.windowId, ".jpg", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isPhoneSelected());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("left", ctx.invadersToSnap[ctx.windowId].left, "px")("top", ctx.invadersToSnap[ctx.windowId].top, "px")("width", ctx.invadersToSnap[ctx.windowId].width, "px")("height", ctx.invadersToSnap[ctx.windowId].height + 80, "px");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invadersToSnap[ctx.windowId].backpacker);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("+ ", ctx.invadersToSnap[ctx.windowId].score, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.invadersToSnap[ctx.windowId].legend.split(""));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"]], styles: [".livingroom-window-root[_ngcontent-%COMP%] {\n  height: 100%;\n  width: 100%;\n}\n\n.openedWindow[_ngcontent-%COMP%] {\n  height: 555px;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/window-opened.png');\n  background-size: 100% 100%;\n  z-index: 3000;\n  top: 50px;\n  position: absolute;\n}\n\n.insideWindowImage[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 135px;\n  left: 185px;\n  width: 513px;\n  height: 400px;\n}\n\n.play-music-button[_ngcontent-%COMP%] {\n  height: 70px;\n  width: 70px;\n  background-image: url('/escape-game-space-invaders/play-music-button.png');\n  background-size: 100% 100%;\n  z-index: 4000;\n  top: 550px;\n  left: 145px;\n  position: absolute;\n}\n\n.phone-for-picture[_ngcontent-%COMP%] {\n  height: 100px;\n  width: 75px;\n  background-image: url('/escape-game-space-invaders/phone-flash-invaders-taking-photo.png');\n  background-size: 100% 100%;\n  z-index: 5000;\n  top: 50px;\n  position: absolute;\n  cursor: crosshair;\n}\n\n.invader-to-pick-block[_ngcontent-%COMP%] {\n  z-index: 7000;\n  position: absolute;\n}\n\n#picture-flash[_ngcontent-%COMP%] {\n  position: absolute;\n  background: white;\n  height: 100%;\n  width: 100%;\n  z-index: 9500;\n  display: none;\n}\n\n#scoreToAdd[_ngcontent-%COMP%] {\n  color: orange;\n  display: none;\n  font-family: Atari, fantasy;\n  font-size: 100px;\n  font-weight: bold;\n  left: 300px;\n  top:  250px;\n  position: absolute;\n  z-index: 9600;\n}\n\n.invader-text[_ngcontent-%COMP%] {\n  position: absolute;\n  left: 210px;\n  bottom: 40px;\n}\n\n.invader-text[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  height: 41px;\n}\n\n.invader-text[_ngcontent-%COMP%]   .letter[_ngcontent-%COMP%] {\n  font-size: 48px;\n  color: black;\n}\n\n.backpacker[_ngcontent-%COMP%] {\n  background-size: 100% 100%;\n  background-image: url('/escape-game-space-invaders/backpacker.png');\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbS13aW5kb3cvbGl2aW5ncm9vbS13aW5kb3cuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQVk7RUFDWixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLHNFQUFpRDtFQUNqRCwwQkFBMEI7RUFDMUIsYUFBYTtFQUNiLFNBQVM7RUFDVCxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtBQUNmOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCwwRUFBcUQ7RUFDckQsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixVQUFVO0VBQ1YsV0FBVztFQUNYLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGFBQWE7RUFDYixXQUFXO0VBQ1gsMEZBQXFFO0VBQ3JFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsU0FBUztFQUNULGtCQUFrQjtFQUNsQixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxhQUFhO0VBQ2Isa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osV0FBVztFQUNYLGFBQWE7RUFDYixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsYUFBYTtFQUNiLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtBQUNkOztBQUNBO0VBQ0UsWUFBWTtBQUNkOztBQUNBO0VBQ0UsZUFBZTtFQUNmLFlBQVk7QUFDZDs7QUFDQTtFQUNFLDBCQUEwQjtFQUMxQixtRUFBbUU7RUFDbkUsa0JBQWtCO0FBQ3BCIiwiZmlsZSI6InNyYy9hcHAvbGl2aW5ncm9vbS13aW5kb3cvbGl2aW5ncm9vbS13aW5kb3cuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5saXZpbmdyb29tLXdpbmRvdy1yb290IHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm9wZW5lZFdpbmRvdyB7XG4gIGhlaWdodDogNTU1cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvd2luZG93LW9wZW5lZC5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAzMDAwO1xuICB0b3A6IDUwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLmluc2lkZVdpbmRvd0ltYWdlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEzNXB4O1xuICBsZWZ0OiAxODVweDtcbiAgd2lkdGg6IDUxM3B4O1xuICBoZWlnaHQ6IDQwMHB4O1xufVxuXG4ucGxheS1tdXNpYy1idXR0b24ge1xuICBoZWlnaHQ6IDcwcHg7XG4gIHdpZHRoOiA3MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvcGxheS1tdXNpYy1idXR0b24ucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgei1pbmRleDogNDAwMDtcbiAgdG9wOiA1NTBweDtcbiAgbGVmdDogMTQ1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLnBob25lLWZvci1waWN0dXJlIHtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDc1cHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy9waG9uZS1mbGFzaC1pbnZhZGVycy10YWtpbmctcGhvdG8ucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgei1pbmRleDogNTAwMDtcbiAgdG9wOiA1MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGN1cnNvcjogY3Jvc3NoYWlyO1xufVxuXG4uaW52YWRlci10by1waWNrLWJsb2NrIHtcbiAgei1pbmRleDogNzAwMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuI3BpY3R1cmUtZmxhc2gge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiA5NTAwO1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4jc2NvcmVUb0FkZCB7XG4gIGNvbG9yOiBvcmFuZ2U7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIGZvbnQtZmFtaWx5OiBBdGFyaSwgZmFudGFzeTtcbiAgZm9udC1zaXplOiAxMDBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGxlZnQ6IDMwMHB4O1xuICB0b3A6ICAyNTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5NjAwO1xufVxuXG4uaW52YWRlci10ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAyMTBweDtcbiAgYm90dG9tOiA0MHB4O1xufVxuLmludmFkZXItdGV4dCBpbWcge1xuICBoZWlnaHQ6IDQxcHg7XG59XG4uaW52YWRlci10ZXh0IC5sZXR0ZXIge1xuICBmb250LXNpemU6IDQ4cHg7XG4gIGNvbG9yOiBibGFjaztcbn1cbi5iYWNrcGFja2VyIHtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pbWFnZXMvdmFyaW91cy9iYWNrcGFja2VyLnBuZ1wiKTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LivingroomWindowComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-livingroom-window',
                templateUrl: './livingroom-window.component.html',
                styleUrls: ['./livingroom-window.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }, { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] }, { type: _service_game_service__WEBPACK_IMPORTED_MODULE_5__["GameService"] }, { type: _service_phone_service__WEBPACK_IMPORTED_MODULE_6__["PhoneService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/livingroom1/livingroom1.component.ts":
/*!******************************************************!*\
  !*** ./src/app/livingroom1/livingroom1.component.ts ***!
  \******************************************************/
/*! exports provided: Livingroom1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Livingroom1Component", function() { return Livingroom1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _model_Bullet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/Bullet */ "./src/app/model/Bullet.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");








function Livingroom1Component_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 20);
} }
function Livingroom1Component_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function Livingroom1Component_div_12_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom1Component_div_12_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.shootLaser(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function (a0, a1) { return { "top.px": a0, "left.px": a1 }; };
function Livingroom1Component_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 28);
} if (rf & 2) {
    const bullet_r13 = ctx.$implicit;
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](1, _c0, bullet_r13.y, bullet_r13.x + ctx_r3.BULLET_OFFSET_LEFT));
} }
const _c1 = function (a0, a1, a2, a3) { return { "top.px": a0, "left.px": a1, "width.px": a2, "height.px": a3 }; };
function Livingroom1Component_div_14_img_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 31);
} if (rf & 2) {
    const invader_r15 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/invaders/spaceInvaders-", invader_r15.letter.toLowerCase(), ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction4"](2, _c1, invader_r15.y, invader_r15.x, invader_r15.width, invader_r15.height));
} }
function Livingroom1Component_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, Livingroom1Component_div_14_img_2_Template, 1, 7, "img", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r4.getInvaders());
} }
function Livingroom1Component_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 32);
} }
const _c2 = function (a0, a1, a2, a3, a4, a5) { return { "top.px": a0, "left.px": a1, "width.px": a2, "height.px": a3, "background-image": a4, "background-color": a5 }; };
function Livingroom1Component_div_16_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom1Component_div_16_div_1_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const trophyPaint_r17 = ctx.$implicit; const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r18.placeTrophy(trophyPaint_r17); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const trophyPaint_r17 = ctx.$implicit;
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction6"](1, _c2, trophyPaint_r17.y, trophyPaint_r17.x, trophyPaint_r17.width, trophyPaint_r17.height, ctx_r16.getBgImage(trophyPaint_r17), ctx_r16.isOnPainting(trophyPaint_r17.name) ? "white" : ""));
} }
function Livingroom1Component_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, Livingroom1Component_div_16_div_1_Template, 1, 8, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.trophyOnPainting);
} }
function Livingroom1Component_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom1Component_div_17_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r20.takeOldGoldKey(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function Livingroom1Component_div_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c3 = function () { return ["/doorOpenedGameWon"]; };
function Livingroom1Component_div_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 49);
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c3));
} }
function Livingroom1Component_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c4 = function () { return ["/livingroom4"]; };
const _c5 = function () { return ["/livingroom2"]; };
const _c6 = function () { return ["/under-sofa-right"]; };
class Invader {
}
class Livingroom1Component {
    constructor(gameService, router) {
        this.gameService = gameService;
        this.router = router;
        this.invaders = [];
        this.bullets = [];
        this.currentX = 0;
        this.currentY = 0;
        this.Y_LASER_OFFSET = 100;
        this.BULLET_OFFSET_LEFT = 35;
        this.trophyOnPainting = [
            { x: 120, y: 46, height: 64, width: 32, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].NURSE_INVADER },
            { x: 50, y: 0, height: 70, width: 70, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].INFIRMARY_INVADER },
            { x: 120, y: 0, height: 46, width: 32, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].BANDAID_INVADER },
            { x: 0, y: 0, height: 50, width: 50, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SYRINGE_INVADER },
            { x: 90, y: 70, height: 40, width: 30, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].HEART },
            { x: 50, y: 70, height: 40, width: 40, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].CARROTS },
            { x: 0, y: 50, height: 60, width: 50, name: _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].RABBIT },
        ];
    }
    getLeftOffset() {
        return this.rootEl.offset().left;
    }
    ngOnInit() {
        if (this.isLaserGameOn()) {
            this.prepareLaserGame();
        }
        this.countVisit();
        this.openCurtain();
    }
    countVisit() {
        let usedItem = this.gameService.usedItems[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LIVING_ROOM1_COUNT_VISIT];
        if (!usedItem) {
            this.gameService.addToUsedItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LIVING_ROOM1_COUNT_VISIT, 0);
        }
        this.gameService.usedItems[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LIVING_ROOM1_COUNT_VISIT]++;
    }
    openCurtain() {
        if (this.areCurtainOpened()) {
            return;
        }
        this.gameService.playSound("mario-kart-race-start-gaming-sound-effect-hd.mp3");
        setTimeout(() => {
            jquery__WEBPACK_IMPORTED_MODULE_2__(".rnOuter").addClass("rnInnerOpen");
            this.gameService.useInNSecond(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].INTRO_DONE, true, 6000);
        }, 3500);
    }
    areCurtainOpened() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].INTRO_DONE)
            || this.gameService.usedItems[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LIVING_ROOM1_COUNT_VISIT] > 1;
    }
    openDrawer2() {
        if (this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].KEY_DRAWER1)) {
            this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].KEY_DRAWER1);
        }
        if (this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].KEY_DRAWER1)) {
            this.gameService.playSound("close_door.mp3");
            this.router.navigate(['/livingroom1drawerPurple']);
        }
    }
    prepareLaserGame() {
        this.rootEl = jquery__WEBPACK_IMPORTED_MODULE_2__("#livingroom1-root");
        this.rootEl.on('mousemove', (e) => {
            this.currentX = e.pageX;
            this.currentY = e.pageY;
            jquery__WEBPACK_IMPORTED_MODULE_2__('#space-invader-laser').css({
                left: e.pageX - this.getLeftOffset(),
                top: e.pageY - this.Y_LASER_OFFSET
            });
        });
        this.initInvaders();
        this.loopThrewBullets();
        this.makeExplosion();
    }
    makeExplosion() {
        if (!this.isLaserGameOn()) {
            return;
        }
        this.gameService.playSound("explosion.wav");
        setTimeout(this.makeExplosion.bind(this), Math.random() * 6000 + 1000);
    }
    isLaserSelected() {
        return this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LASER);
    }
    shootLaser() {
        this.gameService.playSound("shoot.wav");
        this.throwBullet();
    }
    throwBullet() {
        let bullet = new _model_Bullet__WEBPACK_IMPORTED_MODULE_3__["Bullet"](this.currentX - this.getLeftOffset(), this.currentY - this.Y_LASER_OFFSET);
        this.bullets.push(bullet);
    }
    loopThrewBullets() {
        let i = this.bullets.length;
        while (i--) {
            let bullet = this.bullets[i];
            bullet.y -= 1;
            if (bullet.y < 0 || this.touchingInvader(bullet)) {
                this.bullets.splice(i, 1);
            }
        }
        setTimeout(this.loopThrewBullets.bind(this), 2);
    }
    touchingInvader(bullet) {
        for (let i = 0; i < this.invaders.length; i++) {
            let invader = this.invaders[i];
            let bulletX = this.bulletXToInvaderX(bullet.x);
            let bulletY = this.bulletYToInvaderY(bullet);
            if (bulletX > invader.x
                && bulletX < invader.x + invader.width
                && bulletY < invader.y
                && bulletY > invader.y - invader.height) {
                this.killInvader(i);
                return true;
            }
        }
        return false;
    }
    killInvader(i) {
        this.invaders.splice(i, 1);
        this.gameService.playSound("fastinvader3.wav");
        if (this.invaders.length == 0) {
            this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LASER);
        }
    }
    bulletXToInvaderX(bulletX) {
        return bulletX - 418 + this.BULLET_OFFSET_LEFT;
    }
    bulletYToInvaderY(bullet) {
        return bullet.y - 95 - 13;
    }
    isLaserGameOn() {
        return this.gameService.isInCurrentItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LASER);
    }
    getInvaders() {
        return this.invaders;
    }
    initInvaders() {
        for (let i = 0; i < this.gameService.getInvaderLetters().length; i++) {
            let letter = this.gameService.getInvaderLetters()[i];
            let invader = new Invader();
            invader.letter = letter;
            invader.x = (i % 4) * 37;
            invader.y = (Math.floor(i / 4)) * 27;
            invader.height = 23;
            invader.width = 23;
            this.invaders.push(invader);
        }
    }
    isLaserGameWon() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LASER);
    }
    placeTrophy(trophyPaint) {
        if (trophyPaint.name == this.gameService.getSelectTrophy()) {
            this.gameService.trophies[trophyPaint.name].onPainting = true;
            this.gameService.selectedTrophyName = undefined;
            if (Object.keys(this.trophyOnPainting).length == this.countTrophiesOnPainting()) {
                this.gameService.addToUsedItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PAINTING_DONE, true);
                this.gameService.playSound("heavenly-music-gaming-sound-effect-hd.mp3");
            }
        }
    }
    countTrophiesOnPainting() {
        let count = 0;
        for (let trophy in this.gameService.trophies) {
            if (this.gameService.trophies[trophy].onPainting) {
                count++;
            }
        }
        return count;
    }
    isOnPainting(trophyName) {
        return this.gameService.isTrophyOnPainting(trophyName);
    }
    getBgImage(trophyPaint) {
        return this.isOnPainting(trophyPaint.name) ?
            "url(assets/images/trophies/" + trophyPaint.name + ".png)" : "";
    }
    takeOldGoldKey() {
        this.gameService.addItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].OLD_GOLD_KEY);
    }
    isOldKeyTaken() {
        return this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].OLD_GOLD_KEY);
    }
    isPaintingFinished() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PAINTING_DONE);
    }
    openDoor() {
        if (this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].OLD_GOLD_KEY)) {
            return this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].OLD_GOLD_KEY);
        }
    }
    hasGoldenKeyBeenUsed() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].OLD_GOLD_KEY);
    }
}
Livingroom1Component.ɵfac = function Livingroom1Component_Factory(t) { return new (t || Livingroom1Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_4__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"])); };
Livingroom1Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Livingroom1Component, selectors: [["app-livingroom1"]], decls: 22, vars: 17, consts: [["id", "livingroom1-root"], [3, "routerLink"], [1, "goToLeft"], [1, "goToRight"], [1, "under-sofa-right"], [1, "door", "door-closed"], ["class", "space-invaders", 4, "ngIf"], ["class", "rxWorld", 4, "ngIf"], [1, "key-lock"], [1, "livingroom1-drawer2", "pointer", 3, "click"], ["id", "space-invader-laser", 3, "click", 4, "ngIf"], ["class", "bullet", 3, "ngStyle", 4, "ngFor", "ngForOf"], ["class", "painting-white-bg", 4, "ngIf"], ["class", "cat-fear", 4, "ngIf"], ["id", "painting-bg", 4, "ngIf"], ["class", "old-gold-key pointer", 3, "click", 4, "ngIf"], ["class", "sun", 4, "ngIf"], [1, "doorKnob", "pointer", 3, "click"], ["class", "door door-opened pointer", 3, "routerLink", 4, "ngIf"], ["class", "sun2", 4, "ngIf"], [1, "space-invaders"], [1, "rxWorld"], [1, "rnOuter"], [1, "aoTable"], [1, "aoTableCell"], [1, "rnInner"], [1, "rnUnit"], ["id", "space-invader-laser", 3, "click"], [1, "bullet", 3, "ngStyle"], [1, "painting-white-bg"], ["class", "invader", 3, "ngStyle", "src", 4, "ngFor", "ngForOf"], [1, "invader", 3, "ngStyle", "src"], [1, "cat-fear"], ["id", "painting-bg"], ["class", "trophy-on-painting", 3, "ngStyle", "click", 4, "ngFor", "ngForOf"], [1, "trophy-on-painting", 3, "ngStyle", "click"], [1, "old-gold-key", "pointer", 3, "click"], [1, "sun"], [1, "ray_box"], [1, "ray", "ray1"], [1, "ray", "ray2"], [1, "ray", "ray3"], [1, "ray", "ray4"], [1, "ray", "ray5"], [1, "ray", "ray6"], [1, "ray", "ray7"], [1, "ray", "ray8"], [1, "ray", "ray9"], [1, "ray", "ray10"], [1, "door", "door-opened", "pointer", 3, "routerLink"], [1, "sun2"]], template: function Livingroom1Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, Livingroom1Component_div_8_Template, 1, 0, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, Livingroom1Component_div_9_Template, 15, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom1Component_Template_div_click_11_listener() { return ctx.openDrawer2(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, Livingroom1Component_div_12_Template, 1, 0, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, Livingroom1Component_div_13_Template, 1, 4, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, Livingroom1Component_div_14_Template, 3, 1, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, Livingroom1Component_div_15_Template, 1, 0, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, Livingroom1Component_div_16_Template, 2, 1, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, Livingroom1Component_div_17_Template, 1, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, Livingroom1Component_div_18_Template, 12, 0, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom1Component_Template_div_click_19_listener() { return ctx.openDoor(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, Livingroom1Component_div_20_Template, 1, 2, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, Livingroom1Component_div_21_Template, 12, 0, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c4));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](15, _c5));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](16, _c6));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isLaserGameOn());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.areCurtainOpened());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLaserSelected());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.bullets);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLaserGameOn());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLaserGameOn());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLaserGameWon());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isPaintingFinished() && !ctx.isOldKeyTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isPaintingFinished());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.hasGoldenKeyBeenUsed());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.hasGoldenKeyBeenUsed());
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgStyle"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLink"]], styles: ["#livingroom1-root[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/livingroom1.jpg');\n  background-size: 950px 650px;\n  height: 100%;\n}\n\n.door[_ngcontent-%COMP%] {\n  position: absolute;\n  background-size: 100% 100%;\n  height: 429px;\n  width: 136px;\n  left: 69px;\n  top: 50px;\n}\n\n.door-closed[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/door.png');\n}\n\n.door-opened[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/door-opened.png');\n  z-index: 2000;\n}\n\n.doorKnob[_ngcontent-%COMP%] {\n  height: 50px;\n  width: 50px;\n  left: 150px;\n  top: 270px;\n  position: absolute;\n  z-index: 6000;;\n}\n\n.key-lock[_ngcontent-%COMP%] {\n  position: absolute;\n  background-image: url('/escape-game-space-invaders/key-lock.png');\n  background-size: 100% 100%;\n  height: 18px;\n  width: 18px;\n  left: 239px;\n  top: 450px;\n  z-index: 1000;\n}\n\n.livingroom1-drawer2[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 450px;\n  width: 61px;\n  height: 19px;\n  left: 218px;\n  z-index: 6000;\n}\n\n.space-invaders[_ngcontent-%COMP%] {\n  position: absolute;\n  background-image: url('/escape-game-space-invaders/spaceInvaders.jpg');\n  background-size: 100% 100%;\n  height: 110px;\n  width: 154px;\n  left: 418px;\n  top: 95px;\n}\n\n.under-sofa-right[_ngcontent-%COMP%] {\n  left: 450px;\n  top: 500px;\n  height: 100px;\n  width: 250px;\n  position: absolute;\n}\n\n.keyDrawer1[_ngcontent-%COMP%] {\n\n}\n\n\n\n\n\n\n\n#space-invader-laser[_ngcontent-%COMP%] {\n  position: absolute;\n  background-image: url('/escape-game-space-invaders/space-invader-laser.png');\n  background-size: 100% 100%;\n  height: 60px;\n  width: 75px;\n  cursor: none;\n  z-index: 5000;\n}\n\n.bullet[_ngcontent-%COMP%] {\n  background-color: white;\n  height: 7px;\n  width: 1px;\n  position: absolute;\n  z-index: 2000;\n  border: 1px rgb(240,240,240) solid;\n}\n\n.painting-white-bg[_ngcontent-%COMP%] {\n  position: absolute;\n  background-color: white;\n  height: 110px;\n  width: 154px;\n  left: 418px;\n  top: 95px;\n}\n\n.invader[_ngcontent-%COMP%] {\n  margin-left: 4px;\n  position: absolute;\n}\n\n.cat-fear[_ngcontent-%COMP%] {\n  position: absolute;\n  background-image: url('/escape-game-space-invaders/cat-fear.png');\n  background-size: 100% 100%;\n  height: 110px;\n  width: 154px;\n  left: 418px;\n  top: 495px;\n}\n\n#painting-bg[_ngcontent-%COMP%] {\n  position: absolute;\n  background-image: url('/escape-game-space-invaders/painting-bg.png');\n  background-size: 100% 100%;\n  height: 110px;\n  width: 154px;\n  left: 418px;\n  top: 95px;\n}\n\n.trophy-on-painting[_ngcontent-%COMP%] {\n  position: absolute;\n  z-index: 5000;\n  background-size: 100% 100%;\n}\n\n.old-gold-key[_ngcontent-%COMP%] {\n  position: absolute;\n  background-image: url('/escape-game-space-invaders/old-golden-key.png');\n  background-size: 100% 100%;\n  height: 110px;\n  width: 154px;\n  left: 418px;\n  top: 95px;\n  z-index: 8000;\n}\n\n\n\nbody[_ngcontent-%COMP%] {\n  background:#2EB5E5 ;\n}\n\n.sun2[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 80px;\n  left: 90px;\n  margin: auto;\n  width: 70px;\n  height: 70px;\n  border-radius: 50%;\n  background: white;\n  opacity: 0.9;\n  box-shadow: 0px 0px 40px 15px white;\n}\n\n.sun[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 120px;\n  left: 460px;\n  margin: auto;\n  width: 70px;\n  height: 70px;\n  border-radius: 50%;\n  background: white;\n  opacity: 0.9;\n  box-shadow: 0px 0px 40px 15px white;\n}\n\n.ray_box[_ngcontent-%COMP%] {\n  position: absolute;\n  margin: auto;\n  top:0px;\n  left:0;\n  right:0;\n  bottom:0;\n  width:70px;\n  -webkit-animation: ray_anim 120s linear infinite;\n  animation: ray_anim 10s linear infinite;\n}\n\n.ray[_ngcontent-%COMP%] {\n  background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,0.8) 50%, rgba(255,255,255,0) 100%);\n  margin-left:10px;\n  border-radius:80% 80% 0 0;\n  position:absolute;\n  opacity:0.1;\n  background-color: white;\n}\n\n.ray1[_ngcontent-%COMP%] {\n  height:170px;\n  width:30px;\n  -webkit-transform: rotate(180deg);\n  top:-175px;\n  left: 15px;\n}\n\n.ray2[_ngcontent-%COMP%] {\n  height:100px;\n  width:8px;\n  -webkit-transform: rotate(220deg);\n  top:-90px;\n  left: 75px;\n}\n\n.ray3[_ngcontent-%COMP%] {\n  height:170px;\n  width:50px;\n  -webkit-transform: rotate(250deg);\n  top:-80px;\n  left: 100px;\n}\n\n.ray4[_ngcontent-%COMP%] {\n  height:120px;\n  width:14px;\n  -webkit-transform: rotate(305deg);\n  top:30px;\n  left: 100px;\n}\n\n.ray5[_ngcontent-%COMP%] {\n  height:140px;\n  width:30px;\n  -webkit-transform: rotate(-15deg);\n  top:60px;\n  left: 40px;\n}\n\n.ray6[_ngcontent-%COMP%] {\n  height:90px;\n  width:50px;\n  -webkit-transform: rotate(30deg);\n  top:60px;\n  left: -40px;\n}\n\n.ray7[_ngcontent-%COMP%] {\n  height:180px;\n  width:10px;\n  -webkit-transform: rotate(70deg);\n  top:-35px;\n  left: -40px;\n}\n\n.ray8[_ngcontent-%COMP%] {\n  height:120px;\n  width:30px;\n  -webkit-transform: rotate(100deg);\n  top:-45px;\n  left:-90px;\n}\n\n.ray9[_ngcontent-%COMP%] {\n  height:80px;\n  width:10px;\n  -webkit-transform: rotate(120deg);\n  top:-65px;\n  left:-60px;\n}\n\n.ray10[_ngcontent-%COMP%] {\n  height:190px;\n  width:23px;\n  -webkit-transform: rotate(150deg);\n  top:-185px;\n  left: -60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTEvbGl2aW5ncm9vbTEuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG9FQUErQztFQUMvQyw0QkFBNEI7RUFDNUIsWUFBWTtBQUNkOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsWUFBWTtFQUNaLFVBQVU7RUFDVixTQUFTO0FBQ1g7O0FBQ0E7RUFDRSw2REFBd0M7QUFDMUM7O0FBQ0E7RUFDRSxvRUFBK0M7RUFDL0MsYUFBYTtBQUNmOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXO0VBQ1gsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsaUVBQTRDO0VBQzVDLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osV0FBVztFQUNYLFdBQVc7RUFDWCxVQUFVO0VBQ1YsYUFBYTtBQUNmOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixXQUFXO0VBQ1gsWUFBWTtFQUNaLFdBQVc7RUFDWCxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsc0VBQWlEO0VBQ2pELDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsWUFBWTtFQUNaLFdBQVc7RUFDWCxTQUFTO0FBQ1g7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsVUFBVTtFQUNWLGFBQWE7RUFDYixZQUFZO0VBQ1osa0JBQWtCO0FBQ3BCOztBQUVBOztBQUVBOztBQUVBLG1CQUFtQjs7QUFDbkIsbUJBQW1COztBQUNuQixtQkFBbUI7O0FBQ25CO0VBQ0Usa0JBQWtCO0VBQ2xCLDRFQUEwRTtFQUMxRSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtBQUNmOztBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixrQ0FBa0M7QUFDcEM7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsdUJBQXVCO0VBQ3ZCLGFBQWE7RUFDYixZQUFZO0VBQ1osV0FBVztFQUNYLFNBQVM7QUFDWDs7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsaUVBQTRDO0VBQzVDLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0FBQ1o7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsb0VBQStDO0VBQy9DLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsWUFBWTtFQUNaLFdBQVc7RUFDWCxTQUFTO0FBQ1g7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLDBCQUEwQjtBQUM1Qjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQix1RUFBcUU7RUFDckUsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixZQUFZO0VBQ1osV0FBVztFQUNYLFNBQVM7RUFDVCxhQUFhO0FBQ2Y7O0FBRUEsaUJBQWlCOztBQUVqQjtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsVUFBVTtFQUNWLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLG1DQUFtQztBQUNyQzs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsV0FBVztFQUNYLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLG1DQUFtQztBQUNyQzs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osT0FBTztFQUNQLE1BQU07RUFDTixPQUFPO0VBQ1AsUUFBUTtFQUNSLFVBQVU7RUFDVixnREFBZ0Q7RUFDaEQsdUNBQXVDO0FBQ3pDOztBQUNBO0VBRUUsbUhBQW1IO0VBQ25ILGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCx1QkFBdUI7QUFDekI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLGlDQUFpQztFQUNqQyxVQUFVO0VBQ1YsVUFBVTtBQUNaOztBQUNBO0VBQ0UsWUFBWTtFQUNaLFNBQVM7RUFDVCxpQ0FBaUM7RUFDakMsU0FBUztFQUNULFVBQVU7QUFDWjs7QUFDQTtFQUNFLFlBQVk7RUFDWixVQUFVO0VBQ1YsaUNBQWlDO0VBQ2pDLFNBQVM7RUFDVCxXQUFXO0FBQ2I7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLGlDQUFpQztFQUNqQyxRQUFRO0VBQ1IsV0FBVztBQUNiOztBQUNBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDVixpQ0FBaUM7RUFDakMsUUFBUTtFQUNSLFVBQVU7QUFDWjs7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1YsZ0NBQWdDO0VBQ2hDLFFBQVE7RUFDUixXQUFXO0FBQ2I7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLGdDQUFnQztFQUNoQyxTQUFTO0VBQ1QsV0FBVztBQUNiOztBQUNBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDVixpQ0FBaUM7RUFDakMsU0FBUztFQUNULFVBQVU7QUFDWjs7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1YsaUNBQWlDO0VBQ2pDLFNBQVM7RUFDVCxVQUFVO0FBQ1o7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLGlDQUFpQztFQUNqQyxVQUFVO0VBQ1YsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvbGl2aW5ncm9vbTEvbGl2aW5ncm9vbTEuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNsaXZpbmdyb29tMS1yb290IHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2xpdmluZ3Jvb20xLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiA5NTBweCA2NTBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uZG9vciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogNDI5cHg7XG4gIHdpZHRoOiAxMzZweDtcbiAgbGVmdDogNjlweDtcbiAgdG9wOiA1MHB4O1xufVxuLmRvb3ItY2xvc2VkIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2Rvb3IucG5nXCIpO1xufVxuLmRvb3Itb3BlbmVkIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2Rvb3Itb3BlbmVkLnBuZ1wiKTtcbiAgei1pbmRleDogMjAwMDtcbn1cblxuLmRvb3JLbm9iIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbiAgbGVmdDogMTUwcHg7XG4gIHRvcDogMjcwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogNjAwMDs7XG59XG5cbi5rZXktbG9jayB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2tleS1sb2NrLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogMThweDtcbiAgd2lkdGg6IDE4cHg7XG4gIGxlZnQ6IDIzOXB4O1xuICB0b3A6IDQ1MHB4O1xuICB6LWluZGV4OiAxMDAwO1xufVxuXG4ubGl2aW5ncm9vbTEtZHJhd2VyMiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA0NTBweDtcbiAgd2lkdGg6IDYxcHg7XG4gIGhlaWdodDogMTlweDtcbiAgbGVmdDogMjE4cHg7XG4gIHotaW5kZXg6IDYwMDA7XG59XG5cbi5zcGFjZS1pbnZhZGVycyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL3NwYWNlSW52YWRlcnMuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiAxMTBweDtcbiAgd2lkdGg6IDE1NHB4O1xuICBsZWZ0OiA0MThweDtcbiAgdG9wOiA5NXB4O1xufVxuLnVuZGVyLXNvZmEtcmlnaHQge1xuICBsZWZ0OiA0NTBweDtcbiAgdG9wOiA1MDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDI1MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5rZXlEcmF3ZXIxIHtcblxufVxuXG4vKioqKioqKioqKioqKioqKioqL1xuLyoqKiBMQVNFUiBHQU1FICoqKi9cbi8qKioqKioqKioqKioqKioqKiovXG4jc3BhY2UtaW52YWRlci1sYXNlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ltYWdlcy9pdGVtcy9zcGFjZS1pbnZhZGVyLWxhc2VyLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IDc1cHg7XG4gIGN1cnNvcjogbm9uZTtcbiAgei1pbmRleDogNTAwMDtcbn1cbi5idWxsZXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiA3cHg7XG4gIHdpZHRoOiAxcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMjAwMDtcbiAgYm9yZGVyOiAxcHggcmdiKDI0MCwyNDAsMjQwKSBzb2xpZDtcbn1cbi5wYWludGluZy13aGl0ZS1iZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMTEwcHg7XG4gIHdpZHRoOiAxNTRweDtcbiAgbGVmdDogNDE4cHg7XG4gIHRvcDogOTVweDtcbn1cbi5pbnZhZGVyIHtcbiAgbWFyZ2luLWxlZnQ6IDRweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLmNhdC1mZWFyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvY2F0LWZlYXIucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgaGVpZ2h0OiAxMTBweDtcbiAgd2lkdGg6IDE1NHB4O1xuICBsZWZ0OiA0MThweDtcbiAgdG9wOiA0OTVweDtcbn1cbiNwYWludGluZy1iZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL3BhaW50aW5nLWJnLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogMTEwcHg7XG4gIHdpZHRoOiAxNTRweDtcbiAgbGVmdDogNDE4cHg7XG4gIHRvcDogOTVweDtcbn1cbi50cm9waHktb24tcGFpbnRpbmcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDUwMDA7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuXG4ub2xkLWdvbGQta2V5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaW1hZ2VzL2l0ZW1zL29sZC1nb2xkZW4ta2V5LnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGhlaWdodDogMTEwcHg7XG4gIHdpZHRoOiAxNTRweDtcbiAgbGVmdDogNDE4cHg7XG4gIHRvcDogOTVweDtcbiAgei1pbmRleDogODAwMDtcbn1cblxuLyogTGlnaHRzIEJlYW1zICovXG5cbmJvZHkge1xuICBiYWNrZ3JvdW5kOiMyRUI1RTUgO1xufVxuXG4uc3VuMiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA4MHB4O1xuICBsZWZ0OiA5MHB4O1xuICBtYXJnaW46IGF1dG87XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIG9wYWNpdHk6IDAuOTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCA0MHB4IDE1cHggd2hpdGU7XG59XG5cbi5zdW4ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTIwcHg7XG4gIGxlZnQ6IDQ2MHB4O1xuICBtYXJnaW46IGF1dG87XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDcwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIG9wYWNpdHk6IDAuOTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCA0MHB4IDE1cHggd2hpdGU7XG59XG5cbi5yYXlfYm94IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW46IGF1dG87XG4gIHRvcDowcHg7XG4gIGxlZnQ6MDtcbiAgcmlnaHQ6MDtcbiAgYm90dG9tOjA7XG4gIHdpZHRoOjcwcHg7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiByYXlfYW5pbSAxMjBzIGxpbmVhciBpbmZpbml0ZTtcbiAgYW5pbWF0aW9uOiByYXlfYW5pbSAxMHMgbGluZWFyIGluZmluaXRlO1xufVxuLnJheSB7XG4gIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSgyNTUsMjU1LDI1NSwwKSAwJSwgcmdiYSgyNTUsMjU1LDI1NSwwLjgpIDUwJSwgcmdiYSgyNTUsMjU1LDI1NSwwKSAxMDAlKTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSgyNTUsMjU1LDI1NSwwKSAwJSwgcmdiYSgyNTUsMjU1LDI1NSwwLjgpIDUwJSwgcmdiYSgyNTUsMjU1LDI1NSwwKSAxMDAlKTtcbiAgbWFyZ2luLWxlZnQ6MTBweDtcbiAgYm9yZGVyLXJhZGl1czo4MCUgODAlIDAgMDtcbiAgcG9zaXRpb246YWJzb2x1dGU7XG4gIG9wYWNpdHk6MC4xO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLnJheTEge1xuICBoZWlnaHQ6MTcwcHg7XG4gIHdpZHRoOjMwcHg7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbiAgdG9wOi0xNzVweDtcbiAgbGVmdDogMTVweDtcbn1cbi5yYXkyIHtcbiAgaGVpZ2h0OjEwMHB4O1xuICB3aWR0aDo4cHg7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjIwZGVnKTtcbiAgdG9wOi05MHB4O1xuICBsZWZ0OiA3NXB4O1xufVxuLnJheTMge1xuICBoZWlnaHQ6MTcwcHg7XG4gIHdpZHRoOjUwcHg7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMjUwZGVnKTtcbiAgdG9wOi04MHB4O1xuICBsZWZ0OiAxMDBweDtcbn1cbi5yYXk0IHtcbiAgaGVpZ2h0OjEyMHB4O1xuICB3aWR0aDoxNHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDMwNWRlZyk7XG4gIHRvcDozMHB4O1xuICBsZWZ0OiAxMDBweDtcbn1cbi5yYXk1IHtcbiAgaGVpZ2h0OjE0MHB4O1xuICB3aWR0aDozMHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKC0xNWRlZyk7XG4gIHRvcDo2MHB4O1xuICBsZWZ0OiA0MHB4O1xufVxuLnJheTYge1xuICBoZWlnaHQ6OTBweDtcbiAgd2lkdGg6NTBweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XG4gIHRvcDo2MHB4O1xuICBsZWZ0OiAtNDBweDtcbn1cbi5yYXk3IHtcbiAgaGVpZ2h0OjE4MHB4O1xuICB3aWR0aDoxMHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDcwZGVnKTtcbiAgdG9wOi0zNXB4O1xuICBsZWZ0OiAtNDBweDtcbn1cbi5yYXk4IHtcbiAgaGVpZ2h0OjEyMHB4O1xuICB3aWR0aDozMHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDEwMGRlZyk7XG4gIHRvcDotNDVweDtcbiAgbGVmdDotOTBweDtcbn1cbi5yYXk5IHtcbiAgaGVpZ2h0OjgwcHg7XG4gIHdpZHRoOjEwcHg7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcbiAgdG9wOi02NXB4O1xuICBsZWZ0Oi02MHB4O1xufVxuLnJheTEwIHtcbiAgaGVpZ2h0OjE5MHB4O1xuICB3aWR0aDoyM3B4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XG4gIHRvcDotMTg1cHg7XG4gIGxlZnQ6IC02MHB4O1xufVxuXG5cbiJdfQ== */", "body[_ngcontent-%COMP%] {\n  background:#2EB5E5 ;\n}\n\n.sun2[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 80px;\n  left: 90px;\n  margin: auto;\n  width: 70px;\n  height: 70px;\n  border-radius: 50%;\n  background: white;\n  opacity: 0.9;\n  box-shadow: 0px 0px 40px 15px white;\n}\n\n.sun[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 120px;\n  left: 460px;\n  margin: auto;\n  width: 70px;\n  height: 70px;\n  border-radius: 50%;\n  background: white;\n  opacity: 0.9;\n  box-shadow: 0px 0px 40px 15px white;\n}\n\n.ray_box[_ngcontent-%COMP%] {\n  position: absolute;\n  margin: auto;\n  top:0px;\n  left:0;\n  right:0;\n  bottom:0;\n  width:70px;\n  -webkit-animation: ray_anim 120s linear infinite;\n  animation: ray_anim 10s linear infinite;\n}\n\n.ray[_ngcontent-%COMP%] {\n  background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,0.8) 50%, rgba(255,255,255,0) 100%);\n  margin-left:10px;\n  border-radius:80% 80% 0 0;\n  position:absolute;\n  opacity:0.1;\n  background-color: white;\n}\n\n.ray1[_ngcontent-%COMP%] {\n  height:170px;\n  width:30px;\n  -webkit-transform: rotate(180deg);\n  top:-175px;\n  left: 15px;\n}\n\n.ray2[_ngcontent-%COMP%] {\n  height:100px;\n  width:8px;\n  -webkit-transform: rotate(220deg);\n  top:-90px;\n  left: 75px;\n}\n\n.ray3[_ngcontent-%COMP%] {\n  height:170px;\n  width:50px;\n  -webkit-transform: rotate(250deg);\n  top:-80px;\n  left: 100px;\n}\n\n.ray4[_ngcontent-%COMP%] {\n  height:120px;\n  width:14px;\n  -webkit-transform: rotate(305deg);\n  top:30px;\n  left: 100px;\n}\n\n.ray5[_ngcontent-%COMP%] {\n  height:140px;\n  width:30px;\n  -webkit-transform: rotate(-15deg);\n  top:60px;\n  left: 40px;\n}\n\n.ray6[_ngcontent-%COMP%] {\n  height:90px;\n  width:50px;\n  -webkit-transform: rotate(30deg);\n  top:60px;\n  left: -40px;\n}\n\n.ray7[_ngcontent-%COMP%] {\n  height:180px;\n  width:10px;\n  -webkit-transform: rotate(70deg);\n  top:-35px;\n  left: -40px;\n}\n\n.ray8[_ngcontent-%COMP%] {\n  height:120px;\n  width:30px;\n  -webkit-transform: rotate(100deg);\n  top:-45px;\n  left:-90px;\n}\n\n.ray9[_ngcontent-%COMP%] {\n  height:80px;\n  width:10px;\n  -webkit-transform: rotate(120deg);\n  top:-65px;\n  left:-60px;\n}\n\n.ray10[_ngcontent-%COMP%] {\n  height:190px;\n  width:23px;\n  -webkit-transform: rotate(150deg);\n  top:-185px;\n  left: -60px;\n}\n\n@-webkit-keyframes ray_anim {\n  0% { transform: rotate(0deg);}\n  100% { transform: rotate(360deg);}\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTEvbGl2aW5ncm9vbTEtbGlnaHRyYXlzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFVBQVU7RUFDVixZQUFZO0VBQ1osV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixtQ0FBbUM7QUFDckM7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFdBQVc7RUFDWCxZQUFZO0VBQ1osV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixtQ0FBbUM7QUFDckM7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLE9BQU87RUFDUCxNQUFNO0VBQ04sT0FBTztFQUNQLFFBQVE7RUFDUixVQUFVO0VBQ1YsZ0RBQWdEO0VBQ2hELHVDQUF1QztBQUN6Qzs7QUFDQTtFQUVFLG1IQUFtSDtFQUNuSCxnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsdUJBQXVCO0FBQ3pCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDVixpQ0FBaUM7RUFDakMsVUFBVTtFQUNWLFVBQVU7QUFDWjs7QUFDQTtFQUNFLFlBQVk7RUFDWixTQUFTO0VBQ1QsaUNBQWlDO0VBQ2pDLFNBQVM7RUFDVCxVQUFVO0FBQ1o7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLGlDQUFpQztFQUNqQyxTQUFTO0VBQ1QsV0FBVztBQUNiOztBQUNBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDVixpQ0FBaUM7RUFDakMsUUFBUTtFQUNSLFdBQVc7QUFDYjs7QUFDQTtFQUNFLFlBQVk7RUFDWixVQUFVO0VBQ1YsaUNBQWlDO0VBQ2pDLFFBQVE7RUFDUixVQUFVO0FBQ1o7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsVUFBVTtFQUNWLGdDQUFnQztFQUNoQyxRQUFRO0VBQ1IsV0FBVztBQUNiOztBQUNBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDVixnQ0FBZ0M7RUFDaEMsU0FBUztFQUNULFdBQVc7QUFDYjs7QUFDQTtFQUNFLFlBQVk7RUFDWixVQUFVO0VBQ1YsaUNBQWlDO0VBQ2pDLFNBQVM7RUFDVCxVQUFVO0FBQ1o7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsVUFBVTtFQUNWLGlDQUFpQztFQUNqQyxTQUFTO0VBQ1QsVUFBVTtBQUNaOztBQUNBO0VBQ0UsWUFBWTtFQUNaLFVBQVU7RUFDVixpQ0FBaUM7RUFDakMsVUFBVTtFQUNWLFdBQVc7QUFDYjs7QUFHQTtFQUNFLEtBQXNDLHVCQUF1QixDQUFDO0VBQzlELE9BQTBDLHlCQUF5QixDQUFDO0FBQ3RFIiwiZmlsZSI6InNyYy9hcHAvbGl2aW5ncm9vbTEvbGl2aW5ncm9vbTEtbGlnaHRyYXlzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5IHtcbiAgYmFja2dyb3VuZDojMkVCNUU1IDtcbn1cblxuLnN1bjIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogODBweDtcbiAgbGVmdDogOTBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBvcGFjaXR5OiAwLjk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggNDBweCAxNXB4IHdoaXRlO1xufVxuXG4uc3VuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEyMHB4O1xuICBsZWZ0OiA0NjBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBvcGFjaXR5OiAwLjk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggNDBweCAxNXB4IHdoaXRlO1xufVxuXG4ucmF5X2JveCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luOiBhdXRvO1xuICB0b3A6MHB4O1xuICBsZWZ0OjA7XG4gIHJpZ2h0OjA7XG4gIGJvdHRvbTowO1xuICB3aWR0aDo3MHB4O1xuICAtd2Via2l0LWFuaW1hdGlvbjogcmF5X2FuaW0gMTIwcyBsaW5lYXIgaW5maW5pdGU7XG4gIGFuaW1hdGlvbjogcmF5X2FuaW0gMTBzIGxpbmVhciBpbmZpbml0ZTtcbn1cbi5yYXkge1xuICBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0byBib3R0b20sIHJnYmEoMjU1LDI1NSwyNTUsMCkgMCUsIHJnYmEoMjU1LDI1NSwyNTUsMC44KSA1MCUsIHJnYmEoMjU1LDI1NSwyNTUsMCkgMTAwJSk7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sIHJnYmEoMjU1LDI1NSwyNTUsMCkgMCUsIHJnYmEoMjU1LDI1NSwyNTUsMC44KSA1MCUsIHJnYmEoMjU1LDI1NSwyNTUsMCkgMTAwJSk7XG4gIG1hcmdpbi1sZWZ0OjEwcHg7XG4gIGJvcmRlci1yYWRpdXM6ODAlIDgwJSAwIDA7XG4gIHBvc2l0aW9uOmFic29sdXRlO1xuICBvcGFjaXR5OjAuMTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5yYXkxIHtcbiAgaGVpZ2h0OjE3MHB4O1xuICB3aWR0aDozMHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG4gIHRvcDotMTc1cHg7XG4gIGxlZnQ6IDE1cHg7XG59XG4ucmF5MiB7XG4gIGhlaWdodDoxMDBweDtcbiAgd2lkdGg6OHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDIyMGRlZyk7XG4gIHRvcDotOTBweDtcbiAgbGVmdDogNzVweDtcbn1cbi5yYXkzIHtcbiAgaGVpZ2h0OjE3MHB4O1xuICB3aWR0aDo1MHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDI1MGRlZyk7XG4gIHRvcDotODBweDtcbiAgbGVmdDogMTAwcHg7XG59XG4ucmF5NCB7XG4gIGhlaWdodDoxMjBweDtcbiAgd2lkdGg6MTRweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzMDVkZWcpO1xuICB0b3A6MzBweDtcbiAgbGVmdDogMTAwcHg7XG59XG4ucmF5NSB7XG4gIGhlaWdodDoxNDBweDtcbiAgd2lkdGg6MzBweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgtMTVkZWcpO1xuICB0b3A6NjBweDtcbiAgbGVmdDogNDBweDtcbn1cbi5yYXk2IHtcbiAgaGVpZ2h0OjkwcHg7XG4gIHdpZHRoOjUwcHg7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzBkZWcpO1xuICB0b3A6NjBweDtcbiAgbGVmdDogLTQwcHg7XG59XG4ucmF5NyB7XG4gIGhlaWdodDoxODBweDtcbiAgd2lkdGg6MTBweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg3MGRlZyk7XG4gIHRvcDotMzVweDtcbiAgbGVmdDogLTQwcHg7XG59XG4ucmF5OCB7XG4gIGhlaWdodDoxMjBweDtcbiAgd2lkdGg6MzBweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxMDBkZWcpO1xuICB0b3A6LTQ1cHg7XG4gIGxlZnQ6LTkwcHg7XG59XG4ucmF5OSB7XG4gIGhlaWdodDo4MHB4O1xuICB3aWR0aDoxMHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDEyMGRlZyk7XG4gIHRvcDotNjVweDtcbiAgbGVmdDotNjBweDtcbn1cbi5yYXkxMCB7XG4gIGhlaWdodDoxOTBweDtcbiAgd2lkdGg6MjNweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxNTBkZWcpO1xuICB0b3A6LTE4NXB4O1xuICBsZWZ0OiAtNjBweDtcbn1cblxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgcmF5X2FuaW0ge1xuICAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7IHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO31cbiAgMTAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTsgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTt9XG59XG4iXX0= */", ".rnOuter[_ngcontent-%COMP%] {\n  overflow: hidden;\n  position: relative;\n  height: 100vh;\n  z-index: 9000;\n}\n.rnInner[_ngcontent-%COMP%] {\n  width: 100%;\n  position: absolute;\n  top: -10%;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  margin: auto;\n  transform-origin: -120% top;\n}\n.rnUnit[_ngcontent-%COMP%] {\n  width: 10%;;\n  height: 93vh;\n  background: repeating-linear-gradient(to left,#e61a1a 4vw,#8a0f0f 8vw,#f07575 10vw);\n  background-size: 100% 100%;\n  display: inline-block;\n  transform-origin: 0 0%;\n  transform: rotate(3deg);\n  -webkit-animation: rnUnit 2s ease infinite;\n          animation: rnUnit 2s ease infinite;\n}\n@-webkit-keyframes rnUnit {\n  50% {\n    transform: rotate(-3deg);\n  }\n}\n@keyframes rnUnit {\n  50% {\n    transform: rotate(-3deg);\n  }\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(1) {\n  -webkit-animation-delay: -0.1s;\n          animation-delay: -0.1s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(2) {\n  -webkit-animation-delay: -0.2s;\n          animation-delay: -0.2s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(3) {\n  -webkit-animation-delay: -0.3s;\n          animation-delay: -0.3s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(4) {\n  -webkit-animation-delay: -0.4s;\n          animation-delay: -0.4s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(5) {\n  -webkit-animation-delay: -0.5s;\n          animation-delay: -0.5s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(6) {\n  -webkit-animation-delay: -0.6s;\n          animation-delay: -0.6s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(7) {\n  -webkit-animation-delay: -0.7s;\n          animation-delay: -0.7s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(8) {\n  -webkit-animation-delay: -0.8s;\n          animation-delay: -0.8s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(9) {\n  -webkit-animation-delay: -0.9s;\n          animation-delay: -0.9s;\n}\n.rnUnit[_ngcontent-%COMP%]:nth-child(10) {\n  -webkit-animation-delay: -1s;\n          animation-delay: -1s;\n}\n.aoTable[_ngcontent-%COMP%] {\n  display: table;\n  width: 100%;\n  height: 100vh;\n  text-align: center;\n}\n.aoTableCell[_ngcontent-%COMP%] {\n  color: #ae1313;\n  display: table-cell;\n  vertical-align: middle;\n  transition: color 3s ease;\n}\n.rnInnerOpen[_ngcontent-%COMP%] {\n  transform-origin: -120% top;\n  transform: scaleX(0);\n  transform-style: preserve-3d;\n  transition: transform 6s ease;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTEvbGl2aW5ncm9vbTEtY3VydGFpbnMuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixhQUFhO0FBQ2Y7QUFDQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFFBQVE7RUFDUixTQUFTO0VBQ1QsT0FBTztFQUNQLFlBQVk7RUFDWiwyQkFBMkI7QUFDN0I7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osbUZBQW1GO0VBQ25GLDBCQUEwQjtFQUMxQixxQkFBcUI7RUFDckIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QiwwQ0FBa0M7VUFBbEMsa0NBQWtDO0FBQ3BDO0FBQ0E7RUFDRTtJQUNFLHdCQUF3QjtFQUMxQjtBQUNGO0FBSkE7RUFDRTtJQUNFLHdCQUF3QjtFQUMxQjtBQUNGO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw4QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSw0QkFBb0I7VUFBcEIsb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUNYLGFBQWE7RUFDYixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLHlCQUF5QjtBQUMzQjtBQUVBO0VBQ0UsMkJBQTJCO0VBQzNCLG9CQUFvQjtFQUNwQiw0QkFBNEI7RUFDNUIsNkJBQTZCO0FBQy9CIiwiZmlsZSI6InNyYy9hcHAvbGl2aW5ncm9vbTEvbGl2aW5ncm9vbTEtY3VydGFpbnMuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbi5ybk91dGVyIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICB6LWluZGV4OiA5MDAwO1xufVxuLnJuSW5uZXIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IC0xMCU7XG4gIHJpZ2h0OiAwO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgdHJhbnNmb3JtLW9yaWdpbjogLTEyMCUgdG9wO1xufVxuLnJuVW5pdCB7XG4gIHdpZHRoOiAxMCU7O1xuICBoZWlnaHQ6IDkzdmg7XG4gIGJhY2tncm91bmQ6IHJlcGVhdGluZy1saW5lYXItZ3JhZGllbnQodG8gbGVmdCwjZTYxYTFhIDR2dywjOGEwZjBmIDh2dywjZjA3NTc1IDEwdncpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAwIDAlO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgzZGVnKTtcbiAgYW5pbWF0aW9uOiByblVuaXQgMnMgZWFzZSBpbmZpbml0ZTtcbn1cbkBrZXlmcmFtZXMgcm5Vbml0IHtcbiAgNTAlIHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgtM2RlZyk7XG4gIH1cbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDEpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xcztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDIpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4ycztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDMpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4zcztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDQpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC40cztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDUpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC41cztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDYpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC42cztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDcpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC43cztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDgpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC44cztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDkpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC45cztcbn1cbi5yblVuaXQ6bnRoLWNoaWxkKDEwKSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTFzO1xufVxuLmFvVGFibGUge1xuICBkaXNwbGF5OiB0YWJsZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5hb1RhYmxlQ2VsbCB7XG4gIGNvbG9yOiAjYWUxMzEzO1xuICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICB0cmFuc2l0aW9uOiBjb2xvciAzcyBlYXNlO1xufVxuXG4ucm5Jbm5lck9wZW4ge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiAtMTIwJSB0b3A7XG4gIHRyYW5zZm9ybTogc2NhbGVYKDApO1xuICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gNnMgZWFzZTtcbn1cblxuXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Livingroom1Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-livingroom1',
                templateUrl: './livingroom1.component.html',
                styleUrls: ['./livingroom1.component.css',
                    './livingroom1-lightrays.component.css',
                    './livingroom1-curtains.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_4__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/livingroom1drawer-purple/livingroom1drawer-purple.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/livingroom1drawer-purple/livingroom1drawer-purple.component.ts ***!
  \********************************************************************************/
/*! exports provided: Livingroom1drawerPurpleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Livingroom1drawerPurpleComponent", function() { return Livingroom1drawerPurpleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _service_phone_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/phone.service */ "./src/app/service/phone.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");







function Livingroom1drawerPurpleComponent_img_3_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom1drawerPurpleComponent_img_3_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1.takePhone(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom1"]; };
class Livingroom1drawerPurpleComponent {
    constructor(gameService, phoneService) {
        this.gameService = gameService;
        this.phoneService = phoneService;
    }
    ngOnInit() {
        this.AppConstants = _app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"];
    }
    takePhone() {
        this.gameService.addItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PHONE_ITEM, this.phoneService.getNewPhone());
    }
    hasPhoneBeenTaken() {
        return this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PHONE_ITEM);
    }
}
Livingroom1drawerPurpleComponent.ɵfac = function Livingroom1drawerPurpleComponent_Factory(t) { return new (t || Livingroom1drawerPurpleComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_phone_service__WEBPACK_IMPORTED_MODULE_3__["PhoneService"])); };
Livingroom1drawerPurpleComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Livingroom1drawerPurpleComponent, selectors: [["app-livingroom1drawer-purple"]], decls: 4, vars: 3, consts: [["id", "livingroom1drawer-purple"], [3, "routerLink"], [1, "goToBottom"], ["src", "assets/images/items/phone-flash-invaders.png", "class", "phone", 3, "click", 4, "ngIf"], ["src", "assets/images/items/phone-flash-invaders.png", 1, "phone", 3, "click"]], template: function Livingroom1drawerPurpleComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, Livingroom1drawerPurpleComponent_img_3_Template, 1, 0, "img", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasPhoneBeenTaken());
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"]], styles: ["#livingroom1drawer-purple[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/emptyDrawer.jpg');\n  background-size: 100% 100%;\n}\n.phone[_ngcontent-%COMP%] {\n  height: 300px;\n  position: absolute;\n  left: 250px;\n  top: 200px;\n  transform: rotate(-45deg);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTFkcmF3ZXItcHVycGxlL2xpdmluZ3Jvb20xZHJhd2VyLXB1cnBsZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsb0VBQTREO0VBQzVELDBCQUEwQjtBQUM1QjtBQUNBO0VBQ0UsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsVUFBVTtFQUNWLHlCQUF5QjtBQUMzQiIsImZpbGUiOiJzcmMvYXBwL2xpdmluZ3Jvb20xZHJhd2VyLXB1cnBsZS9saXZpbmdyb29tMWRyYXdlci1wdXJwbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNsaXZpbmdyb29tMWRyYXdlci1wdXJwbGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pbWFnZXMvZW1wdHlEcmF3ZXIuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbn1cbi5waG9uZSB7XG4gIGhlaWdodDogMzAwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMjUwcHg7XG4gIHRvcDogMjAwcHg7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Livingroom1drawerPurpleComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-livingroom1drawer-purple',
                templateUrl: './livingroom1drawer-purple.component.html',
                styleUrls: ['./livingroom1drawer-purple.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }, { type: _service_phone_service__WEBPACK_IMPORTED_MODULE_3__["PhoneService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/livingroom2/livingroom2.component.ts":
/*!******************************************************!*\
  !*** ./src/app/livingroom2/livingroom2.component.ts ***!
  \******************************************************/
/*! exports provided: Livingroom2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Livingroom2Component", function() { return Livingroom2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");






function Livingroom2Component_div_16_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom2Component_div_16_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1.openWoodPlank(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom1"]; };
const _c1 = function () { return ["/livingroom3"]; };
const _c2 = function () { return ["/emptydrawer"]; };
const _c3 = function () { return ["/clock-digital"]; };
class Livingroom2Component {
    constructor(gameService, router) {
        this.gameService = gameService;
        this.router = router;
    }
    ngOnInit() { }
    openWoodPlank() {
        if (this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SCREWDRIVER)) {
            this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SCREWDRIVER);
        }
        if (this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SCREWDRIVER)) {
            this.router.navigate(["/inside-cupboard-left-inner"]);
        }
    }
    hasWoodPlankBeenOpened() {
        return this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SCREWDRIVER);
    }
}
Livingroom2Component.ɵfac = function Livingroom2Component_Factory(t) { return new (t || Livingroom2Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"])); };
Livingroom2Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Livingroom2Component, selectors: [["app-livingroom2"]], decls: 18, vars: 13, consts: [["id", "livingroom2-root"], [1, "wallColor", "wallBlock"], [1, "carpetColor", "carpetBlock"], [3, "routerLink"], [1, "goToLeft"], [1, "goToRight"], [1, "cupboard"], [1, "drawer1"], [1, "drawer2"], [1, "drawer3"], [1, "clock-digital"], ["class", "wood-plank pointer", 3, "click", 4, "ngIf"], [1, "leftDoorCupboard", "pointer", 3, "click"], [1, "wood-plank", "pointer", 3, "click"]], template: function Livingroom2Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, Livingroom2Component_div_16_Template, 1, 0, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom2Component_Template_div_click_17_listener() { return ctx.openWoodPlank(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](7, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](8, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](9, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c3));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasWoodPlankBeenOpened());
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"]], styles: [".cupboard[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 500px;\n  width: 800px;\n  background-image: url('/escape-game-space-invaders/cupboard.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 50px;\n  bottom: 120px;\n}\n.wood-plank[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 75px;\n  width: 150px;\n  background-image: url('/escape-game-space-invaders/wood-plank.png');\n  background-size: 100% 100%;\n  z-index: 120;\n  left: 225px;\n  bottom: 200px;\n}\n.leftDoorCupboard[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 187px;\n  width: 208px;\n  z-index: 6000;\n  left: 187px;\n  top: 319px;\n}\n.drawer1[_ngcontent-%COMP%] {\n  height: 50px;\n  width: 105px;\n  top: 321px;\n  left: 397px;\n  z-index: 9999;\n  position: absolute;\n}\n.drawer2[_ngcontent-%COMP%] {\n  height: 50px;\n  width: 105px;\n  top: 371px;\n  left: 397px;\n  z-index: 9999;\n  position: absolute;\n}\n.drawer3[_ngcontent-%COMP%] {\n  height: 87px;\n  width: 105px;\n  top: 421px;\n  left: 397px;\n  z-index: 9999;\n  position: absolute;\n}\n.clock-digital[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 40px;\n  width: 100px;\n  background-image: url('/escape-game-space-invaders/clock-digital.png');\n  background-size: 100% 100%;\n  z-index: 120;\n  left: 405px;\n  top: 267px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTIvbGl2aW5ncm9vbTIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGlFQUE0QztFQUM1QywwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFVBQVU7RUFDVixhQUFhO0FBQ2Y7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtFQUNaLG1FQUE4QztFQUM5QywwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFdBQVc7RUFDWCxhQUFhO0FBQ2Y7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsVUFBVTtBQUNaO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLFVBQVU7RUFDVixXQUFXO0VBQ1gsYUFBYTtFQUNiLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixVQUFVO0VBQ1YsV0FBVztFQUNYLGFBQWE7RUFDYixrQkFBa0I7QUFDcEI7QUFFQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osVUFBVTtFQUNWLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFlBQVk7RUFDWixzRUFBc0U7RUFDdEUsMEJBQTBCO0VBQzFCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtBQUNaIiwiZmlsZSI6InNyYy9hcHAvbGl2aW5ncm9vbTIvbGl2aW5ncm9vbTIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jdXBib2FyZCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiA1MDBweDtcbiAgd2lkdGg6IDgwMHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvY3VwYm9hcmQucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgei1pbmRleDogMTAwO1xuICBsZWZ0OiA1MHB4O1xuICBib3R0b206IDEyMHB4O1xufVxuLndvb2QtcGxhbmsge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNzVweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvd29vZC1wbGFuay5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMjA7XG4gIGxlZnQ6IDIyNXB4O1xuICBib3R0b206IDIwMHB4O1xufVxuLmxlZnREb29yQ3VwYm9hcmQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTg3cHg7XG4gIHdpZHRoOiAyMDhweDtcbiAgei1pbmRleDogNjAwMDtcbiAgbGVmdDogMTg3cHg7XG4gIHRvcDogMzE5cHg7XG59XG4uZHJhd2VyMSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwNXB4O1xuICB0b3A6IDMyMXB4O1xuICBsZWZ0OiAzOTdweDtcbiAgei1pbmRleDogOTk5OTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG4uZHJhd2VyMiB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwNXB4O1xuICB0b3A6IDM3MXB4O1xuICBsZWZ0OiAzOTdweDtcbiAgei1pbmRleDogOTk5OTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG4uZHJhd2VyMyB7XG4gIGhlaWdodDogODdweDtcbiAgd2lkdGg6IDEwNXB4O1xuICB0b3A6IDQyMXB4O1xuICBsZWZ0OiAzOTdweDtcbiAgei1pbmRleDogOTk5OTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLmNsb2NrLWRpZ2l0YWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDEwMHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi9hc3NldHMvaW1hZ2VzL3ZhcmlvdXMvY2xvY2stZGlnaXRhbC5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMjA7XG4gIGxlZnQ6IDQwNXB4O1xuICB0b3A6IDI2N3B4O1xufVxuXG5cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Livingroom2Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-livingroom2',
                templateUrl: './livingroom2.component.html',
                styleUrls: ['./livingroom2.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/livingroom3/livingroom3.component.ts":
/*!******************************************************!*\
  !*** ./src/app/livingroom3/livingroom3.component.ts ***!
  \******************************************************/
/*! exports provided: Livingroom3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Livingroom3Component", function() { return Livingroom3Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





const _c0 = function () { return ["/livingroom2"]; };
const _c1 = function () { return ["/livingroom4"]; };
const _c2 = function () { return ["/livingroom-window/0"]; };
const _c3 = function () { return ["/livingroom-window/6"]; };
const _c4 = function () { return ["/livingroom-window/7"]; };
const _c5 = function () { return ["/livingroom-window/1"]; };
const _c6 = function () { return ["/jukebox"]; };
const _c7 = function () { return ["/livingroom3drawer1"]; };
const _c8 = function () { return ["/inside-wood-cupboard-left"]; };
const _c9 = function () { return ["/japan-expo"]; };
class Livingroom3Component {
    constructor(gameService, router) {
        this.gameService = gameService;
        this.router = router;
    }
    ngOnInit() {
    }
    openTv() {
        if (this.gameService.isItemSelected(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].REMOTE)) {
            this.router.navigate(['/watching-tv']);
        }
    }
    clickOnCrackedWall() {
        $(".cracked-wall-text").show();
    }
}
Livingroom3Component.ɵfac = function Livingroom3Component_Factory(t) { return new (t || Livingroom3Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"])); };
Livingroom3Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Livingroom3Component, selectors: [["app-livingroom3"]], decls: 40, vars: 20, consts: [["id", "livingroom3-root"], [1, "wallColor", "wallBlock"], [1, "carpetColor", "carpetBlock"], [3, "routerLink"], [1, "goToLeft"], [1, "goToRight"], [1, "window", "window0"], [1, "window-container"], [1, "window-background"], [1, "backWindow"], [1, "window", "window6"], [1, "window", "window7"], [1, "window", "window1"], [1, "jukebox"], [1, "open-drawer1"], [1, "inside-wood-cupboard-left", "pointer", 3, "routerLink"], [1, "inside-wood-cupboard-right"], [1, "tv-desk", "pointer", 3, "click"], [1, "soucoupe-et-perroquet", "pointer", 3, "click"], [1, "openTV", "pointer", 3, "click"], [1, "cracked-wall", "pointer", 3, "click"], [1, "cracked-wall-text"]], template: function Livingroom3Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom3Component_Template_div_click_34_listener() { return ctx.openTv(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom3Component_Template_div_click_35_listener() { return ctx.openTv(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom3Component_Template_div_click_36_listener() { return ctx.openTv(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom3Component_Template_div_click_37_listener() { return ctx.clickOnCrackedWall(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Nothing to find here. It's just a Dance Rocker who has hit my wall. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c3));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c4));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](15, _c5));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](16, _c6));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](17, _c7));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](18, _c8));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](19, _c9));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLink"]], styles: [".jukebox[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 260px;\n  width: 150px;\n  background-image: url('/escape-game-space-invaders/jukebox.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 20px;\n  bottom: 126px;\n}\n.tv-desk[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 300px;\n  width: 600px;\n  background-image: url('/escape-game-space-invaders/tv-desk.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 170px;\n  bottom: 120px;\n}\n.soucoupe-et-perroquet[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 107px;\n  width: 226px;\n  background-image: url('/escape-game-space-invaders/soucoupe-et-perroquet.jpg');\n  background-size: 100% 100%;\n  z-index: 5000;\n  left: 356px;\n  bottom: 297px;\n}\n.openTV[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 107px;\n  width: 226px;\n  z-index: 6000;\n  left: 356px;\n  bottom: 297px;\n}\n.open-drawer1[_ngcontent-%COMP%] {\n  height: 50px;\n  width: 183px;\n  top: 451px;\n  left: 379px;\n  z-index: 9999;\n  position: absolute;\n}\n.window0[_ngcontent-%COMP%] {\n  left: 100px;\n  top: 50px;\n}\n.window1[_ngcontent-%COMP%] {\n  left: 300px;\n  top: 50px;\n}\n.window6[_ngcontent-%COMP%] {\n  left: 500px;\n  top: 50px;\n}\n.window7[_ngcontent-%COMP%] {\n  left: 700px;\n  top: 50px;\n}\n.inside-wood-cupboard-left[_ngcontent-%COMP%] {\n  left: 181px;\n  height: 100px;\n  position: absolute;\n  top: 400px;\n  width: 190px;\n  z-index: 200;\n}\n.inside-wood-cupboard-right[_ngcontent-%COMP%] {\n  left: 568px;\n  height: 100px;\n  position: absolute;\n  top: 400px;\n  width: 190px;\n  z-index: 200;\n}\n.cracked-wall[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100px;\n  width: 100px;\n  background-image: url('/escape-game-space-invaders/cracked-wall.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 770px;\n  bottom: 190px;\n}\n.cracked-wall-text[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100px;\n  width: 100px;\n  z-index: 6000;\n  left: 0;\n  top: -100px;\n  opacity: 0;\n  transition: opacity 0.5s ease-in-out;\n}\n.cracked-wall[_ngcontent-%COMP%]:hover   .cracked-wall-text[_ngcontent-%COMP%] {\n  opacity: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTMvbGl2aW5ncm9vbTMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdFQUFnRTtFQUNoRSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFVBQVU7RUFDVixhQUFhO0FBQ2Y7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdFQUEyQztFQUMzQywwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFdBQVc7RUFDWCxhQUFhO0FBQ2Y7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLDhFQUF5RDtFQUN6RCwwQkFBMEI7RUFDMUIsYUFBYTtFQUNiLFdBQVc7RUFDWCxhQUFhO0FBQ2Y7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixXQUFXO0VBQ1gsYUFBYTtBQUNmO0FBRUE7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLFVBQVU7RUFDVixXQUFXO0VBQ1gsYUFBYTtFQUNiLGtCQUFrQjtBQUNwQjtBQUdBO0VBQ0UsV0FBVztFQUNYLFNBQVM7QUFDWDtBQUVBO0VBQ0UsV0FBVztFQUNYLFNBQVM7QUFDWDtBQUVBO0VBQ0UsV0FBVztFQUNYLFNBQVM7QUFDWDtBQUVBO0VBQ0UsV0FBVztFQUNYLFNBQVM7QUFDWDtBQUVBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0FBQ2Q7QUFFQTtFQUNFLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixZQUFZO0VBQ1osWUFBWTtBQUNkO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixxRUFBZ0Q7RUFDaEQsMEJBQTBCO0VBQzFCLFlBQVk7RUFDWixXQUFXO0VBQ1gsYUFBYTtBQUNmO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixhQUFhO0VBQ2IsT0FBTztFQUNQLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysb0NBQW9DO0FBQ3RDO0FBQ0E7RUFDRSxVQUFVO0FBQ1oiLCJmaWxlIjoic3JjL2FwcC9saXZpbmdyb29tMy9saXZpbmdyb29tMy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmp1a2Vib3gge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjYwcHg7XG4gIHdpZHRoOiAxNTBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ltYWdlcy92YXJpb3VzL2p1a2Vib3gucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgei1pbmRleDogMTAwO1xuICBsZWZ0OiAyMHB4O1xuICBib3R0b206IDEyNnB4O1xufVxuLnR2LWRlc2sge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMzAwcHg7XG4gIHdpZHRoOiA2MDBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL3R2LWRlc2sucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgei1pbmRleDogMTAwO1xuICBsZWZ0OiAxNzBweDtcbiAgYm90dG9tOiAxMjBweDtcbn1cblxuLnNvdWNvdXBlLWV0LXBlcnJvcXVldCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxMDdweDtcbiAgd2lkdGg6IDIyNnB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvc291Y291cGUtZXQtcGVycm9xdWV0LmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHotaW5kZXg6IDUwMDA7XG4gIGxlZnQ6IDM1NnB4O1xuICBib3R0b206IDI5N3B4O1xufVxuXG4ub3BlblRWIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwN3B4O1xuICB3aWR0aDogMjI2cHg7XG4gIHotaW5kZXg6IDYwMDA7XG4gIGxlZnQ6IDM1NnB4O1xuICBib3R0b206IDI5N3B4O1xufVxuXG4ub3Blbi1kcmF3ZXIxIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTgzcHg7XG4gIHRvcDogNDUxcHg7XG4gIGxlZnQ6IDM3OXB4O1xuICB6LWluZGV4OiA5OTk5O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cblxuLndpbmRvdzAge1xuICBsZWZ0OiAxMDBweDtcbiAgdG9wOiA1MHB4O1xufVxuXG4ud2luZG93MSB7XG4gIGxlZnQ6IDMwMHB4O1xuICB0b3A6IDUwcHg7XG59XG5cbi53aW5kb3c2IHtcbiAgbGVmdDogNTAwcHg7XG4gIHRvcDogNTBweDtcbn1cblxuLndpbmRvdzcge1xuICBsZWZ0OiA3MDBweDtcbiAgdG9wOiA1MHB4O1xufVxuXG4uaW5zaWRlLXdvb2QtY3VwYm9hcmQtbGVmdCB7XG4gIGxlZnQ6IDE4MXB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNDAwcHg7XG4gIHdpZHRoOiAxOTBweDtcbiAgei1pbmRleDogMjAwO1xufVxuXG4uaW5zaWRlLXdvb2QtY3VwYm9hcmQtcmlnaHQge1xuICBsZWZ0OiA1NjhweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDQwMHB4O1xuICB3aWR0aDogMTkwcHg7XG4gIHotaW5kZXg6IDIwMDtcbn1cblxuLmNyYWNrZWQtd2FsbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDEwMHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvY3JhY2tlZC13YWxsLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHotaW5kZXg6IDEwMDtcbiAgbGVmdDogNzcwcHg7XG4gIGJvdHRvbTogMTkwcHg7XG59XG4uY3JhY2tlZC13YWxsLXRleHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbiAgei1pbmRleDogNjAwMDtcbiAgbGVmdDogMDtcbiAgdG9wOiAtMTAwcHg7XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMC41cyBlYXNlLWluLW91dDtcbn1cbi5jcmFja2VkLXdhbGw6aG92ZXIgLmNyYWNrZWQtd2FsbC10ZXh0IHtcbiAgb3BhY2l0eTogMTtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Livingroom3Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-livingroom3',
                templateUrl: './livingroom3.component.html',
                styleUrls: ['./livingroom3.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/livingroom3drawer1/livingroom3drawer1.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/livingroom3drawer1/livingroom3drawer1.component.ts ***!
  \********************************************************************/
/*! exports provided: Livingroom3drawer1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Livingroom3drawer1Component", function() { return Livingroom3drawer1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






function Livingroom3drawer1Component_img_1_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom3drawer1Component_img_1_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.addCarrot(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function Livingroom3drawer1Component_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom3drawer1Component_div_2_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.addKey(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom3"]; };
class Livingroom3drawer1Component {
    constructor(gameService) {
        this.gameService = gameService;
    }
    ngOnInit() {
    }
    addKey() {
        this.gameService.addItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].KEY_DRAWER1);
    }
    hasKeyBeenTaken() {
        return this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].KEY_DRAWER1);
    }
    addCarrot() {
        this.gameService.addTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].CARROTS);
    }
    hasCarrotBeenTaken() {
        return this.gameService.hasTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].CARROTS);
    }
}
Livingroom3drawer1Component.ɵfac = function Livingroom3drawer1Component_Factory(t) { return new (t || Livingroom3drawer1Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
Livingroom3drawer1Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Livingroom3drawer1Component, selectors: [["app-livingroom3drawer1"]], decls: 5, vars: 4, consts: [["id", "emptydrawer-root"], ["class", "carrots pointer", "src", "assets/images/trophies/carrots.png", 3, "click", 4, "ngIf"], ["class", "key key-bg pointer", 3, "click", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], ["src", "assets/images/trophies/carrots.png", 1, "carrots", "pointer", 3, "click"], [1, "key", "key-bg", "pointer", 3, "click"]], template: function Livingroom3drawer1Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, Livingroom3drawer1Component_img_1_Template, 1, 0, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, Livingroom3drawer1Component_div_2_Template, 1, 0, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasCarrotBeenTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.hasKeyBeenTaken());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]], styles: ["#emptydrawer-root[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/emptyDrawer.jpg');\n  background-size: 100% 100%;\n}\n.key[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 120px;\n  width: 100px;\n  background-size: 100% 100%;\n  left: 660px;\n  top:150px;\n}\n.carrots[_ngcontent-%COMP%] {\n  height: 220px;\n  background-size: 100% 100%;\n  left: 260px;\n  top:350px;\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTNkcmF3ZXIxL2xpdmluZ3Jvb20zZHJhd2VyMS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsb0VBQStDO0VBQy9DLDBCQUEwQjtBQUM1QjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLFdBQVc7RUFDWCxTQUFTO0FBQ1g7QUFDQTtFQUNFLGFBQWE7RUFDYiwwQkFBMEI7RUFDMUIsV0FBVztFQUNYLFNBQVM7RUFDVCxrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9saXZpbmdyb29tM2RyYXdlcjEvbGl2aW5ncm9vbTNkcmF3ZXIxLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjZW1wdHlkcmF3ZXItcm9vdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2VtcHR5RHJhd2VyLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG59XG4ua2V5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEyMHB4O1xuICB3aWR0aDogMTAwcHg7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBsZWZ0OiA2NjBweDtcbiAgdG9wOjE1MHB4O1xufVxuLmNhcnJvdHMge1xuICBoZWlnaHQ6IDIyMHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgbGVmdDogMjYwcHg7XG4gIHRvcDozNTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Livingroom3drawer1Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-livingroom3drawer1',
                templateUrl: './livingroom3drawer1.component.html',
                styleUrls: ['./livingroom3drawer1.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/livingroom4/livingroom4.component.ts":
/*!******************************************************!*\
  !*** ./src/app/livingroom4/livingroom4.component.ts ***!
  \******************************************************/
/*! exports provided: Livingroom4Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Livingroom4Component", function() { return Livingroom4Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");






function Livingroom4Component_img_28_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom4Component_img_28_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1.takeSushis(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom3"]; };
const _c1 = function () { return ["/livingroom1"]; };
const _c2 = function () { return ["/livingroom-window/2"]; };
const _c3 = function () { return ["/livingroom-window/3"]; };
const _c4 = function () { return ["/livingroom-window/4"]; };
const _c5 = function () { return ["/livingroom-window/5"]; };
const _c6 = function () { return ["/coffee"]; };
class Livingroom4Component {
    constructor(router, gameService) {
        this.router = router;
        this.gameService = gameService;
    }
    ngOnInit() {
    }
    goToChest() {
        if (this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LOCKER_GOLD_OPENED)) {
            this.router.navigate(["/inside-chest-gold"]);
        }
        else {
            this.router.navigate(["/lockergold1"]);
        }
    }
    goInsideComputer() {
        if (this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].COMPUTER_OPENED)) {
            this.router.navigate(["/inside-computer-secured"]);
        }
        else {
            this.router.navigate(["/inside-computer-login"]);
        }
    }
    shouldShowSushis() {
        return !this.gameService.hasItemBeenTaken(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SUSHIS);
    }
    takeSushis() {
        this.gameService.addItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].SUSHIS);
    }
}
Livingroom4Component.ɵfac = function Livingroom4Component_Factory(t) { return new (t || Livingroom4Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"])); };
Livingroom4Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Livingroom4Component, selectors: [["app-livingroom4"]], decls: 38, vars: 15, consts: [["id", "livingroom4-root"], [3, "routerLink"], [1, "goToLeft"], [1, "goToRight"], [1, "wallColor", "wallBlock"], [1, "carpetColor", "carpetBlock"], [1, "window", "window2"], [1, "window-container"], [1, "window-background"], [1, "backWindow"], [1, "window", "window3"], [1, "window", "window4"], [1, "window", "window5"], [1, "desk"], ["src", "assets/images/items/sushis.png", "class", "sushis pointer", 3, "click", 4, "ngIf"], [3, "click"], [1, "macbook", "pointer"], [1, "white-cup-1"], [1, "white-cup-2"], ["src", "assets/images/various/monsieur-chat.png", 1, "monsieur-chat"], [1, "pointer", 3, "click"], [1, "trunk-1"], [1, "locker-1"], ["src", "assets/images/items/sushis.png", 1, "sushis", "pointer", 3, "click"]], template: function Livingroom4Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, Livingroom4Component_img_28_Template, 1, 0, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom4Component_Template_a_click_29_listener() { return ctx.goInsideComputer(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Livingroom4Component_Template_a_click_35_listener() { return ctx.goToChest(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](8, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](9, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c3));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c4));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c5));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.shouldShowSushis());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c6));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"]], styles: [".desk[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 200px;\n  width: 400px;\n  background-image: url('/escape-game-space-invaders/desk.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 100px;\n  bottom: 100px;\n}\n.trunk-1[_ngcontent-%COMP%] {\n  height: 150px;\n  width: 300px;\n  background-image: url('/escape-game-space-invaders/trunk-1.png');\n  background-size: 100% 100%;\n  z-index: 100;\n  left: 550px;\n  bottom: 100px;\n  position: absolute;\n}\n.locker-1[_ngcontent-%COMP%] {\n  height: 30px;\n  width: 20px;\n  background-image: url('/escape-game-space-invaders/locker-truncated.png');\n  background-size: 100% 100%;\n  z-index: 120;\n  left: 699px;\n  top: 485px;\n  position: absolute;\n}\n.macbook[_ngcontent-%COMP%] {\n  height: 125px;\n  width: 200px;\n  background-image: url('/escape-game-space-invaders/macbook.png');\n  background-size: 100% 100%;\n  z-index: 120;\n  left: 199px;\n  top: 275px;\n  position: absolute;\n}\n.white-cup-1[_ngcontent-%COMP%] {\n  height: 50px;\n  width: 50px;\n  background-image: url('/escape-game-space-invaders/white-cup.png');\n  background-size: 100% 100%;\n  z-index: 150;\n  left: 398px;\n  top: 346px;\n  position: absolute;\n}\n.white-cup-2[_ngcontent-%COMP%] {\n  height: 50px;\n  width: 50px;\n  background-image: url('/escape-game-space-invaders/white-cup.png');\n  background-size: 100% 100%;\n  z-index: 150;\n  left: 438px;\n  top: 356px;\n  position: absolute;\n}\n.window2[_ngcontent-%COMP%] {\n  left: 50px;\n  top: 50px;\n}\n.window3[_ngcontent-%COMP%] {\n  left: 250px;\n  top: 50px;\n}\n.window4[_ngcontent-%COMP%] {\n  left: 450px;\n  top: 50px;\n}\n.window5[_ngcontent-%COMP%] {\n  left: 650px;\n  top: 50px;\n}\n.sushis[_ngcontent-%COMP%] {\n  height: 50px;\n  left: 140px;\n  top: 344px;\n  position: absolute;\n  z-index: 1000;\n}\n.monsieur-chat[_ngcontent-%COMP%] {\n  position: absolute;\n  z-index: 5000;\n  top: 308px;\n  left: 590px;\n  height: 100px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGl2aW5ncm9vbTQvbGl2aW5ncm9vbTQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLDZEQUF3QztFQUN4QywwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFdBQVc7RUFDWCxhQUFhO0FBQ2Y7QUFDQTtFQUNFLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0VBQTJDO0VBQzNDLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osV0FBVztFQUNYLGFBQWE7RUFDYixrQkFBa0I7QUFDcEI7QUFFQTtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gseUVBQXlFO0VBQ3pFLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0VBQTJDO0VBQzNDLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0VBQTZDO0VBQzdDLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0VBQTZDO0VBQzdDLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFVBQVU7RUFDVixTQUFTO0FBQ1g7QUFDQTtFQUNFLFdBQVc7RUFDWCxTQUFTO0FBQ1g7QUFDQTtFQUNFLFdBQVc7RUFDWCxTQUFTO0FBQ1g7QUFDQTtFQUNFLFdBQVc7RUFDWCxTQUFTO0FBQ1g7QUFDQTtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixhQUFhO0FBQ2Y7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsVUFBVTtFQUNWLFdBQVc7RUFDWCxhQUFhO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9saXZpbmdyb29tNC9saXZpbmdyb29tNC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRlc2sge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiA0MDBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2Rlc2sucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgei1pbmRleDogMTAwO1xuICBsZWZ0OiAxMDBweDtcbiAgYm90dG9tOiAxMDBweDtcbn1cbi50cnVuay0xIHtcbiAgaGVpZ2h0OiAxNTBweDtcbiAgd2lkdGg6IDMwMHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvdHJ1bmstMS5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMDA7XG4gIGxlZnQ6IDU1MHB4O1xuICBib3R0b206IDEwMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5sb2NrZXItMSB7XG4gIGhlaWdodDogMzBweDtcbiAgd2lkdGg6IDIwcHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pbWFnZXMvdmFyaW91cy9sb2NrZXItdHJ1bmNhdGVkLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHotaW5kZXg6IDEyMDtcbiAgbGVmdDogNjk5cHg7XG4gIHRvcDogNDg1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5tYWNib29rIHtcbiAgaGVpZ2h0OiAxMjVweDtcbiAgd2lkdGg6IDIwMHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvbWFjYm9vay5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMjA7XG4gIGxlZnQ6IDE5OXB4O1xuICB0b3A6IDI3NXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4ud2hpdGUtY3VwLTEge1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvd2hpdGUtY3VwLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHotaW5kZXg6IDE1MDtcbiAgbGVmdDogMzk4cHg7XG4gIHRvcDogMzQ2cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi53aGl0ZS1jdXAtMiB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy93aGl0ZS1jdXAucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgei1pbmRleDogMTUwO1xuICBsZWZ0OiA0MzhweDtcbiAgdG9wOiAzNTZweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLndpbmRvdzIge1xuICBsZWZ0OiA1MHB4O1xuICB0b3A6IDUwcHg7XG59XG4ud2luZG93MyB7XG4gIGxlZnQ6IDI1MHB4O1xuICB0b3A6IDUwcHg7XG59XG4ud2luZG93NCB7XG4gIGxlZnQ6IDQ1MHB4O1xuICB0b3A6IDUwcHg7XG59XG4ud2luZG93NSB7XG4gIGxlZnQ6IDY1MHB4O1xuICB0b3A6IDUwcHg7XG59XG4uc3VzaGlzIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBsZWZ0OiAxNDBweDtcbiAgdG9wOiAzNDRweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxMDAwO1xufVxuLm1vbnNpZXVyLWNoYXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDUwMDA7XG4gIHRvcDogMzA4cHg7XG4gIGxlZnQ6IDU5MHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Livingroom4Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-livingroom4',
                templateUrl: './livingroom4.component.html',
                styleUrls: ['./livingroom4.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/lockergold1/lockergold1.component.ts":
/*!******************************************************!*\
  !*** ./src/app/lockergold1/lockergold1.component.ts ***!
  \******************************************************/
/*! exports provided: Lockergold1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lockergold1Component", function() { return Lockergold1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_phone_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/phone.service */ "./src/app/service/phone.service.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");







function Lockergold1Component_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Lockergold1Component_div_1_Template_div_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1.validate(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Lockergold1Component_div_1_Template_div_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.increment(0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Lockergold1Component_div_1_Template_div_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.increment(1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Lockergold1Component_div_1_Template_div_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.increment(2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Lockergold1Component_div_1_Template_div_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.increment(3); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.code, " ");
} }
const _c0 = function () { return ["/livingroom4"]; };
class Lockergold1Component {
    constructor(phoneService, gameService, router) {
        this.phoneService = phoneService;
        this.gameService = gameService;
        this.router = router;
        this.code = "0000";
    }
    ngOnInit() {
    }
    replaceAt(string, index, replacement) {
        return string.substr(0, index) + replacement + string.substr(index + 1);
    }
    increment(number) {
        let char = this.code.charAt(number);
        let oldNum = parseInt(char);
        let newNum = oldNum == 9 ? 0 : (oldNum + 1);
        this.code = this.replaceAt(this.code, number, newNum);
        this.validate();
    }
    validate() {
        if (parseInt(this.code) === this.phoneService.getSumOfScores()) {
            this.gameService.usedItems[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LOCKER_GOLD_OPENED] = true;
            this.gameService.useItem(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PHONE_ITEM);
            this.gameService.playSound("Chest-Creak.wav");
            this.router.navigate(["/inside-chest-gold"]);
        }
    }
    isLockerOpened() {
        return this.gameService.usedItems[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LOCKER_GOLD_OPENED];
    }
}
Lockergold1Component.ɵfac = function Lockergold1Component_Factory(t) { return new (t || Lockergold1Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_phone_service__WEBPACK_IMPORTED_MODULE_2__["PhoneService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
Lockergold1Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Lockergold1Component, selectors: [["app-lockergold1"]], decls: 4, vars: 3, consts: [[1, "locker-1-bg"], ["class", "locker-1", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], [1, "locker-1"], [1, "validate-code", "pointer", 3, "click"], [1, "code-container", "total-score"], [1, "increment0", "increment", 3, "click"], [1, "increment1", "increment", 3, "click"], [1, "increment2", "increment", 3, "click"], [1, "increment3", "increment", 3, "click"]], template: function Lockergold1Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, Lockergold1Component_div_1_Template, 8, 1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isLockerOpened());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]], styles: [".locker-1-bg[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  background-image: url('/escape-game-space-invaders/zoom-locker-gold-1-background.jpg');\n  background-size: 100% 100%;\n  z-index: 1000;\n  position: absolute;\n}\n.locker-1[_ngcontent-%COMP%] {\n  height: 370px;\n  width: 320px;\n  background-image: url('/escape-game-space-invaders/locker-truncated.png');\n  background-size: 100% 100%;\n  z-index: 120;\n  left: 378px;\n  top: 285px;\n  position: absolute;\n}\n.code-container[_ngcontent-%COMP%] {\n  text-align: center;\n  top: 227px;\n  left: 77px;\n  position: absolute;\n  padding-top: 14px;\n  height: 37px;\n  font-size: 55px;\n  letter-spacing: 15px;\n  background-color: black;\n}\n.increment[_ngcontent-%COMP%] {\n  cursor: pointer;\n  width: 34px;\n  height: 50px;\n  margin-top: -34px;\n  position: absolute;\n}\n.increment1[_ngcontent-%COMP%] {\n  left: 40px;\n}\n.increment2[_ngcontent-%COMP%] {\n  left: 83px;\n}\n.increment3[_ngcontent-%COMP%] {\n  left: 126px;\n}\n.validate-code[_ngcontent-%COMP%] {\n  width: 300px;\n  height: 218px;\n  position: absolute;\n  z-index: 5000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9ja2VyZ29sZDEvbG9ja2VyZ29sZDEuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osc0ZBQWlFO0VBQ2pFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2Isa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLHlFQUF5RTtFQUN6RSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLHVCQUF1QjtBQUN6QjtBQUNBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7RUFDWCxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsVUFBVTtBQUNaO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUVBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsYUFBYTtBQUNmIiwiZmlsZSI6InNyYy9hcHAvbG9ja2VyZ29sZDEvbG9ja2VyZ29sZDEuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2NrZXItMS1iZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImltYWdlcy96b29tLWxvY2tlci1nb2xkLTEtYmFja2dyb3VuZC5qcGdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICB6LWluZGV4OiAxMDAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4ubG9ja2VyLTEge1xuICBoZWlnaHQ6IDM3MHB4O1xuICB3aWR0aDogMzIwcHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pbWFnZXMvdmFyaW91cy9sb2NrZXItdHJ1bmNhdGVkLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHotaW5kZXg6IDEyMDtcbiAgbGVmdDogMzc4cHg7XG4gIHRvcDogMjg1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5jb2RlLWNvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdG9wOiAyMjdweDtcbiAgbGVmdDogNzdweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBwYWRkaW5nLXRvcDogMTRweDtcbiAgaGVpZ2h0OiAzN3B4O1xuICBmb250LXNpemU6IDU1cHg7XG4gIGxldHRlci1zcGFjaW5nOiAxNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cbi5pbmNyZW1lbnQge1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHdpZHRoOiAzNHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIG1hcmdpbi10b3A6IC0zNHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4uaW5jcmVtZW50MSB7XG4gIGxlZnQ6IDQwcHg7XG59XG4uaW5jcmVtZW50MiB7XG4gIGxlZnQ6IDgzcHg7XG59XG4uaW5jcmVtZW50MyB7XG4gIGxlZnQ6IDEyNnB4O1xufVxuXG4udmFsaWRhdGUtY29kZSB7XG4gIHdpZHRoOiAzMDBweDtcbiAgaGVpZ2h0OiAyMThweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA1MDAwO1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Lockergold1Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-lockergold1',
                templateUrl: './lockergold1.component.html',
                styleUrls: ['./lockergold1.component.css']
            }]
    }], function () { return [{ type: _service_phone_service__WEBPACK_IMPORTED_MODULE_2__["PhoneService"] }, { type: _service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/model/Bullet.ts":
/*!*********************************!*\
  !*** ./src/app/model/Bullet.ts ***!
  \*********************************/
/*! exports provided: Bullet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Bullet", function() { return Bullet; });
class Bullet {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}


/***/ }),

/***/ "./src/app/phone-invaders/phone-invaders.component.ts":
/*!************************************************************!*\
  !*** ./src/app/phone-invaders/phone-invaders.component.ts ***!
  \************************************************************/
/*! exports provided: PhoneInvadersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneInvadersComponent", function() { return PhoneInvadersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _service_phone_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/phone.service */ "./src/app/service/phone.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");




function PhoneInvadersComponent_div_5_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const invader_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/livingroom-windows/city-find-invader", invader_r2.key, "-snapped.jpg", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function PhoneInvadersComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, PhoneInvadersComponent_div_5_div_2_Template, 2, 1, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "keyvalue");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](3, 1, ctx_r0.getSnappedInvaders()));
} }
const _c0 = function (a0) { return { "totalScoreSuperiorTo0": a0 }; };
class PhoneInvadersComponent {
    constructor(phoneService) {
        this.phoneService = phoneService;
    }
    ngOnInit() {
    }
    getTotalPoint() {
        if (this.phoneService.getTotalPoint() == 0)
            return "0000";
        return this.phoneService.getTotalPoint();
    }
    getSnappedInvaders() {
        return this.phoneService.getInvadersSnapped();
    }
    countInvaders() {
        return Object.keys(this.phoneService.getInvadersSnapped()).length;
    }
}
PhoneInvadersComponent.ɵfac = function PhoneInvadersComponent_Factory(t) { return new (t || PhoneInvadersComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_phone_service__WEBPACK_IMPORTED_MODULE_1__["PhoneService"])); };
PhoneInvadersComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PhoneInvadersComponent, selectors: [["app-phone-invaders"]], decls: 6, vars: 5, consts: [[1, "home-root"], [1, "phone-container"], ["src", "assets/images/items/phone-flash-invaders.png", 1, "phone"], [1, "total-score", 3, "ngClass"], ["class", "phone-background", 4, "ngIf"], [1, "phone-background"], [1, "snapped-invader-list-container"], ["class", "snapped-invader-container", 4, "ngFor", "ngForOf"], [1, "snapped-invader-container"], [3, "src"]], template: function PhoneInvadersComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, PhoneInvadersComponent_div_5_Template, 4, 3, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c0, ctx.getTotalPoint() > 0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Score : ", ctx.getTotalPoint(), "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.countInvaders() > 0);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["KeyValuePipe"]], styles: [".home-root[_ngcontent-%COMP%] {\n  background-size: 950px 650px;\n  height: 100%;\n  background-color: rgb(10,10,10);\n  color: white;\n}\n.phone-container[_ngcontent-%COMP%] {\n  position: relative;\n  margin-left: 150px;\n  width: 500px;\n}\n.phone-background[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 230px;\n  left: 130px;\n  height: 350px;\n  width: 250px;\n  background-color: black;\n  overflow: scroll;\n}\n.phone[_ngcontent-%COMP%] {\n  display: block;\n  max-width: 100%;\n  position: absolute;\n}\n.total-score[_ngcontent-%COMP%] {\n  position: absolute;\n  font-size: 30px;\n  top: 274px;\n  left: 161px;\n  z-index: 100;\n}\n.totalScoreSuperiorTo0[_ngcontent-%COMP%] {\n  top: 230px;\n}\n.snapped-invader-list-container[_ngcontent-%COMP%] {\n  padding-top: 20px;\n  padding-left: 30px;\n}\n.snapped-invader-container[_ngcontent-%COMP%] {\n  height : 90px;\n  width : 90px;\n  float: left;\n  margin: 5px;\n}\n.snapped-invader-container[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGhvbmUtaW52YWRlcnMvcGhvbmUtaW52YWRlcnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDRCQUE0QjtFQUM1QixZQUFZO0VBQ1osK0JBQStCO0VBQy9CLFlBQVk7QUFDZDtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixZQUFZO0FBQ2Q7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsV0FBVztFQUNYLGFBQWE7RUFDYixZQUFZO0VBQ1osdUJBQXVCO0VBQ3ZCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsY0FBYztFQUNkLGVBQWU7RUFDZixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsVUFBVTtFQUNWLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7QUFDQTtFQUNFLFVBQVU7QUFDWjtBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsYUFBYTtFQUNiLFlBQVk7RUFDWixXQUFXO0VBQ1gsV0FBVztBQUNiO0FBRUE7RUFDRSxXQUFXO0VBQ1gsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvcGhvbmUtaW52YWRlcnMvcGhvbmUtaW52YWRlcnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ob21lLXJvb3Qge1xuICBiYWNrZ3JvdW5kLXNpemU6IDk1MHB4IDY1MHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigxMCwxMCwxMCk7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5waG9uZS1jb250YWluZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1sZWZ0OiAxNTBweDtcbiAgd2lkdGg6IDUwMHB4O1xufVxuLnBob25lLWJhY2tncm91bmQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMjMwcHg7XG4gIGxlZnQ6IDEzMHB4O1xuICBoZWlnaHQ6IDM1MHB4O1xuICB3aWR0aDogMjUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogc2Nyb2xsO1xufVxuLnBob25lIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLnRvdGFsLXNjb3JlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBmb250LXNpemU6IDMwcHg7XG4gIHRvcDogMjc0cHg7XG4gIGxlZnQ6IDE2MXB4O1xuICB6LWluZGV4OiAxMDA7XG59XG4udG90YWxTY29yZVN1cGVyaW9yVG8wIHtcbiAgdG9wOiAyMzBweDtcbn1cblxuLnNuYXBwZWQtaW52YWRlci1saXN0LWNvbnRhaW5lciB7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDMwcHg7XG59XG5cbi5zbmFwcGVkLWludmFkZXItY29udGFpbmVyIHtcbiAgaGVpZ2h0IDogOTBweDtcbiAgd2lkdGggOiA5MHB4O1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luOiA1cHg7XG59XG5cbi5zbmFwcGVkLWludmFkZXItY29udGFpbmVyIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PhoneInvadersComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-phone-invaders',
                templateUrl: './phone-invaders.component.html',
                styleUrls: ['./phone-invaders.component.css']
            }]
    }], function () { return [{ type: _service_phone_service__WEBPACK_IMPORTED_MODULE_1__["PhoneService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/read-grimoire/read-grimoire.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/read-grimoire/read-grimoire.component.ts ***!
  \**********************************************************/
/*! exports provided: ReadGrimoireComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReadGrimoireComponent", function() { return ReadGrimoireComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");



function ReadGrimoireComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " Hexadecimal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " SPACE (\" \") = 20 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " I = 49");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " J = 4A");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " P = 50");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "See next page...");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function ReadGrimoireComponent_img_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 8);
} }
class ReadGrimoireComponent {
    constructor() {
        this.page = 1;
    }
    ngOnInit() {
    }
    changePage() {
        this.page = (this.page == 2) ? 1 : (this.page + 1);
    }
}
ReadGrimoireComponent.ɵfac = function ReadGrimoireComponent_Factory(t) { return new (t || ReadGrimoireComponent)(); };
ReadGrimoireComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ReadGrimoireComponent, selectors: [["app-read-grimoire"]], decls: 3, vars: 2, consts: [["id", "inside-grimoire", 1, "pointer", 3, "click"], ["class", "page1", 4, "ngIf"], ["src", "assets/images/various/hexa-ascii.jpg", "class", "hexa-ascii", 4, "ngIf"], [1, "page1"], ["src", "assets/images/various/space-invader-green.png", 1, "green-space-invader"], [1, "clipart-bubble"], [1, "inner-text"], [1, "next-page"], ["src", "assets/images/various/hexa-ascii.jpg", 1, "hexa-ascii"]], template: function ReadGrimoireComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReadGrimoireComponent_Template_div_click_0_listener() { return ctx.changePage(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ReadGrimoireComponent_div_1_Template, 16, 0, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ReadGrimoireComponent_img_2_Template, 1, 0, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.page == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.page == 2);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"]], styles: ["#inside-grimoire[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-color: black;\n  background-image: url('/escape-game-space-invaders/inside-grimoire.png');\n  background-size: 100% 100%;\n}\n\n.hexa-ascii[_ngcontent-%COMP%] {\n  opacity: 0.7;\n  height: 529px;\n  left: 117px;\n  position: absolute;\n  top: 50px;\n}\n\n.green-space-invader[_ngcontent-%COMP%] {\n  height: 100px;\n  left: 75px;\n  top: 250px;\n  position: absolute;\n}\n\n.clipart-bubble[_ngcontent-%COMP%] {\n  height: 370px;\n  width: 320px;\n  background-image: url('/escape-game-space-invaders/comic-clipart-bubble.png');\n  background-size: 100% 100%;\n  z-index: 120;\n  left: 142px;\n  top: 015px;\n  position: absolute;\n}\n\n.inner-text[_ngcontent-%COMP%] {\n  font-family: Comic Sans MS, Comic Sans, cursive;\n  left: 90px;\n  top: 50px;\n  position: absolute;\n}\n\n.next-page[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 100px;\n  right: 200px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVhZC1ncmltb2lyZS9yZWFkLWdyaW1vaXJlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsd0VBQW1EO0VBQ25ELDBCQUEwQjtBQUM1Qjs7QUFFQTtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixTQUFTO0FBQ1g7O0FBQ0E7RUFDRSxhQUFhO0VBQ2IsVUFBVTtFQUNWLFVBQVU7RUFDVixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLDZFQUF3RDtFQUN4RCwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1Ysa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UsK0NBQStDO0VBQy9DLFVBQVU7RUFDVixTQUFTO0VBQ1Qsa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0FBQ2QiLCJmaWxlIjoic3JjL2FwcC9yZWFkLWdyaW1vaXJlL3JlYWQtZ3JpbW9pcmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNpbnNpZGUtZ3JpbW9pcmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvaW5zaWRlLWdyaW1vaXJlLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG59XG5cbi5oZXhhLWFzY2lpIHtcbiAgb3BhY2l0eTogMC43O1xuICBoZWlnaHQ6IDUyOXB4O1xuICBsZWZ0OiAxMTdweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwcHg7XG59XG4uZ3JlZW4tc3BhY2UtaW52YWRlciB7XG4gIGhlaWdodDogMTAwcHg7XG4gIGxlZnQ6IDc1cHg7XG4gIHRvcDogMjUwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5jbGlwYXJ0LWJ1YmJsZSB7XG4gIGhlaWdodDogMzcwcHg7XG4gIHdpZHRoOiAzMjBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaW1hZ2VzL2NvbWljLWNsaXBhcnQtYnViYmxlLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHotaW5kZXg6IDEyMDtcbiAgbGVmdDogMTQycHg7XG4gIHRvcDogMDE1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cbi5pbm5lci10ZXh0IHtcbiAgZm9udC1mYW1pbHk6IENvbWljIFNhbnMgTVMsIENvbWljIFNhbnMsIGN1cnNpdmU7XG4gIGxlZnQ6IDkwcHg7XG4gIHRvcDogNTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLm5leHQtcGFnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAxMDBweDtcbiAgcmlnaHQ6IDIwMHB4O1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ReadGrimoireComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-read-grimoire',
                templateUrl: './read-grimoire.component.html',
                styleUrls: ['./read-grimoire.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/service/game.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/game.service.ts ***!
  \*****************************************/
/*! exports provided: GameService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameService", function() { return GameService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class GameService {
    constructor() {
        this.currentItems = {};
        this.usedItems = {};
        this.trophies = {};
        // Hello Hacker friend, this is top Secret "encryption" ;-)
        this.stc = "SSBBTSBKVVBJVEVS";
        this.topSecret = "NzM0NUI=";
        this.theJukebokMusic = "It's a small World";
        // ************************
        // LocalStorage
        // ************************
        this.localStorageKey = "Saved";
        this.loadGame();
        this.launchAutoSave();
    }
    addTrophy(name) {
        // @ts-ignore
        gtag('event', 'add_' + name);
        // @ts-ignore
        gtag('event', 'trophy_' + (Object.keys(this.trophies).length));
        this.trophies[name] = {
            taken: true,
            onPainting: false
        };
    }
    hasTrophy(name) {
        return this.trophies[name];
    }
    getCurrentItems() {
        return this.currentItems;
    }
    addItem(name, opt) {
        this.currentItems[name] = opt ? opt : true;
    }
    hasItemBeenTaken(name) {
        return this.currentItems[name] || this.usedItems[name];
    }
    hasItemBeenUsed(name) {
        return this.usedItems[name];
    }
    selectItem(name) {
        this.selectedItemName = this.isItemSelected(name) ? undefined : name;
    }
    isItemSelected(name) {
        return this.selectedItemName == name;
    }
    useItem(KEY_DRAWER1) {
        this.selectedItemName = undefined;
        this.addToUsedItem(KEY_DRAWER1, this.currentItems[KEY_DRAWER1]);
        delete this.currentItems[KEY_DRAWER1];
    }
    useInNSecond(key, value, timeout) {
        setTimeout(() => {
            this.addToUsedItem(key, value);
        }, timeout);
    }
    addToUsedItem(key, obj) {
        this.usedItems[key] = obj;
    }
    isInCurrentItem(item) {
        return this.currentItems[item];
    }
    getSelectTrophy() {
        return this.selectedTrophyName;
    }
    isTrophyOnPainting(trophyName) {
        return this.trophies[trophyName] && this.trophies[trophyName].onPainting;
    }
    // ************************
    // Specific
    // ************************
    getInvaderLetters() {
        return ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F'];
    }
    inInvaderLetter(el) {
        return this.getInvaderLetters().indexOf(el.toUpperCase()) != -1;
    }
    getStc() {
        return atob(this.stc);
    }
    getTopSecretCode() {
        return atob(this.topSecret);
    }
    loadGame() {
        let game = localStorage.getItem(this.localStorageKey);
        if (game) {
            this.loadData(game);
        }
    }
    saveGame() {
        localStorage.setItem(this.localStorageKey, JSON.stringify(this.getData()));
    }
    loadData(game) {
        let gameJson = JSON.parse(game);
        this.currentItems = gameJson['currentItems'];
        this.usedItems = gameJson['usedItems'];
        this.trophies = gameJson['trophees'];
    }
    getData() {
        return {
            "currentItems": this.currentItems,
            "usedItems": this.usedItems,
            "trophees": this.trophies
        };
    }
    // ************************
    // Utils
    // ************************
    playSound(sound) {
        let audio = new Audio();
        audio.src = "assets/sounds/" + sound;
        audio.load();
        audio.play();
    }
    getTheJukeBoxMusic() {
        return this.theJukebokMusic;
    }
    playMusic(musicName) {
        if (this.currentMusic) {
            this.currentMusic.pause();
        }
        this.currentMusic = new Audio();
        this.currentMusic.src = "assets/sounds/music/" + musicName;
        this.currentMusic.load();
        this.currentMusic.play();
    }
    isCurrentMusicPlaying() {
        return this.currentMusic && this.currentMusic.paused;
    }
    playOrPauseCurrentMusic() {
        if (!this.currentMusic) {
            return;
        }
        if (this.currentMusic.paused) {
            this.currentMusic.play();
        }
        else {
            this.currentMusic.pause();
        }
    }
    stopCurrentMusic() {
        if (this.currentMusic) {
            this.currentMusic.pause();
        }
    }
    resetGame(force) {
        if (!force) {
            force = confirm("This will Reset all your Game. You'll lose all your items ! Are you sure you want to do this ?");
        }
        if (force) {
            this.currentItems = {};
            this.usedItems = {};
            this.trophies = {};
            this.saveGame();
        }
    }
    launchAutoSave() {
        this.saveGame();
        setTimeout(this.launchAutoSave.bind(this), 5000);
    }
}
GameService.ɵfac = function GameService_Factory(t) { return new (t || GameService)(); };
GameService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: GameService, factory: GameService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GameService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/service/phone.service.ts":
/*!******************************************!*\
  !*** ./src/app/service/phone.service.ts ***!
  \******************************************/
/*! exports provided: PhoneService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneService", function() { return PhoneService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./game.service */ "./src/app/service/game.service.ts");




class PhoneService {
    constructor(gameService) {
        this.gameService = gameService;
        this.invadersToSnap = [
            { left: 372, top: 313, width: 41, height: 106, score: 642, legend: 'DUDE', song: "the-dude.mp3",
                backpacker: { x: 486, y: 273, width: 75, height: 150 } },
            { left: 545, top: 260, width: 59, height: 56, score: 569, legend: 'BEER', song: "beer-song.mp3",
                backpacker: { x: 490, y: 463, width: 35, height: 75 } },
            { left: 505, top: 330, width: 20, height: 20, score: 664, legend: 'PARIS', song: "edith-piaf-sous-le-ciel-de-paris.mp3",
                backpacker: { x: 266, y: 413, width: 50, height: 90 } },
            { left: 566, top: 280, width: 56, height: 68, score: 934, legend: 'MUSHROOM', song: "the-mushroom-song-original-song-by-eric-butler.mp3",
                backpacker: { x: 286, y: 213, width: 25, height: 50 } },
            { left: 259, top: 324, width: 256, height: 248, score: 442, legend: 'AtlAntic CoAst SurF', song: "the-beach-boys-surfer-girl.mp3",
                backpacker: { x: 194, y: 453, width: 50, height: 90 } },
            { left: 259, top: 284, width: 134, height: 159, score: 512, legend: "MOUNTAIN", song: "the-misty-mountains-cold-the-hobbit.mp3",
                backpacker: { x: 549, y: 320, width: 25, height: 50 } },
            { left: 330, top: 300, width: 200, height: 150, score: 230, legend: "XX'X A XXAXX XXXXX", song: "its-a-small-world-after.mp3",
                backpacker: { x: 563, y: 166, width: 9, height: 17 } },
            { left: 390, top: 148, width: 50, height: 100, score: 230, legend: "CELINE DION", song: "celine-dion-jirai-ou-tu-iras.mp3",
                backpacker: { x: 488, y: 356, width: 59, height: 157 } }
        ];
    }
    getInvadersToSnap() {
        return this.invadersToSnap;
    }
    getPhone() {
        return this.gameService.getCurrentItems()[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PHONE_ITEM];
    }
    getInvadersSnapped() {
        return this.getPhone()[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PHONE_ITEM_INVADERS];
    }
    hadAlreadyBeenSnapped(invaderId) {
        return this.getInvadersSnapped()[invaderId];
    }
    snapInvader(invaderId, score) {
        this.getInvadersSnapped()[invaderId] = true;
        if (this.hadAlreadyBeenSnapped(invaderId)) {
            this.getPhone().totalScore += score;
        }
    }
    getTotalPoint() {
        return this.getPhone() ? this.getPhone().totalScore : 0;
    }
    getSumOfScores() {
        let sum = 0;
        for (let i = 0; i < this.invadersToSnap.length; i++) {
            sum += this.invadersToSnap[i].score;
        }
        return sum;
    }
    getNewPhone() {
        let phone = {};
        phone["totalScore"] = 0;
        phone[_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].PHONE_ITEM_INVADERS] = {};
        phone["zoomUrl"] = "/phone-invaders";
        return phone;
    }
}
PhoneService.ɵfac = function PhoneService_Factory(t) { return new (t || PhoneService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
PhoneService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: PhoneService, factory: PhoneService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PhoneService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/trophies/trophy.component.ts":
/*!**********************************************!*\
  !*** ./src/app/trophies/trophy.component.ts ***!
  \**********************************************/
/*! exports provided: TrophyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrophyComponent", function() { return TrophyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");







function TrophyComponent_div_0_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TrophyComponent_div_0_div_1_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const trophy_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit; const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.selectTrophy(trophy_r2.key); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const trophy_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("selectedTrophy", ctx_r3.isTrophySelected(trophy_r2.key));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/trophies/", trophy_r2.key, ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function TrophyComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TrophyComponent_div_0_div_1_Template, 2, 3, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const trophy_r2 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.isOnPainting(trophy_r2.key));
} }
const _c0 = function (a0, a1) { return { "top.px": a0, "left.px": a1 }; };
function TrophyComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/trophies/", ctx_r1.getSelectedTrophy(), ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](2, _c0, ctx_r1.trophyY, ctx_r1.trophyX));
} }
class TrophyComponent {
    constructor(gameService, router) {
        this.gameService = gameService;
        this.router = router;
    }
    ngOnInit() {
    }
    getTrophies() {
        return this.gameService.trophies;
    }
    isOnLivingRoom1Page() {
        return this.router.url == "/livingroom1";
    }
    getSelectedTrophy() {
        return this.gameService.getSelectTrophy();
    }
    isTrophySelected(name) {
        return this.getSelectedTrophy() == name;
    }
    selectTrophy(key) {
        if (!this.gameService.hasItemBeenUsed(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].LASER)) {
            return;
        }
        if (key == this.getSelectedTrophy()) {
            this.gameService.selectedTrophyName = undefined;
        }
        else {
            this.gameService.selectedTrophyName = key;
        }
    }
    onMouseMove(e) {
        let rootEl = jquery__WEBPACK_IMPORTED_MODULE_2__("#livingroom1-root");
        if (rootEl.offset()) {
            this.trophyX = e.pageX - rootEl.offset().left + 70;
            this.trophyY = e.pageY - 80;
        }
    }
    isOnPainting(key) {
        return this.gameService.isTrophyOnPainting(key);
    }
}
TrophyComponent.ɵfac = function TrophyComponent_Factory(t) { return new (t || TrophyComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
TrophyComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TrophyComponent, selectors: [["app-trophy"]], hostBindings: function TrophyComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mousemove", function TrophyComponent_mousemove_HostBindingHandler($event) { return ctx.onMouseMove($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveDocument"]);
    } }, decls: 3, vars: 4, consts: [[4, "ngFor", "ngForOf"], [4, "ngIf"], ["class", "trophy-container", 3, "selectedTrophy", "click", 4, "ngIf"], [1, "trophy-container", 3, "click"], [1, "image-trophy", 3, "src"], [1, "trophy-to-place", 3, "src", "ngStyle"]], template: function TrophyComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TrophyComponent_div_0_Template, 2, 1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](1, "keyvalue");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TrophyComponent_div_2_Template, 2, 5, "div", 1);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](1, 2, ctx.getTrophies()));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.getSelectedTrophy() && ctx.isOnLivingRoom1Page());
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgStyle"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["KeyValuePipe"]], styles: [".trophy-container[_ngcontent-%COMP%] {\n  position: relative;\n  margin-left: 5px;\n  margin-right: 5px;\n  height: 80px;\n  width: 80px;\n  border: 1px solid black;\n  background-color: rgb(40,40,40);\n  border-radius: 5px;\n  z-index: 6000;\n}\n\n.image-trophy[_ngcontent-%COMP%] {\n  display:  block;\n  max-width: 80%;\n  max-height: 80%;\n  margin: auto auto;\n  padding: 5px;\n}\n\n.selectedTrophy[_ngcontent-%COMP%] {\n  border: 3px white solid;\n}\n\n.trophy-to-place[_ngcontent-%COMP%] {\n  cursor: -webkit-grab;\n  cursor: grab;\n  height: 50px;\n  position: absolute;\n  z-index: 5000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdHJvcGhpZXMvdHJvcGh5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osV0FBVztFQUNYLHVCQUF1QjtFQUN2QiwrQkFBK0I7RUFDL0Isa0JBQWtCO0VBQ2xCLGFBQWE7QUFDZjs7QUFFQTtFQUNFLGVBQWU7RUFDZixjQUFjO0VBQ2QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixZQUFZO0FBQ2Q7O0FBQ0E7RUFDRSx1QkFBdUI7QUFDekI7O0FBQ0E7RUFDRSxvQkFBWTtFQUFaLFlBQVk7RUFDWixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGFBQWE7QUFDZiIsImZpbGUiOiJzcmMvYXBwL3Ryb3BoaWVzL3Ryb3BoeS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRyb3BoeS1jb250YWluZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBoZWlnaHQ6IDgwcHg7XG4gIHdpZHRoOiA4MHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDQwLDQwLDQwKTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB6LWluZGV4OiA2MDAwO1xufVxuXG4uaW1hZ2UtdHJvcGh5IHtcbiAgZGlzcGxheTogIGJsb2NrO1xuICBtYXgtd2lkdGg6IDgwJTtcbiAgbWF4LWhlaWdodDogODAlO1xuICBtYXJnaW46IGF1dG8gYXV0bztcbiAgcGFkZGluZzogNXB4O1xufVxuLnNlbGVjdGVkVHJvcGh5IHtcbiAgYm9yZGVyOiAzcHggd2hpdGUgc29saWQ7XG59XG4udHJvcGh5LXRvLXBsYWNlIHtcbiAgY3Vyc29yOiBncmFiO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogNTAwMDtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TrophyComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-trophy',
                templateUrl: './trophy.component.html',
                styleUrls: ['./trophy.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_3__["GameService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }]; }, { onMouseMove: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['document:mousemove', ['$event']]
        }] }); })();


/***/ }),

/***/ "./src/app/under-sofa-right/under-sofa-right.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/under-sofa-right/under-sofa-right.component.ts ***!
  \****************************************************************/
/*! exports provided: UnderSofaRightComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnderSofaRightComponent", function() { return UnderSofaRightComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.constants */ "./src/app/app.constants.ts");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");






function UnderSofaRightComponent_img_3_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UnderSofaRightComponent_img_3_Template_img_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r1.takeNurse(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/livingroom1"]; };
class UnderSofaRightComponent {
    constructor(gameService) {
        this.gameService = gameService;
    }
    ngOnInit() {
    }
    takeNurse() {
        this.gameService.addTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].NURSE_INVADER);
    }
    isNurseTaken() {
        return this.gameService.hasTrophy(_app_constants__WEBPACK_IMPORTED_MODULE_1__["AppConstants"].NURSE_INVADER);
    }
}
UnderSofaRightComponent.ɵfac = function UnderSofaRightComponent_Factory(t) { return new (t || UnderSofaRightComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"])); };
UnderSofaRightComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: UnderSofaRightComponent, selectors: [["app-under-sofa-right"]], decls: 4, vars: 3, consts: [[1, "kitten-under-sofa"], [3, "routerLink"], [1, "goToBottom"], ["src", "assets/images/trophies/nurse-invader.png", "class", "nurse-invader pointer", 3, "click", 4, "ngIf"], ["src", "assets/images/trophies/nurse-invader.png", 1, "nurse-invader", "pointer", 3, "click"]], template: function UnderSofaRightComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, UnderSofaRightComponent_img_3_Template, 1, 0, "img", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isNurseTaken());
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"]], styles: [".kitten-under-sofa[_ngcontent-%COMP%] {\n  background-image: url('/escape-game-space-invaders/Kitten-hiding.jpg');\n  background-size: 100% 100%;\n  height: 100%;\n  width: 100%;\n}\n.nurse-invader[_ngcontent-%COMP%] {\n  height: 250px;\n  left: 520px;\n  top: 338px;\n  position: absolute;\n  transform: rotate(45deg);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdW5kZXItc29mYS1yaWdodC91bmRlci1zb2ZhLXJpZ2h0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzRUFBaUQ7RUFDakQsMEJBQTBCO0VBQzFCLFlBQVk7RUFDWixXQUFXO0FBQ2I7QUFDQTtFQUNFLGFBQWE7RUFDYixXQUFXO0VBQ1gsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQix3QkFBd0I7QUFDMUIiLCJmaWxlIjoic3JjL2FwcC91bmRlci1zb2ZhLXJpZ2h0L3VuZGVyLXNvZmEtcmlnaHQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5raXR0ZW4tdW5kZXItc29mYSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnaW1hZ2VzL0tpdHRlbi1oaWRpbmcuanBnJyk7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLm51cnNlLWludmFkZXIge1xuICBoZWlnaHQ6IDI1MHB4O1xuICBsZWZ0OiA1MjBweDtcbiAgdG9wOiAzMzhweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UnderSofaRightComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-under-sofa-right',
                templateUrl: './under-sofa-right.component.html',
                styleUrls: ['./under-sofa-right.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_2__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/watchingtv/watchingtv.component.ts":
/*!****************************************************!*\
  !*** ./src/app/watchingtv/watchingtv.component.ts ***!
  \****************************************************/
/*! exports provided: WatchingtvComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WatchingtvComponent", function() { return WatchingtvComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _service_game_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/game.service */ "./src/app/service/game.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");








function WatchingtvComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WatchingtvComponent_div_3_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const channel_r7 = ctx.$implicit; const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.switchToChannel(channel_r7); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const channel_r7 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](channel_r7);
} }
function WatchingtvComponent_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Tip: Channels 1 and 2 are a lot fun ! But not useful for the escape-game ;-)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function WatchingtvComponent_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Tip: The green invader is speaking a strange language, can you understand it ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function WatchingtvComponent_div_6_mat_icon_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "pause");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function WatchingtvComponent_div_6_mat_icon_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "play_arrow");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function WatchingtvComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WatchingtvComponent_div_6_Template_div_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r12.backToBeginning(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "first_page");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WatchingtvComponent_div_6_Template_div_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r14.rewindSubs(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "fast_rewind");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WatchingtvComponent_div_6_Template_div_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r15.playPause(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, WatchingtvComponent_div_6_mat_icon_11_Template, 2, 0, "mat-icon", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, WatchingtvComponent_div_6_mat_icon_12_Template, 2, 0, "mat-icon", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WatchingtvComponent_div_6_Template_div_click_13_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r16.moreSpeed(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "speed");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.isPlaying);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r3.isPlaying);
} }
function WatchingtvComponent_iframe_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "iframe", 18);
} }
function WatchingtvComponent_iframe_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "iframe", 19);
} }
function WatchingtvComponent_div_9_span_4_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/images/invaders/spaceInvaders-", el_r18.toLowerCase(), ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("alt", el_r18.toLowerCase());
} }
function WatchingtvComponent_div_9_span_4_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](el_r18);
} }
function WatchingtvComponent_div_9_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, WatchingtvComponent_div_9_span_4_span_1_Template, 2, 2, "span", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, WatchingtvComponent_div_9_span_4_span_2_Template, 2, 1, "span", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const el_r18 = ctx.$implicit;
    const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r17.inInvaderLetter(el_r18));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r17.inInvaderLetter(el_r18));
} }
const _c0 = function (a0) { return { "left.px": a0 }; };
function WatchingtvComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, WatchingtvComponent_div_9_span_4_Template, 3, 2, "span", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](2, _c0, ctx_r6.subtitleLeft));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.speech.split(""));
} }
const _c1 = function () { return [1, 2, 3]; };
const _c2 = function () { return ["/livingroom3"]; };
class WatchingtvComponent {
    constructor(gameService) {
        this.gameService = gameService;
        this.widthCalculated = 0;
        this.i = 0;
        this.channel = 1;
        this.isPlaying = true;
        this.timeoutDelay = 7;
        this.subtitleLeft = 150;
        this.decrement = 1;
        this.speech = this.getSpeech();
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
    }
    ngAfterViewChecked() {
        this.recalculateWidth();
    }
    switchToChannel(channel) {
        this.channel = channel;
        this.widthCalculated = 0;
        if (this.channel == 3) {
            this.initShake();
            this.animateSubtitles();
        }
    }
    switchChannel(add) {
        this.channel += add;
        if (this.channel == 4)
            this.channel = 1;
        if (this.channel == 0)
            this.channel = 3;
        this.switchToChannel(this.channel);
    }
    initSubtitles() {
        let watchingTv = jQuery("#watching-tv");
        if (!watchingTv || !watchingTv.offset())
            return;
        let hideLeft = $(".hide-left");
        hideLeft.css("left", -watchingTv.offset().left + "px");
        hideLeft.css("width", watchingTv.offset().left + "px");
        let hideRight = $(".hide-right");
        let rightWidth = $(window).width() - (watchingTv.offset().left + parseInt(watchingTv.css("width")));
        hideRight.css("right", -rightWidth);
        hideRight.css("width", rightWidth + "px");
    }
    animateSubtitles() {
        this.subtitleLeft = 0;
        this.initSubtitles();
        this.changeSubtitlesPos();
    }
    recalculateWidth() {
        let subtitleInnerEl = $(".subtitles-inner");
        let width = subtitleInnerEl.width();
        if (width > this.widthCalculated) {
            this.initSubtitles();
            this.widthCalculated = width;
        }
    }
    changeSubtitlesPos() {
        if (this.isPlaying && this.subtitleLeft > -this.widthCalculated - 500) {
            this.subtitleLeft -= this.decrement;
        }
        setTimeout(this.changeSubtitlesPos.bind(this), this.timeoutDelay);
    }
    moreSpeed() {
        this.decrement++;
    }
    playPause() {
        this.isPlaying = !this.isPlaying;
        this.decrement = 1;
    }
    backToBeginning() {
        this.subtitleLeft = 150;
    }
    rewindSubs() {
        this.subtitleLeft += 200;
    }
    initShake() {
        this.div = $(".invader-president");
        this.leftInvaderPresident = parseInt(this.div.css("left"));
        this.topInvaderPresident = parseInt(this.div.css("top"));
        this.shake();
    }
    shake() {
        this.i++;
        if (this.i % 50 > 0 && this.i % 50 < 30)
            this.div.animate({
                left: this.leftInvaderPresident + this.random() + "px",
                top: this.topInvaderPresident + this.random() + "px"
            }, 100);
        setTimeout(() => {
            this.shake();
        }, 100);
    }
    random() {
        return Math.round(Math.random() * 40 - 20);
    }
    getSpeech() {
        let speech = "";
        let stc = this.gameService.getStc();
        for (let i = 0; i < stc.length; i++) {
            speech += stc.charCodeAt(i).toString(16);
            speech += " |";
        }
        return speech;
    }
    inInvaderLetter(el) {
        return this.gameService.inInvaderLetter(el);
    }
}
WatchingtvComponent.ɵfac = function WatchingtvComponent_Factory(t) { return new (t || WatchingtvComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"])); };
WatchingtvComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: WatchingtvComponent, selectors: [["app-watchingtv"]], decls: 12, vars: 10, consts: [[1, "switch-channel-container"], ["class", "channel-button pointer", 3, "click", 4, "ngFor", "ngForOf"], ["class", "tips-channel1and2", 4, "ngIf"], ["class", "tips-channel3", 4, "ngIf"], [4, "ngIf"], ["class", "tv-channel pointer", "src", "https://www.youtube.com/embed/BywJa6iaa-M?autoplay=1", 4, "ngIf"], ["class", "tv-channel pointer", "src", "https://www.youtube.com/embed/VczbbiRmDik?autoplay=0", 4, "ngIf"], ["id", "watching-tv", 4, "ngIf"], [3, "routerLink"], [1, "goToBottom"], [1, "channel-button", "pointer", 3, "click"], [1, "tips-channel1and2"], [1, "tips-channel3"], [1, "button", 3, "click"], ["mat-fab", "", "color", "primary", "aria-label", "Back to beginning", "matTooltip", "Back to beginning", "matTooltipPosition", "below"], ["mat-fab", "", "color", "primary", "aria-label", "Rewind", "matTooltip", "Rewind", "matTooltipPosition", "below"], ["mat-fab", "", "color", "primary", "aria-label", "Play/Pause", "matTooltip", "Play/Pause", "matTooltipPosition", "below"], ["mat-fab", "", "color", "primary", "aria-label", "Play faster", "matTooltip", "Play faster", "matTooltipPosition", "below"], ["src", "https://www.youtube.com/embed/BywJa6iaa-M?autoplay=1", 1, "tv-channel", "pointer"], ["src", "https://www.youtube.com/embed/VczbbiRmDik?autoplay=0", 1, "tv-channel", "pointer"], ["id", "watching-tv"], [1, "invader-president"], [1, "subtitles"], [1, "subtitles-inner", 3, "ngStyle"], [4, "ngFor", "ngForOf"], [1, "hide-left"], [1, "hide-right"], ["class", "letter", 4, "ngIf"], [3, "src", "alt"], [1, "letter"]], template: function WatchingtvComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " CHANNELS:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, WatchingtvComponent_div_3_Template, 2, 1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, WatchingtvComponent_span_4_Template, 2, 0, "span", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, WatchingtvComponent_span_5_Template, 2, 0, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, WatchingtvComponent_div_6_Template, 17, 2, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, WatchingtvComponent_iframe_7_Template, 1, 0, "iframe", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, WatchingtvComponent_iframe_8_Template, 1, 0, "iframe", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, WatchingtvComponent_div_9_Template, 7, 4, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](8, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channel < 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channel == 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channel == 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channel == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channel == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.channel == 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](9, _c2));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButton"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__["MatTooltip"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIcon"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgStyle"]], styles: [".switch-channel-container[_ngcontent-%COMP%] {\n  position: absolute;\n  background-color: rgba(100,100,100, 0.7);\n  color: white;\n  z-index: 5000;\n  padding-top: 10px;\n  text-align: center;\n  box-shadow: 5px 5px 5px black;\n}\n\n.channel-button[_ngcontent-%COMP%] {\n  background-color: black;\n  padding: 15px;\n  text-align: center;\n  width: 30px;\n  height: 30px;\n  border-radius: 90px;\n  display: table-cell;\n  overflow: hidden;\n  vertical-align: middle;\n  margin-left: 5px;\n\n}\n\n.button[_ngcontent-%COMP%] {\n  float : left;\n}\n\n.switch-channel[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 100%;\n  z-index: 6000;\n}\n\n.switch-channel-container[_ngcontent-%COMP%] {\n  \n}\n\n.goToBottom[_ngcontent-%COMP%] {\n  z-index: 7000;\n}\n\n#watching-tv[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n  background-image: url('/escape-game-space-invaders/president-speech.jpg');\n  background-size: 100% 100%;\n}\n\n.invader-president[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 200px;\n  width: 200px;\n  background-image: url('/escape-game-space-invaders/space-invader-green.png');\n  background-size: 100% 100%;\n  top: 71px;\n  left: 295px;\n}\n\n.goToBottom[_ngcontent-%COMP%] {\n  height:200px;\n}\n\n.subtitles[_ngcontent-%COMP%] {\n  background-color: rgba(80,80,80,0.7);\n  width: 100%;\n  height: 100px;\n  bottom: 20px;\n  position: absolute;\n  letter-spacing: 20px;\n  color: white;\n  font-size: 80px;\n  overflow: hidden;\n  z-index: 100;\n}\n\n.subtitles-inner[_ngcontent-%COMP%] {\n  padding-left: 700px;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  overflow: hidden;\n  position: absolute;\n}\n\n.hide-left[_ngcontent-%COMP%], .hide-right[_ngcontent-%COMP%] {\n  height: 100px;\n  bottom: 20px;\n  background-color: black;\n  position: absolute;\n  z-index: 5000;\n}\n\n.tv-channel[_ngcontent-%COMP%] {\n  position: absolute;\n  height: 100%;\n  width: 100%;\n}\n\n.tips-channel1and2[_ngcontent-%COMP%], .tips-channel3[_ngcontent-%COMP%] {\n  width: 180px;\n  display: block;\n  font-style: italic;\n  font-size: 10px;\n}\n\n.tips-channel3[_ngcontent-%COMP%] {\n  width: 224px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2F0Y2hpbmd0di93YXRjaGluZ3R2LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsd0NBQXdDO0VBQ3hDLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQiw2QkFBNkI7QUFDL0I7O0FBRUE7RUFDRSx1QkFBdUI7RUFDdkIsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUN0QixnQkFBZ0I7O0FBRWxCOztBQUVBO0VBQ0UsWUFBWTtBQUNkOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxhQUFhO0FBQ2Y7O0FBQ0E7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCx5RUFBb0Q7RUFDcEQsMEJBQTBCO0FBQzVCOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0VBQ1osNEVBQTRFO0VBQzVFLDBCQUEwQjtFQUMxQixTQUFTO0VBQ1QsV0FBVztBQUNiOztBQUNBO0VBQ0UsWUFBWTtBQUNkOztBQUNBO0VBQ0Usb0NBQW9DO0VBQ3BDLFdBQVc7RUFDWCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsWUFBWTtFQUNaLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsWUFBWTtBQUNkOztBQUNBO0VBQ0UsbUJBQW1CO0VBQ25CLDBCQUFrQjtFQUFsQix1QkFBa0I7RUFBbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsYUFBYTtBQUNmOztBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxZQUFZO0VBQ1osY0FBYztFQUNkLGtCQUFrQjtFQUNsQixlQUFlO0FBQ2pCOztBQUNBO0VBQ0UsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvd2F0Y2hpbmd0di93YXRjaGluZ3R2LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3dpdGNoLWNoYW5uZWwtY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDEwMCwxMDAsMTAwLCAwLjcpO1xuICBjb2xvcjogd2hpdGU7XG4gIHotaW5kZXg6IDUwMDA7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJveC1zaGFkb3c6IDVweCA1cHggNXB4IGJsYWNrO1xufVxuXG4uY2hhbm5lbC1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgcGFkZGluZzogMTVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBib3JkZXItcmFkaXVzOiA5MHB4O1xuICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBtYXJnaW4tbGVmdDogNXB4O1xuXG59XG5cbi5idXR0b24ge1xuICBmbG9hdCA6IGxlZnQ7XG59XG4uc3dpdGNoLWNoYW5uZWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiA2MDAwO1xufVxuLnN3aXRjaC1jaGFubmVsLWNvbnRhaW5lciB7XG4gIC8qIGhlaWdodDogMTAwcHg7ICovXG59XG5cbi5nb1RvQm90dG9tIHtcbiAgei1pbmRleDogNzAwMDtcbn1cblxuI3dhdGNoaW5nLXR2IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJpbWFnZXMvcHJlc2lkZW50LXNwZWVjaC5qcGdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xufVxuLmludmFkZXItcHJlc2lkZW50IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDIwMHB4O1xuICB3aWR0aDogMjAwcHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pbWFnZXMvdmFyaW91cy9zcGFjZS1pbnZhZGVyLWdyZWVuLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIHRvcDogNzFweDtcbiAgbGVmdDogMjk1cHg7XG59XG4uZ29Ub0JvdHRvbSB7XG4gIGhlaWdodDoyMDBweDtcbn1cbi5zdWJ0aXRsZXMge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDgwLDgwLDgwLDAuNyk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBib3R0b206IDIwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDIwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiA4MHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB6LWluZGV4OiAxMDA7XG59XG4uc3VidGl0bGVzLWlubmVyIHtcbiAgcGFkZGluZy1sZWZ0OiA3MDBweDtcbiAgd2lkdGg6IG1heC1jb250ZW50O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4uaGlkZS1sZWZ0LCAuaGlkZS1yaWdodCB7XG4gIGhlaWdodDogMTAwcHg7XG4gIGJvdHRvbTogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogNTAwMDtcbn1cblxuXG4udHYtY2hhbm5lbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnRpcHMtY2hhbm5lbDFhbmQyLCAudGlwcy1jaGFubmVsMyB7XG4gIHdpZHRoOiAxODBweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgZm9udC1zaXplOiAxMHB4O1xufVxuLnRpcHMtY2hhbm5lbDMge1xuICB3aWR0aDogMjI0cHg7XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WatchingtvComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-watchingtv',
                templateUrl: './watchingtv.component.html',
                styleUrls: ['./watchingtv.component.css']
            }]
    }], function () { return [{ type: _service_game_service__WEBPACK_IMPORTED_MODULE_1__["GameService"] }]; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/thibautdebroca/Documents/dev/space-invaders/src/main/resources/static/escape-game/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map